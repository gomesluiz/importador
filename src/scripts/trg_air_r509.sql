DELIMITER //
DROP TRIGGER IF EXISTS trg_air_r509
//
CREATE TRIGGER trg_air_r509
	AFTER 	INSERT 
		ON R509 
		FOR EACH ROW
BEGIN 
	DELETE FROM _drel WHERE fonte = NEW.fonte AND referencia = NEW.referencia;
        INSERT INTO _drel SELECT * FROM drel WHERE fonte = NEW.fonte AND referencia = NEW.referencia;
END;
//