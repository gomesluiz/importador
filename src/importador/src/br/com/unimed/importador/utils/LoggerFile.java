/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class LoggerFile {

    private static Logger logger = null;
    private static FileHandler fh;
    private static Vector<Level> type = new Vector<Level>();
    private static LoggerFile _instance = null;

    public static synchronized  Logger getLogger() {
       if (logger != null) {
          return logger;
       }

       logger = Logger.getLogger("Log");
       logger.setUseParentHandlers(false);
       try {
          Proper.loadFile("base.ini");
          fh = new FileHandler(Proper.getValue("error_log_path"),
          Boolean.getBoolean(Proper.getValue("action_log")));
          fh.setFormatter(new Formatador());
          logger.addHandler(fh);
       } catch (SecurityException e) { /* not to do */
       } catch (IOException e) { /* not to do */
       }
       return logger;
   }

    public static String getDateTime() {
        String data = "dd/MM/yyyy";
        String hora = "h:mm - a";
        String data1, hora1;

        java.util.Date agora = new java.util.Date();

        SimpleDateFormat formata = new SimpleDateFormat(data);
        data1 = formata.format(agora);
        formata = new SimpleDateFormat(hora);
        hora1 = formata.format(agora);

        return data1 + " " + hora1;
    }

  
    private static class Formatador extends java.util.logging.Formatter {

        @Override
        public String format(LogRecord record) {
            if (record.getLevel() == Level.FINEST) {
                return record.getLevel() + "  :  "
                        + record.getSourceClassName() + " -:- "
                        + record.getSourceMethodName() + " -:- "
                        + record.getMessage() + "\n";
            } else {
                return record.getLevel() + "  :  "
                        + record.getMessage() + "\n";
            }
        }
    }

    public static void setType(Level _type) {
        type.addElement(_type);
    }

    public static Vector<Level> getType() {
        return type;
    }

    public static String getNameType(int index) {

        Level tp = type.get(index);

        if (tp == Level.ALL) {
            return "ALL";
        }

        if (tp == Level.CONFIG) {
            return "CONFIG";
        }

        if (tp == Level.FINE) {
            return "FINE";
        }

        if (tp == Level.FINER) {
            return "FINER";
        }

        if (tp == Level.FINEST) {
            return "FINEST";
        }

        if (tp == Level.INFO) {
            return "INFO";
        }
        if (tp == Level.OFF) {
            return "OFF";
        }

        if (tp == Level.SEVERE) {
            return "SEVERE";
        }

        if (tp == Level.WARNING) {
            return "WARNING";

        }
        return null;
    }
    public static void close(){
       fh.close();
       logger = null;
    }
}
