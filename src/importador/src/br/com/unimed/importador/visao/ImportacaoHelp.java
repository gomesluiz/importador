/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.visao;

import java.io.File;
import java.net.URL;
import javax.help.HelpBroker;
import javax.help.HelpSet;

/**
 *
 * @author paulo
 */
public class ImportacaoHelp {

    private HelpSet hs;
    private HelpBroker hb;
    private static ImportacaoHelp _instance = null;

    public static synchronized ImportacaoHelp getInstance() {
        if (_instance == null) {
            _instance = new ImportacaoHelp();
        }

        return _instance;
    }

    public void openHelp() {
        // Identify the location of the .hs file

        /*String pathToHs = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();



       if( pathToHs.contains("jar")){
           pathToHs = pathToHs.substring(0,
                   pathToHs.lastIndexOf(File.separatorChar));

           pathToHs += pathToHs + File.separatorChar;
       }

       pathToHs +=  "help" +
               File.separatorChar + "doc" +
               File.separatorChar + "Importador-hs.xml";

       System.out.println(pathToHs); */



        //Create a URL for the location of the help set
        try {
            URL hsURL = new URL("jar:file:importador.jar!/br/com/unimed/importador/help/doc/Importador-hs.xml");

            //hsURL = getClass().getResource(pathToHs);
            hs = new HelpSet(null, hsURL);
        } catch (Exception ee) {
            // Print info to the console if there is an exception
            System.out.println("HelpSet " + ee.getMessage());
            //System.out.println("Help Set " + pathToHs + " not found");
           
            return;
        }

        // Create a HelpBroker object for manipulating the help set
        hb = hs.createHelpBroker();
        //Display help set
        hb.setDisplayed(true);
    }
}
