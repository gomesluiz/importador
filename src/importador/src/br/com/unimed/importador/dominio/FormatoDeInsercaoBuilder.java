package br.com.unimed.importador.dominio;

class FormatoDeInsercaoBuilder extends FormatoDeBancoDeDados{
   
   public FormatoDeInsercaoBuilder() {
      super("R", new StringBuilder ("INSERT INTO"), new StringBuilder("VALUES"));
   }
   public FormatoDeInsercaoBuilder nome(String codigo) {
      if (!temCabecalho){
         this.clausulaPrincipal.append(espaco());
         this.clausulaPrincipal.append(this.prefixoDaTabela).append(codigo);
         this.clausulaPrincipal.append(espaco());
         this.clausulaPrincipal.append(abre());
         this.clausulaSecundaria.append(abre());
         this.temCabecalho = true;
      }
      return this;
   }
   public FormatoDeInsercaoBuilder campo (MascaraDeCampo campo) {
      if (temCampo) {
         this.clausulaPrincipal.append(virgula());
         this.clausulaSecundaria.append(virgula());
      } else {
         this.temCampo = true;
      }
      this.clausulaPrincipal.append(campo.getCodigo());
      this.clausulaSecundaria.append(this.formataValor(campo));
      return this;
   }

   public FormatoDeInsercaoBuilder termina() {
      this.clausulaPrincipal.append(fecha());
      this.clausulaSecundaria.append(fecha());
      return this;
   }
   
}
