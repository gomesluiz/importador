package br.com.unimed.importador.dominio;

class FormatoDeConsultaBuilder extends FormatoDeBancoDeDados{

   public FormatoDeConsultaBuilder() {
      super("R", new StringBuilder ("SELECT * FROM"));
   }
   public FormatoDeConsultaBuilder nome(String codigo) {
      if (!temCabecalho){
         this.clausulaPrincipal.append(espaco());
         this.clausulaPrincipal.append(this.prefixoDaTabela).append(codigo);
         this.clausulaPrincipal.append(espaco());
         this.clausulaPrincipal.append("WHERE");
         this.clausulaPrincipal.append(espaco());
         this.temCabecalho = true;
      }
      return this;
   }
   public FormatoDeConsultaBuilder campo (MascaraDeCampo campo) {
      if (campo.getTipoDeOrigem() == TipoDeOrigem.PARAMETRO){
         if (temCampo) {
            this.clausulaPrincipal.append(espaco())
                                    .append("AND")
                                    .append(espaco());
         }
         this.clausulaPrincipal.append(formataCondicao(campo, "="));
         this.temCampo = true;
      }
      return this;
   }
   public FormatoDeConsultaBuilder termina() {
      this.clausulaPrincipal.append(";");
      return this;
   }
}
