package br.com.unimed.importador.dominio;

public enum TipoDeCampo {

   A, N, NS, AN, ANS, MM, DD, YYYY, HH, MI, SS, DATA1, DATA2, CID, HORA, UF, ML, SL;

   public static String getIndexTipoDeCampo(TipoDeCampo tp) {

      if (tp.equals(TipoDeCampo.A)) {
         return "0";
      }

      if (tp.equals(TipoDeCampo.N)) {
         return "1";
      }

      if (tp.equals(TipoDeCampo.NS)) {
         return "2";
      }

      if (tp.equals(TipoDeCampo.AN)) {
         return "3";
      }

      if (tp.equals(TipoDeCampo.ANS)) {
         return "4";
      }

      if (tp.equals(TipoDeCampo.MM)) {
         return "5";
      }

      if (tp.equals(TipoDeCampo.DD)) {
         return "6";
      }

      if (tp.equals(TipoDeCampo.YYYY)) {
         return "7";
      }

      if (tp.equals(TipoDeCampo.HH)) {
         return "8";
      }

      if (tp.equals(TipoDeCampo.MI)) {
         return "9";
      }

      if (tp.equals(TipoDeCampo.SS)) {
         return "10";
      }

      if (tp.equals(TipoDeCampo.DATA1)) {
         return "11";
      }

      if (tp.equals(TipoDeCampo.DATA2)) {
         return "12";
      }

      if (tp.equals(TipoDeCampo.CID)) {
         return "13";
      }

      if (tp.equals(TipoDeCampo.HORA)) {
         return "14";
      }

      if (tp.equals(TipoDeCampo.UF)) {
         return "15";
      }

      if (tp.equals(TipoDeCampo.ML)) {
         return "90";
      }

      if (tp.equals(TipoDeCampo.SL)) {
         return "91";
      }


      return "0";


   }

   public static TipoDeCampo getTipoDeCampo(int number) {
      switch (number) {
         case 0:
            return TipoDeCampo.A;
         case 1:
            return TipoDeCampo.N;
         case 2:
            return TipoDeCampo.NS;
         case 3:
            return TipoDeCampo.AN;
         case 4:
            return TipoDeCampo.ANS;
         case 5:
            return TipoDeCampo.MM;
         case 6:
            return TipoDeCampo.DD;
         case 7:
            return TipoDeCampo.YYYY;
         case 8:
            return TipoDeCampo.HH;
         case 9:
            return TipoDeCampo.MI;
         case 10:
            return TipoDeCampo.SS;
         case 11:
            return TipoDeCampo.DATA1;
         case 12:
            return TipoDeCampo.DATA2;
         case 13:
            return TipoDeCampo.CID;
         case 14:
            return TipoDeCampo.HORA;
         case 15:
            return TipoDeCampo.UF;
         case 90:
            return TipoDeCampo.ML;
         case 91:
            return TipoDeCampo.SL;

      }
      return TipoDeCampo.A;
   }

   public static String getTipoDeCampoDoBancoDeDados(TipoDeCampo tp) {

      if (tp.equals(TipoDeCampo.A)) {
         return "VARCHAR";
      }

      if (tp.equals(TipoDeCampo.N)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.NS)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.AN)) {
         return "VARCHAR";
      }

      if (tp.equals(TipoDeCampo.ANS)) {
         return "VARCHAR";
      }

      if (tp.equals(TipoDeCampo.MM)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.DD)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.YYYY)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.HH)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.MI)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.SS)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.DATA1)) {
         return "VARCHAR";
      }

      if (tp.equals(TipoDeCampo.DATA2)) {
         return "DATE";
      }

      if (tp.equals(TipoDeCampo.CID)) {
         return "VARCHAR";
      }

      if (tp.equals(TipoDeCampo.HORA)) {
         return "TIME";
      }

      if (tp.equals(TipoDeCampo.UF)) {
         return "CHAR";
      }

      if (tp.equals(TipoDeCampo.ML)) {
         return "DECIMAL";
      }

      if (tp.equals(TipoDeCampo.SL)) {
         return "DECIMAL";
      }
      return "VARCHAR";
   }
}
