package br.com.unimed.importador.dominio;

class FormatoDeTabelaBuilder extends FormatoDeBancoDeDados{
   public FormatoDeTabelaBuilder() {
      super("R", new StringBuilder ("CREATE TABLE"));
   }
   public FormatoDeTabelaBuilder nome(String codigo) {
      if (!temCabecalho){
         this.clausulaPrincipal.append(espaco());
         this.clausulaPrincipal.append(this.prefixoDaTabela).append(codigo);
         this.clausulaPrincipal.append(espaco());
         this.clausulaPrincipal.append(abre());
         this.temCabecalho = true;
      }
      return this;
   }
   public FormatoDeTabelaBuilder campo (MascaraDeCampo campo) {
      if (temCampo) {
         this.clausulaPrincipal.append(virgula());
      } else {
         this.temCampo = true;
      }
      this.clausulaPrincipal.append(formataCampo(campo));
      return this;
   }

   public FormatoDeTabelaBuilder termina() {
      this.clausulaPrincipal.append(fecha());
      this.clausulaPrincipal.append("ENGINE='MYISAM';");
      return this;
   }
}
