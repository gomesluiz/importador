package br.com.unimed.importador.dominio;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.MascaraDeArquivoJpaController;
import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.exceptions.RegistroComTamanhoInvalidoException;
import br.com.unimed.importador.exceptions.RegistroDesconhecidoException;
import br.com.unimed.importador.persistencia.dao.*;
import br.com.unimed.importador.persistencia.io.*;
import br.com.unimed.importador.utils.*;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog.ModalExclusionType;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ProgressMonitor;

public class ControladorDeImportacao {

    private MascaraDeArquivo mascaraDeArquivoDeOrigem = null;
    private DataIOFactory ioLineFactory;
    private InterfaceIODataAccess arquivoDeOrigem;
    private DataAccessFactory mySqlFactory;
    private static ControladorDeImportacao instance = null;
    private static InterfaceJdbcDataAccess arqGenericDao = null;
    private static int contadorDeLinha;
    private static ProgressMonitor barraDeProgresso;
    private static String linhaDoArquivo;
    private static Vector<String> cacheDeTabela = new Vector<String>();
    private Component parent;
    private static final int MAXERRORNUMBER = 1000;
    private boolean verificacaoDeArquivoImportado;
    /*
     * Acessar o Dao Tipo de Campos para criar a tabela
     */
    Map<String, InterfaceJdbcDataAccess> mapTipoCampoDao = null;
    private static int quantidadeDeLinhasDoArquivo = 0;

    public static synchronized ControladorDeImportacao getInstance() {
        if (instance == null) {
            LoggerFile.getLogger().log(Level.INFO, "=== Importador Iniciado em: " + LoggerFile.getDateTime() + "  ===");
            instance = new ControladorDeImportacao();
        }
        return instance;
    }

    private ControladorDeImportacao() {
        mySqlFactory = DataAccessFactory.getDAOFactory(DataAccessFactory.MYSQL);
        LoggerFile.setType(Level.INFO);
        LoggerFile.setType(Level.SEVERE);
        LoggerFile.setType(Level.WARNING);
    }

    /**
     * Retorna um vetor de string com os códigos todos tipos de arquivos
     * armazenados no repostório.
     *
     * Descrição: O método cria um vetor de objetos String, a partir das
     * mascaras de arquivos armazenadas no repositório e versao informada,
     * contendo em cada posição o código da máscara de arquivo.
     *
     * Pré-condição: - existe a coleção de máscaras de arquivos.
     *
     * @return códigos das máscaras de arquivos.
     *
     * @author Affirmare Tecnologia da Informação.
     *
     */
    public List<String> obterTiposDeArquivo(int numVersao) {
        List<br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivo> codigos = null;
        List<String> codigo = new ArrayList<String>();

        MascaraDeArquivoJpaController mascaraDeArquivoController =
                new MascaraDeArquivoJpaController();

        codigos = mascaraDeArquivoController.findMascaraDeArquivoEntities();
        br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivo mascara;
        Iterator<br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivo> itMascaraDeArquivo = codigos.listIterator();

        while (itMascaraDeArquivo.hasNext()) {
            mascara = itMascaraDeArquivo.next();
            if (mascara.getMascaraDeArquivoPK().getNumVersaoMascaraDeArquivo() == numVersao) {
                codigo.add(mascara.getMascaraDeArquivoPK().getCodMascaraDeArquivo());
            }
        }

        return codigo;
    }

    /**
     * Retorna um hashmap contendo os campos que são do tipo parâmetro para um
     * determinado arquivo.
     *
     * Descrição: O método retorna um HashMap contendo as máscaras de campos que
     * são definidas como parâmetros quando a máscara é definida no repositório.
     *
     * Pré-condição: - existe a coleção de máscaras de campos. - o código
     * passado como parâmetro é um código de uma máscara existente.
     *
     * @return máscaras de campos do tipo parâmetro do usuário.
     *
     * @author Affirmare Tecnologia da Informação.
     *
     */
    public List<MascaraDeCampo> obterParametrosDoUsuario(
            final String codigo, final int versao) {

        FabricaDeMascaraDeArquivo fabricaDeMascaraDeArquivo = new FabricaDeMascaraDeArquivo();
        mascaraDeArquivoDeOrigem = fabricaDeMascaraDeArquivo.constroiMascaraDeArquivoPara(codigo, versao);

        return mascaraDeArquivoDeOrigem.getParametrosDoUsuario();
    }

    public void ImportarArquivo(String nomeDoArquivoDeOrigem, final List<MascaraDeCampo> parametrosDoUsuario, boolean apagarArquivoNoDestino) {
        this.parent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        quantidadeDeLinhasDoArquivo = contaLinhasDoArquivo(nomeDoArquivoDeOrigem);
        mascaraDeArquivoDeOrigem.atualizaParametrosDoUsuario(parametrosDoUsuario);
        arqGenericDao = mySqlFactory.getObjectDAO(MySqlDataAccessFactory.Generic);
        if (apagarArquivoNoDestino) {
            apagarArquivoNoDestino();
        }
        this.parent.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        iniciarImportacao(nomeDoArquivoDeOrigem);
    }

    private int contaLinhasDoArquivo(String nomeDoArquivo) {
        int resultado = 0;
        try {
            preparaLeituraDoArquivo(nomeDoArquivo);
            arquivoDeOrigem.connect(arquivoDeOrigem.READ_ASC);
            while (arquivoDeOrigem.readLine() != null) {
                resultado += 1;
            }
            arquivoDeOrigem.closeConnection(arquivoDeOrigem.READ_ASC);
        } catch (IOException ex) {
            Logger.getLogger(ControladorDeImportacao.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
        return resultado;
    }

    private int iniciarImportacao(final String nomeDoArquivoDeOrigem) {
        Thread l = new Thread(new Runnable() {

            public void run() {
                double tempoDeInicio, tempoDeTermino;

                MascaraDeRegistro registro = null;
                contadorDeLinha = 0;
                verificacaoDeArquivoImportado = false;
                tempoDeInicio = System.currentTimeMillis();

                BarraDeProgressoBuilder barraDeProgressoBuilder = new BarraDeProgressoBuilder();
                barraDeProgressoBuilder.parente(parent);
                barraDeProgressoBuilder.mensagem(nomeDoArquivoDeOrigem);
                barraDeProgressoBuilder.min(0);
                barraDeProgressoBuilder.max(quantidadeDeLinhasDoArquivo);
                barraDeProgresso = barraDeProgressoBuilder.geraBarraDeProgresso();

                try {
                    preparaLeituraDoArquivo(nomeDoArquivoDeOrigem);
                    arquivoDeOrigem.connect(arquivoDeOrigem.READ_ASC);
                    ((GenericDAO) arqGenericDao).abrirConexao();


                    while (((linhaDoArquivo = arquivoDeOrigem.readLine()) != null)
                            && !(barraDeProgresso.isCanceled())) {

                        contadorDeLinha += 1;
                        atualizaBarraDeProgresso(contadorDeLinha, quantidadeDeLinhasDoArquivo);
                        try {
                            registro = mascaraDeArquivoDeOrigem.obtemRegistroComValores(linhaDoArquivo);
                            if (!verificacaoDeArquivoImportado) {
                                try {
                                    if (arquivoJaFoiImportado(registro)) {
                                        registraNoLog(Level.SEVERE, contadorDeLinha, String.format("O arquivo %s já foi importado", nomeDoArquivoDeOrigem));
                                        ((GenericDAO) arqGenericDao).incErrorCount();
                                        break;
                                    }
                                    verificacaoDeArquivoImportado = true;
                                } catch (QueryException queryException) {
                                    registraNoLog(Level.SEVERE, contadorDeLinha, queryException.getMessage());
                                    ((GenericDAO) arqGenericDao).incErrorCount();
                                }
                            }
                        } catch (RegistroDesconhecidoException registroDesconhecidoException) {
                            registraNoLog(Level.WARNING, contadorDeLinha, registroDesconhecidoException.getMessage());
                            ((GenericDAO) arqGenericDao).incErrorCount();
                            continue;
                        } catch (RegistroComTamanhoInvalidoException registroComTamanhoInvalidoException) {
                            registraNoLog(Level.SEVERE, contadorDeLinha, registroComTamanhoInvalidoException.getMessage());
                            ((GenericDAO) arqGenericDao).incErrorCount();
                            continue;
                        }

                        if (!tabelaEstaNoCache(registro.getCodigo())) {
                            if (!tabelaEstaNoBancoDeDados(registro.getCodigo())) {
                                criaTabela(registro);
                            }
                            registraTabelaNoCache(registro.getCodigo());
                        }
                        resolvePendenciaDeAlteracao(registro);
                        insereRegistro(registro);
                    }
                } catch (IOException ex) {
                    LoggerFile.getLogger().log(Level.SEVERE, ex.getMessage());
                }

                ((GenericDAO) arqGenericDao).fecharConexao();
                arquivoDeOrigem.closeConnection(arquivoDeOrigem.READ_ASC);
                tempoDeTermino = System.currentTimeMillis();
                try {
                    Proper.loadFile("base.ini");
                } catch (IOException ex) {
                    Logger.getLogger(ControladorDeImportacao.class.getName()).log(Level.SEVERE, null, ex);
                }

                apresentaRelatorio(tempoDeTermino, tempoDeInicio);

                if (((GenericDAO) arqGenericDao).getErrorCount() >= MAXERRORNUMBER) {

                 JOptionPane.showMessageDialog(null,
                            "Foram encontrados muitos problemas !\n"
                          
                            + "Abra o arquivo de log\n"
                            + "em um editor de sua preferência no caminho: "
                            + Proper.getValue("error_log_path") + "\n");
                }

                /*    if (usuOpcao == JOptionPane.NO_OPTION) {

                        LogViewer log = new LogViewer(Proper.getValue("error_log_path"));
                        log.setVisible(true);
                        log.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
                    }
                } else {
                    LogViewer log = new LogViewer(Proper.getValue("error_log_path"));
                    log.setVisible(true);
                    log.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
                } */

                ((GenericDAO) arqGenericDao).setErrorCount(0);
            }

            private void apresentaRelatorio(double tempoDeTermino, double tempoDeInicio) throws HeadlessException {
                String message = "Processamento Finalizado em " + (tempoDeTermino - tempoDeInicio) / 1000 + " segundo(s)";
                message += "\nErros Encontrados: " + ((GenericDAO) arqGenericDao).getErrorCount();
               
                JOptionPane.showMessageDialog(null, message, "Informações", +JOptionPane.INFORMATION_MESSAGE);
                LoggerFile.getLogger().log(Level.INFO, "Processado em: " + (tempoDeTermino - tempoDeInicio) / 1000 + " segundo(s)");
                LoggerFile.getLogger().log(Level.INFO, "Erros Encontrados: " + ((GenericDAO) arqGenericDao).getErrorCount());
                LoggerFile.getLogger().log(Level.INFO, "=== Fim do Processamento ===");
                LoggerFile.close();

            }
        });
        l.start();
        return contadorDeLinha;
    }

    private void insereRegistro(MascaraDeRegistro registro) {
        ((GenericDAO) arqGenericDao).inserir(registro.formataParaInsercao());

    }

    private void criaTabela(MascaraDeRegistro registro) {
        if (((GenericDAO) arqGenericDao).execute(registro.formataParaTabela())) {
            registraNoLog(Level.INFO, 0, String.format("Tabela %s criada.", registro.getCodigo()));
            registraTabelaNoCache(registro.getCodigo());
            //resolverPendenciasDeAlteracao(registro);
        }
    }

    private void registraNoLog(Level nivel, int linha, String msg) {
        LoggerFile.getLogger().log(nivel, (linha > 0) ? String.format("Linha [%d] %s ", linha, msg)
                : String.format("%s ", msg));
    }

    private void registraTabelaNoCache(String tabela) {
        cacheDeTabela.addElement(tabela);
    }

    private boolean tabelaEstaNoBancoDeDados(String tabela) {
        return ((GenericDAO) arqGenericDao).isTableExist(String.format("R%s", tabela));
    }

    private void atualizaBarraDeProgresso(int atual, int max) {
        String msg = String.format("Lendo linha %d De % d  ", atual, max);
        barraDeProgresso.setProgress(atual);
        barraDeProgresso.setNote(msg);
        if (atual > barraDeProgresso.getMaximum()) {
            barraDeProgresso.close();
        }
    }

    private Boolean tabelaEstaNoCache(String tabela) {
        return cacheDeTabela.contains(tabela);
    }

    private void preparaLeituraDoArquivo(String nomeDoArquivoDeOrigem) {
        // Especifica o tipo de operação (read / write)
        ioLineFactory =
                DataIOFactory.getIOFactory(DataIOFactory.READBYTE);

        // arquivoDeOrigem permite setar o arquivo e conectar fisicamente e fechar a conexão
        arquivoDeOrigem = ioLineFactory.getIO();

        //definir o arquivo;
        try {
            arquivoDeOrigem.setFile(new File(nomeDoArquivoDeOrigem));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControladorDeImportacao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void apagarArquivoNoDestino() {

        Set<String> chaves = mascaraDeArquivoDeOrigem.getMascarasDeRegistros().keySet();
        ((GenericDAO) arqGenericDao).abrirConexao();
        for (String chave : chaves) {
            MascaraDeRegistro mr = mascaraDeArquivoDeOrigem.getMascarasDeRegistros().get(chave);
            if (((GenericDAO) arqGenericDao).isTableExist(String.format("R%s", mr.getCodigo()))) {
                ((GenericDAO) arqGenericDao).remover(mr.formataParaRemocao());
            }
        }
        ((GenericDAO) arqGenericDao).fecharConexao();
    }

    private void resolvePendenciaDeAlteracao(MascaraDeRegistro registro) {
        String pendenciasFormatadas = registro.formataParaAlteracao();
        if (pendenciasFormatadas.isEmpty()) {
            return;
        }

        PendenciaDeAlteracaoDAO pendenciaDeAlteracaoDAO = PendenciaDeAlteracaoDAO.getInstance();
        boolean sucesso = ((GenericDAO) arqGenericDao).execute(pendenciasFormatadas);

        if (sucesso == true) {
            registro.mudaEstadoDasPendenciasDeAlteracaoPara(true);
            pendenciaDeAlteracaoDAO.abrirConexao();
            try {
                pendenciaDeAlteracaoDAO.mudaEstadoDasPendenciasDeAlteracaoParaResolvida(registro.getCodigo(), registro.getVersao());
            } catch (QueryException queryException) {
                LoggerFile.getLogger().log(Level.SEVERE, queryException.getMessage());
                ((GenericDAO) arqGenericDao).incErrorCount();
            }
        }
        pendenciaDeAlteracaoDAO.fecharConexao();
    }

    public void setParent(Component parent) {
        this.parent = parent;
    }

    private boolean arquivoJaFoiImportado(MascaraDeRegistro mr) {

        boolean resultado = false;

        if (((GenericDAO) arqGenericDao).isTableExist(String.format("R%s", mr.getCodigo()))) {
            if (((GenericDAO) arqGenericDao).existe(mr.formataParaConsulta())) {
                resultado = true;
            }
        }
        return resultado;
    }
}
