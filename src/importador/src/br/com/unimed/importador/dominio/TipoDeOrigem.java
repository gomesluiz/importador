/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.dominio;

/**
 *
 * @author luiz
 */
public enum TipoDeOrigem {

    PARAMETRO, REGISTRO;

    public static TipoDeOrigem getTipoDeOrigem(int number) {

        switch (number) {
            case 0:
                return TipoDeOrigem.PARAMETRO;
            case 1:
                return TipoDeOrigem.REGISTRO;

        }

        return null;

    }
}
