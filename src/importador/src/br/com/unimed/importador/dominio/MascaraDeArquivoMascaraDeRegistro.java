package br.com.unimed.importador.dominio;


public class MascaraDeArquivoMascaraDeRegistro {
      private String codigoDaMascaraDeArquivo;
      private int    numeroDeversaoDaMascaraDeArquivo;
      private String codigoDaMascaraDeRegistro;
      private int    numeroDeversaoDaMascaraDeRegistro;
      private int    numeroSequencialRegistro;

   public MascaraDeArquivoMascaraDeRegistro(String codigoDaMascaraDeArquivo
           , int numeroDeversaoDaMascaraDeArquivo
           , String codigoDaMascaraDeRegistro
           , int numeroDeversaoDaMascaraDeRegistro
           , int numeroSequencialRegistro) {
      this.codigoDaMascaraDeArquivo = codigoDaMascaraDeArquivo;
      this.numeroDeversaoDaMascaraDeArquivo = numeroDeversaoDaMascaraDeArquivo;
      this.codigoDaMascaraDeRegistro = codigoDaMascaraDeRegistro;
      this.numeroDeversaoDaMascaraDeRegistro = numeroDeversaoDaMascaraDeRegistro;
      this.numeroSequencialRegistro = numeroSequencialRegistro;
   }

   public String getCodigoDaMascaraDeArquivo() {
      return codigoDaMascaraDeArquivo;
   }

   public String getCodigoDaMascaraDeRegistro() {
      return codigoDaMascaraDeRegistro;
   }

   public int getNumeroDeversaoDaMascaraDeArquivo() {
      return numeroDeversaoDaMascaraDeArquivo;
   }

   public int getNumeroDeversaoDaMascaraDeRegistro() {
      return numeroDeversaoDaMascaraDeRegistro;
   }

   public int getNumeroSequencialRegistro() {
      return numeroSequencialRegistro;
   }
}
