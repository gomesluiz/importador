package br.com.unimed.importador.dominio;

import java.awt.Component;
import javax.swing.ProgressMonitor;

public class BarraDeProgressoBuilder {

   private Component parente;
   private String mensagem;
   private String nota;
   int min;
   int max;

   public BarraDeProgressoBuilder() {
   }

   public BarraDeProgressoBuilder parente(Component parente) {
      this.parente = parente;
      return this;
   }

   public BarraDeProgressoBuilder mensagem(String mensagem) {
      this.mensagem = mensagem;
      return this;
   }

   public BarraDeProgressoBuilder nota(String nota) {
      this.nota = nota;
      return this;
   }

   public BarraDeProgressoBuilder min(int min) {
      this.min = min;
      return this;
   }

   public BarraDeProgressoBuilder max(int max) {
      this.max = max;
      return this;
   }

   public ProgressMonitor geraBarraDeProgresso() {
      return new ProgressMonitor(parente, mensagem, nota, min, max);
   }
}
