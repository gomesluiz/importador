package br.com.unimed.importador.dominio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe concreta que representa máscara de um campo e também o valor que poderá
 * ser atribuído a ele. Esta mascara é utilizada para se criar uma máscara de resgistro.
 *
 * @author Affirmare Tecnologia da Informação
 *
 * @version %I%, %G%
 *
 * @since 1.0
 *
 */

public class MascaraDeCampo{

    private int posicaoInicial;
    private int posicaoFinal;
    private String codigo;
    private TipoDeCampo tipoDeCampo;
    private int tamanho;
    private int decimais;
    private TipoDeUso tipoDeUso;
    private TipoDeOrigem tipoDeOrigem;
    private boolean contemTipoRegistro;
    private boolean contemChavePrimaria;
    private String valor;
    private List<PendenciaDeAlteracao> pendencias;

    /**
     * Método construtor para inicialiar os atributos da classe.
     *
     * @param posicaoInicial    posicão inicial do campo no registro
     * @param posicaoFinal      posição final do campo no registro
     * @param codigo              codigo do campo
     * @param tipoDeCampo       tipo do campo
     * @param tamanho           tamanho do campo
     * @param decimais          decimais do campo, se houver
     * @param tipoDeUso         define se o campo é obrigatório ou não
     * @param tipoDeOrigem      define a o tipo de origem do campo
     * @param contemTipoDeRegistro define se o campo contém ou não
     *                                      o tipo do registro
     */
    public MascaraDeCampo(String codigo, int posicaoInicial, int posicaoFinal,
            int tamanho, int decimais, TipoDeOrigem tipoDeOrigem,
            TipoDeCampo tipoDeCampo, TipoDeUso tipoDeUso, boolean contemTipoDeRegistro,
            boolean contemChavePrimaria, boolean contemNumeroDeVersao) {
        this.codigo = codigo.toUpperCase();
        this.posicaoInicial = posicaoInicial;
        this.posicaoFinal = posicaoFinal;
        this.tipoDeCampo = tipoDeCampo;
        this.tamanho = tamanho;
        this.decimais = decimais;
        this.tipoDeUso = tipoDeUso;
        this.tipoDeOrigem = tipoDeOrigem;
        this.contemTipoRegistro = contemTipoDeRegistro;
        this.contemChavePrimaria = contemChavePrimaria;
        this.pendencias = Collections.emptyList();
    }

    /**
     * Retorna as decimais de um campo, se houver.
     *
     * @return decimais do campo.
     */
    public int getDecimais() {
        return decimais;
    }

    /**
     * Define as decimais de um campo.
     *
     * @param decimais  número de decimais de campo
     */
    private void setDecimais(int decimais) {
        this.decimais = decimais;
    }

    /**
     * Retorna o codigo de um campo.
     *
     * @return codigo do campo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Retorna a posição final de um campo.
     *
     * @return sequencia final do campo.
     */
    public int getPosicaoFinal() {
        return posicaoFinal;
    }

    /**
     * Retorna a posição inicial de um campo.
     *
     * @return posição inicial do campo.
     */
    public int getPosicaoInicial() {
        return posicaoInicial;
    }

    /**
     * Retorna o tamanho de um campo.
     *
     * @return tamanho do campo.
     */
    public int getTamanho() {
        return tamanho;
    }

    /**
     * Retorna o tipo de um campo.
     *
     * @return tipo do campo (NUMERICO, DATA ou CARACTER)
     */
    public TipoDeCampo getTipoDeCampo() {
        return tipoDeCampo;
    }

    /**
     * Retorna o tipo de origem do campo.
     *
     * @return tipo de origem do campo (PARAMETRO ou REGISTRO)
     */
    public TipoDeOrigem getTipoDeOrigem() {
        return tipoDeOrigem;
    }

    /**
     * Retorna o tipo de uso do campo.
     *
     * @return tipo de uso do campo (MANDATORIO ou OPCIONAL)
     */
    public TipoDeUso getTipoDeUso() {
        return tipoDeUso;
    }

    /**
     * Retorna true se o campo contém o tipo do registro, caso contrário false.
     *
     * @return se o campo contém, true, ou não o tipo do registro.
     */
    public boolean contemTipoRegistro() {
        return contemTipoRegistro;
    }

    /**
     * Retorna o valor de um campo para um registro.
     *
     * @return  valor do campo.
     */
    public String getValor() {
        return valor;
    }

    /**
     * Atribui um valor a um campo.
     *
     * @param valor
     */
    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * Retorna se o campo faz parte ou nao da chave primaria.
     *
     * @return verdadeiro se o campo faz parte da chave primaria,
     *          se nao falso.
     */
    public boolean contemChavePrimaria() {
        return contemChavePrimaria;
    }

     public void adicionarPendencia(final PendenciaDeAlteracao pendencia) {
        if (this.pendencias.isEmpty()) {
            this.pendencias = new ArrayList<PendenciaDeAlteracao>();
        }
        this.pendencias.add(pendencia);
    }

    /**
     * Retorna a coleção de pendências do campo.
     *
     * @return pendencias
     */
    public List<PendenciaDeAlteracao> getPendenciasDeAlteracao() {
        return pendencias;
    }

   /**
    * Altera o estado de resolvido das pendências para um novo estado.
    *
    * @param novoEstado novo estado das pendencias.
    */
   void mudarEstadoDasPendenciasPara(boolean novoEstado) {
        for (PendenciaDeAlteracao pendencia : getPendenciasDeAlteracao()){
            pendencia.setIndicadorDePendenciaResolvida(novoEstado);
         }
   }
}
