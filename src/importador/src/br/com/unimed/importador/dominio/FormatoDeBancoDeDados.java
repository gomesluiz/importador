/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.dominio;

public class FormatoDeBancoDeDados {

   protected StringBuilder clausulaPrincipal;
   protected StringBuilder clausulaSecundaria;
   protected boolean temCabecalho;
   protected boolean temCampo;
   protected String prefixoDaTabela;

   protected FormatoDeBancoDeDados(String prefixoDaTabela, StringBuilder clausulaPrincipal) {
      this.prefixoDaTabela       = prefixoDaTabela;
      this.clausulaPrincipal     = clausulaPrincipal;
      this.clausulaSecundaria    = null;
      this.temCabecalho    = false;
      this.temCampo        = false;
   }

   protected FormatoDeBancoDeDados(String prefixoDaTabela, StringBuilder clausulaPrincipal,
           StringBuilder clausulaSecundaria) {
      this.prefixoDaTabela    = prefixoDaTabela;
      this.clausulaPrincipal  = clausulaPrincipal;
      this.clausulaSecundaria = clausulaSecundaria;
      this.temCabecalho = false;
      this.temCampo = false;

   }

   protected final String formataCampo(MascaraDeCampo mc) {
      StringBuilder resultado = new StringBuilder();

      resultado.append(mc.getCodigo())              
              .append(espaco())
              .append(TipoDeCampo.getTipoDeCampoDoBancoDeDados(mc.getTipoDeCampo()));

      if (mc.getDecimais() > 0) {
         resultado.append(abre())
                 .append(mc.getTamanho() + mc.getDecimais())
                 .append(virgula()).append(mc.getDecimais())
                 .append(fecha());
      } else {
         switch (mc.getTipoDeCampo()) {
            case DATA2:
            case HORA:
               break;
            default:
               resultado.append(abre()).append(mc.getTamanho()).append(fecha());
         }
      }
      return resultado.toString();
   }

   protected final String formataValor(MascaraDeCampo mc) {
      StringBuilder resultado = new StringBuilder();
      switch (mc.getTipoDeCampo()) {
         case N:
         case NS:
            resultado.append(formataValorDecimal(mc.getValor(), mc.getDecimais()));
            break;
         case MM:
         case DD:
         case YYYY:
         case HH:
         case MI:
         case SS:
         case ML:
         case SL:
            resultado.append(mc.getValor());
            break;
         default:
            resultado.append(" \"").append(mc.getValor()).append("\"");
      }

      return resultado.toString();
   }

   protected final String formataValorDecimal(String valor, int decimais) {
      StringBuilder resultado = new StringBuilder();
      if (decimais == 0) {
         resultado.append(valor);
      } else {
         double divisor = Math.pow(10, decimais);
         double valorComDecimais = Double.valueOf(valor.trim()).doubleValue() / divisor;
         resultado.append(valorComDecimais);
      }
      return resultado.toString();
   }

   protected final String formataCondicao(MascaraDeCampo mc, String operador) {
      StringBuilder resultado = new StringBuilder();

      resultado.append(mc.getCodigo())
               .append(operador)
               .append(formataValor(mc));

      return resultado.toString();
   }

   protected final char abre() {
      return '(';
   }

   protected final char espaco() {
      return ' ';
   }

   protected final char fecha() {
      return ')';
   }

   
   @Override
   public String toString() {
      if (clausulaSecundaria != null){
         clausulaPrincipal.append(clausulaSecundaria);
      }
      return clausulaPrincipal.toString();
   }

   protected final char virgula() {
      return ',';
   }

}
