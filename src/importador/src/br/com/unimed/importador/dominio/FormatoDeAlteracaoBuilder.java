package br.com.unimed.importador.dominio;


class FormatoDeAlteracaoBuilder extends FormatoDeBancoDeDados {

   public FormatoDeAlteracaoBuilder() {
      super("R", new StringBuilder("ALTER TABLE"));
   }

   public FormatoDeAlteracaoBuilder nome(String codigo) {
      if (!temCabecalho) {
         this.clausulaPrincipal.append(espaco());
         this.clausulaPrincipal.append(this.prefixoDaTabela).append(codigo);
         this.clausulaPrincipal.append(espaco());
         this.temCabecalho = true;
      }
      return this;
   }

   public FormatoDeAlteracaoBuilder campo(MascaraDeCampo campo) {
      String pendenciaFormatada;
      pendenciaFormatada = formataPendencia(campo);
      if (!pendenciaFormatada.isEmpty()) {
         this.clausulaPrincipal.append(pendenciaFormatada);
      }
      return this;
   }

   private String formataPendencia(MascaraDeCampo campo) {
      StringBuilder resultado = new StringBuilder();
      
      for (PendenciaDeAlteracao pendencia : campo.getPendenciasDeAlteracao()) {
         if ((pendencia.isIndicadorDePendenciaResolvida()) ||
             (pendencia.getCodigoTipoDePendencia() == TipoDePendencia.EXCLUSAO)){
            continue;
         }
         if (temCampo) {
            resultado.append(virgula());
         } else {
            temCampo = true;
         }
         switch (pendencia.getCodigoTipoDePendencia()) {
            case INCLUSAO:
               resultado.append("ADD COLUMN").append(espaco()).append(formataCampo(campo));
               break;
            case ALTERACAO:           
               resultado.append("MODIFY").append(espaco()).append(formataCampo(campo));
               break;
         }
      }
      return resultado.toString();
   }

   public FormatoDeAlteracaoBuilder termina() {
      if (temCampo) {
         this.clausulaPrincipal.append(";");
      } else {
         this.clausulaPrincipal = new StringBuilder();
      }
      return this;
   }
}
