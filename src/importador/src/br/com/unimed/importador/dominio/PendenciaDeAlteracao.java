package br.com.unimed.importador.dominio;

public class PendenciaDeAlteracao {
   private int numeroSequencial;
   private TipoDePendencia codigoTipoDePendencia;
   private boolean indicadorDePendenciaResolvida;

   public PendenciaDeAlteracao(int numeroSequencial, int codigoTipoDePendencia
           , boolean indicadorDePendenciaResolvida) {
      this.numeroSequencial = numeroSequencial;
      this.codigoTipoDePendencia = TipoDePendencia.getTipoDePendencia(codigoTipoDePendencia);
      this.indicadorDePendenciaResolvida = indicadorDePendenciaResolvida;
   }

   public TipoDePendencia getCodigoTipoDePendencia() {
      return codigoTipoDePendencia;
   }

   public void setCodigoTipoDePendencia(int codigoTipoDePendencia) {
      this.codigoTipoDePendencia = TipoDePendencia.getTipoDePendencia(codigoTipoDePendencia);
   }

   public boolean isIndicadorDePendenciaResolvida() {
      return indicadorDePendenciaResolvida;
   }

   public void setIndicadorDePendenciaResolvida(boolean indicadorDePendenciaResolvida) {
      this.indicadorDePendenciaResolvida = indicadorDePendenciaResolvida;
   }

   public int getNumeroSequencial() {
      return numeroSequencial;
   }

   public void setNumeroSequencial(int numeroSequencial) {
      this.numeroSequencial = numeroSequencial;
   }
}
