package br.com.unimed.importador.dominio;

public enum TipoDePendencia {

   INCLUSAO, ALTERACAO, EXCLUSAO;

   public static TipoDePendencia getTipoDePendencia(int numero) {

      switch (numero) {
         case 1:
            return TipoDePendencia.INCLUSAO;
         case 2:
            return TipoDePendencia.ALTERACAO;
         case 3:
            return TipoDePendencia.EXCLUSAO;
      }
      return null;
   }
}
