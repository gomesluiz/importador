package br.com.unimed.importador.dominio;

import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.persistencia.dao.*;
import br.com.unimed.importador.utils.LoggerFile;
import java.util.List;
import java.util.logging.Level;

public class FabricaDeMascaraDeArquivo {
  MascaraDeArquivo constroiMascaraDeArquivoPara(String codigo, int versao) {
      MascaraDeArquivoDAO respositorioDeMascaraDeArquivo = MascaraDeArquivoDAO.getInstance();
      MascaraDeRegistroDAO respositorioDeMascaraDeRegistro = MascaraDeRegistroDAO.getInstance();
      MascaraDeCampoDAO respositorioDeMascaraDeCampo = MascaraDeCampoDAO.getInstance();
      PendenciaDeAlteracaoDAO respositorioDePendenciaDeAlteracao = PendenciaDeAlteracaoDAO.getInstance();
      MascaraDeArquivoMascaraDeRegistroDAO respositorioDeMascaraDeArquivoMascaraDeRegistro = MascaraDeArquivoMascaraDeRegistroDAO.getInstance();
      try {
         respositorioDeMascaraDeArquivo.abrirConexao();
         MascaraDeArquivo mascaraDeArquivo = respositorioDeMascaraDeArquivo.listaPorChavePrimaria(codigo, versao);
         respositorioDeMascaraDeArquivo.fecharConexao();

         // recupera as mascara de registros associadas a mascara de arquivo
         respositorioDeMascaraDeArquivoMascaraDeRegistro.abrirConexao();
         List<MascaraDeArquivoMascaraDeRegistro> mascarasDeArquivosMascarasDeRegistros = respositorioDeMascaraDeArquivoMascaraDeRegistro.listaPorArquivoEVersao(codigo, versao);
         respositorioDeMascaraDeArquivoMascaraDeRegistro.fecharConexao();

         // adiciona as mascaras de registros associadas a mascara de arquivo
         for (MascaraDeArquivoMascaraDeRegistro ma : mascarasDeArquivosMascarasDeRegistros) {
            respositorioDeMascaraDeRegistro.abrirConexao();
            MascaraDeRegistro mascaraDeRegistro = respositorioDeMascaraDeRegistro.listaPorChavePrimaria(ma.getCodigoDaMascaraDeRegistro(), ma.getNumeroDeversaoDaMascaraDeRegistro());
            respositorioDeMascaraDeRegistro.fecharConexao();

            //recupera as mascaras de campos associadas a mascara de registro
            respositorioDeMascaraDeCampo.abrirConexao();
            List<MascaraDeCampo> mascarasDeCampos = respositorioDeMascaraDeCampo.listaPorRegistroEVersao(mascaraDeRegistro.getCodigo(), mascaraDeRegistro.getVersao());
            respositorioDeMascaraDeCampo.fecharConexao();

            //adiciona as mascaras de campos associadas a mascara de registro
            for (MascaraDeCampo mc : mascarasDeCampos) {
               //recupera as pendencias de alteracao associadas a mascara de campo.
               respositorioDePendenciaDeAlteracao.abrirConexao();
               List<PendenciaDeAlteracao> pendenciasDeAlteracoes = respositorioDePendenciaDeAlteracao.listaNaoResolvidas(mascaraDeRegistro.getCodigo(), mascaraDeRegistro.getVersao(),
                       mc.getCodigo());
               respositorioDePendenciaDeAlteracao.fecharConexao();

               //adiciona as pendencias de alteracao a mascara de campo.
               for(PendenciaDeAlteracao pa : pendenciasDeAlteracoes){
                  mc.adicionarPendencia(pa);
               }
               mascaraDeRegistro.adicionaCampo(mc);
            }
            mascaraDeArquivo.adicionaMascaraDeRegistro(mascaraDeRegistro);
         }
         return mascaraDeArquivo;

      } catch (QueryException queryException) {
         System.out.println(queryException.getMessage());
         LoggerFile.getLogger().log(Level.SEVERE, queryException.getMessage());

         System.exit(1);
      } catch (NumberFormatException numberFormatException) {
         LoggerFile.getLogger().log(Level.SEVERE, numberFormatException.getMessage());
         System.exit(1);
      }
      return null;
   }
}
