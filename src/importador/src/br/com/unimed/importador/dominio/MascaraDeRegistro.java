package br.com.unimed.importador.dominio;

/**
 * Classe mantém a máscara de um registro e as máscaras de campos associadas
 * a ela. Esta mascara é utilizada para se criar uma máscara de resgistro.
 *
 * @author Affirmare Tecnologia da Informação
 *
 * @version %I%, %G%
 *
 * @since 1.0
 *
 */
import br.com.unimed.importador.exceptions.RegistroComTamanhoInvalidoException;
import br.com.unimed.importador.exceptions.RegistroComTamanhoMaiorException;
import br.com.unimed.importador.exceptions.RegistroComTamanhoMenorException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MascaraDeRegistro {

   private String codigo;
   private int versao;
   private String descricao;
   private int tamanho;
   private List<MascaraDeCampo> campos;
   private MascaraDeCampo identificador;

   /**
    * Construtor da classe MascaraDeRegistro.
    *
    * @param codigo    codigo do registro que servirá como identificador
    * @param descricao         descrição do codigo de registro.
    */
   public MascaraDeRegistro(final String codigo, int versao, final String descricao) {
      this.codigo       = codigo;
      this.descricao    = descricao;
      this.versao       = versao;
      this.tamanho      = 0;
      this.campos       = Collections.emptyList();
   }

   /**
    * Associa uma instância de máscara de campo a uma instância de
    * máscara de registro. Além disso, adiciona o tamanho do campo ao
    * tamanho do registro e atualiza o atributo identificador com
    * a referência a campo se o método contemTipoRegistro do mesmo
    * retornar true.
    *
    * @param campo     instância da máscara de campo a ser associada.
    */
   public void adicionaCampo(final MascaraDeCampo campo) {

      if (this.campos.isEmpty()) {
         this.campos = new ArrayList<MascaraDeCampo>();
      }
      this.campos.add(campo);

      atualizaTamanhoDoRegistro();
      identificaRegistro(campo);
   }

   /**
    * Define um campo como identificador do registro.
    *
    * @param campo
    */
   private void identificaRegistro(final MascaraDeCampo campo) {
      if (campo.contemTipoRegistro() == true) {
         this.identificador = campo;
      }
   }

   /**
    * Atualiza o tamanho do registro com o maior tamanho dos campos do
    * registro
    *
    *
    * @param tamanho novo tamanho do registro.
    */
   private void atualizaTamanhoDoRegistro() {
      this.tamanho = 0;
      for (MascaraDeCampo mc: campos) {
         if (getTamanho() < mc.getPosicaoFinal()) {
            this.tamanho = mc.getPosicaoFinal();
         }
      }
   }

   /**
    * Retorna um vetor de Campos com metadados e valores lidos dos arquivos.
    *
    * @param linhaDoArquivo    linha de texto de um arquivo.
    *
    * @exception RegistroComTamanhoInvalidoException
    *
    * @return a própria instância de registro com os atributos dos campos
    * preenchidos com o valores da linha de arquivo.
    */
   public MascaraDeRegistro obtemRegistroComValores(final String linhaDoArquivo)
           throws RegistroComTamanhoInvalidoException {
      int tamanhoDoRegistro = getTamanho();
      String linha = String.format("%1$-" + tamanhoDoRegistro + "s", linhaDoArquivo);
      int tamanhoDaLinha = linha.length();
      if (tamanhoDaLinha == tamanhoDoRegistro) {
         try {
            for (MascaraDeCampo mc : campos) {
               if (mc.getTipoDeOrigem() == TipoDeOrigem.REGISTRO) {
                  mc.setValor(linha.substring(mc.getPosicaoInicial() - 1, mc.getPosicaoFinal()));
               }
            }
         } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            throw new RegistroComTamanhoInvalidoException(indexOutOfBoundsException);
         }
      } else {
         if (tamanhoDaLinha < tamanhoDoRegistro) {
            throw new RegistroComTamanhoMenorException();
         } else {
            throw new RegistroComTamanhoMaiorException();
         }
      }
      return this;
   }

   public String formataParaTabela() {
      FormatoDeTabelaBuilder formatoDeTabelaBuilder = new FormatoDeTabelaBuilder();

      formatoDeTabelaBuilder.nome(getCodigo());

      for (MascaraDeCampo campo : campos) {
         formatoDeTabelaBuilder.campo(campo);
      }
      formatoDeTabelaBuilder.termina();

      return formatoDeTabelaBuilder.toString();
   }

   public String formataParaInsercao() {
      FormatoDeInsercaoBuilder formatoDeInsercaoBuilder = new FormatoDeInsercaoBuilder();

      formatoDeInsercaoBuilder.nome(getCodigo());
      for (MascaraDeCampo campo : campos) {
         formatoDeInsercaoBuilder.campo(campo);
      }
      formatoDeInsercaoBuilder.termina();
      return formatoDeInsercaoBuilder.toString();
   }

   String formataParaRemocao() {
      FormatoDeRemocaoBuilder formatoDeRemocaoBuilder = new FormatoDeRemocaoBuilder();

      formatoDeRemocaoBuilder.nome(getCodigo());
      for (MascaraDeCampo campo : campos) {
         formatoDeRemocaoBuilder.campo(campo);
      }
      formatoDeRemocaoBuilder.termina();
      return formatoDeRemocaoBuilder.toString();
   }

   String formataParaAlteracao() {
      FormatoDeAlteracaoBuilder formatoDeAlteracaoBuilder = new FormatoDeAlteracaoBuilder();

      formatoDeAlteracaoBuilder.nome(getCodigo());
      for (MascaraDeCampo campo : campos) {
          formatoDeAlteracaoBuilder.campo(campo);
      }
      formatoDeAlteracaoBuilder.termina();
      return formatoDeAlteracaoBuilder.toString();
   }

   /**
    * Retorna um vetor de instâncias de máscaras de campos.
    *
    * @return vetor de máscaras de campos.
    */
   public List<MascaraDeCampo> getCampos() {
      return campos;
   }

   /**
    * Retorna a descrição da máscara de registro.
    *
    * @return descrição da máscara de registros.
    */
   public String getDescricao() {
      return descricao;
   }

   
   /**
    * Retorna o codigo da máscara de registro.
    *
    * @return  codigo da máscara de registro.
    */
   public String getCodigo() {
      return codigo;
   }

   /**
    * Retorna a posição inicial do codigo do registro em um arquivo.
    *
    * @return posição inicial do codigo de registro.
    */
   public int getPosicaoInicialDoTipoDoRegistro() {
      return getIdentificador().getPosicaoInicial();
   }

   /**
    * Retorna a posição final do codigo do registro em um arquivo.
    *
    * @return posição final do codigo de registro.
    */
   public int getPosicaoFinalDoTipoDoRegistro() {
      return getIdentificador().getPosicaoFinal();
   }

   /**
    * Retorna o tamanho da máscara de registro.
    *
    * @return  tamanho da máscara de registro.
    */
   public int getTamanho() {
      return tamanho;
   }

   /**
    * Retorna o campo que identifica o registro em um arquivo.
    *
    * @return o campo que identifica o registro em um arquivo.
    */
   public MascaraDeCampo getIdentificador() {

      return this.identificador;
   }

   public int getVersao() {
      return versao;
   }

   public void mudaEstadoDasPendenciasDeAlteracaoPara(boolean novoEstado) {    
      for (MascaraDeCampo mc : campos) {
         mc.mudarEstadoDasPendenciasPara(novoEstado);
      }
   }

   String formataParaConsulta() {
      FormatoDeConsultaBuilder formatoDeConsultaBuilder = new FormatoDeConsultaBuilder();

      formatoDeConsultaBuilder.nome(getCodigo());
      for (MascaraDeCampo campo : campos) {
          formatoDeConsultaBuilder.campo(campo);
      }
      formatoDeConsultaBuilder.termina();
      return formatoDeConsultaBuilder.toString();
   }


}
