/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "mascara_de_arquivo")
@NamedQueries({
    @NamedQuery(name = "MascaraDeArquivo.findAll", query = "SELECT m FROM MascaraDeArquivo m"),
    @NamedQuery(name = "MascaraDeArquivo.findByCodMascaraDeArquivo", query = "SELECT m FROM MascaraDeArquivo m WHERE m.mascaraDeArquivoPK.codMascaraDeArquivo = :codMascaraDeArquivo"),
    @NamedQuery(name = "MascaraDeArquivo.findByNumVersaoMascaraDeArquivo", query = "SELECT m FROM MascaraDeArquivo m WHERE m.mascaraDeArquivoPK.numVersaoMascaraDeArquivo = :numVersaoMascaraDeArquivo"),
    @NamedQuery(name = "MascaraDeArquivo.findByDscMascaraDeArquivo", query = "SELECT m FROM MascaraDeArquivo m WHERE m.dscMascaraDeArquivo = :dscMascaraDeArquivo")})
public class MascaraDeArquivo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MascaraDeArquivoPK mascaraDeArquivoPK;
    @Basic(optional = false)
    @Column(name = "dsc_mascara_de_arquivo")
    private String dscMascaraDeArquivo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mascaraDeArquivo")
    private Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollection;

    public MascaraDeArquivo() {
    }

    public MascaraDeArquivo(MascaraDeArquivoPK mascaraDeArquivoPK) {
        this.mascaraDeArquivoPK = mascaraDeArquivoPK;
    }

    public MascaraDeArquivo(MascaraDeArquivoPK mascaraDeArquivoPK, String dscMascaraDeArquivo) {
        this.mascaraDeArquivoPK = mascaraDeArquivoPK;
        this.dscMascaraDeArquivo = dscMascaraDeArquivo;
    }

    public MascaraDeArquivo(String codMascaraDeArquivo, short numVersaoMascaraDeArquivo) {
        this.mascaraDeArquivoPK = new MascaraDeArquivoPK(codMascaraDeArquivo, numVersaoMascaraDeArquivo);
    }

    public MascaraDeArquivoPK getMascaraDeArquivoPK() {
        return mascaraDeArquivoPK;
    }

    public void setMascaraDeArquivoPK(MascaraDeArquivoPK mascaraDeArquivoPK) {
        this.mascaraDeArquivoPK = mascaraDeArquivoPK;
    }

    public String getDscMascaraDeArquivo() {
        return dscMascaraDeArquivo;
    }

    public void setDscMascaraDeArquivo(String dscMascaraDeArquivo) {
        this.dscMascaraDeArquivo = dscMascaraDeArquivo;
    }

    public Collection<MascaraDeArquivoMascaraDeRegistro> getMascaraDeArquivoMascaraDeRegistroCollection() {
        return mascaraDeArquivoMascaraDeRegistroCollection;
    }

    public void setMascaraDeArquivoMascaraDeRegistroCollection(Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollection) {
        this.mascaraDeArquivoMascaraDeRegistroCollection = mascaraDeArquivoMascaraDeRegistroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mascaraDeArquivoPK != null ? mascaraDeArquivoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MascaraDeArquivo)) {
            return false;
        }
        MascaraDeArquivo other = (MascaraDeArquivo) object;
        if ((this.mascaraDeArquivoPK == null && other.mascaraDeArquivoPK != null) || (this.mascaraDeArquivoPK != null && !this.mascaraDeArquivoPK.equals(other.mascaraDeArquivoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivo[mascaraDeArquivoPK=" + mascaraDeArquivoPK + "]";
    }

}
