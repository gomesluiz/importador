/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.IllegalOrphanException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeOrigem;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampo;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author paulo
 */
public class TipoDeOrigemJpaController {

    public TipoDeOrigemJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoDeOrigem tipoDeOrigem) throws PreexistingEntityException, Exception {
        if (tipoDeOrigem.getMascaraDeCampoCollection() == null) {
            tipoDeOrigem.setMascaraDeCampoCollection(new ArrayList<MascaraDeCampo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollection = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampoToAttach : tipoDeOrigem.getMascaraDeCampoCollection()) {
                mascaraDeCampoCollectionMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollection.add(mascaraDeCampoCollectionMascaraDeCampoToAttach);
            }
            tipoDeOrigem.setMascaraDeCampoCollection(attachedMascaraDeCampoCollection);
            em.persist(tipoDeOrigem);
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampo : tipoDeOrigem.getMascaraDeCampoCollection()) {
                TipoDeOrigem oldTipoDeOrigemOfMascaraDeCampoCollectionMascaraDeCampo = mascaraDeCampoCollectionMascaraDeCampo.getTipoDeOrigem();
                mascaraDeCampoCollectionMascaraDeCampo.setTipoDeOrigem(tipoDeOrigem);
                mascaraDeCampoCollectionMascaraDeCampo = em.merge(mascaraDeCampoCollectionMascaraDeCampo);
                if (oldTipoDeOrigemOfMascaraDeCampoCollectionMascaraDeCampo != null) {
                    oldTipoDeOrigemOfMascaraDeCampoCollectionMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionMascaraDeCampo);
                    oldTipoDeOrigemOfMascaraDeCampoCollectionMascaraDeCampo = em.merge(oldTipoDeOrigemOfMascaraDeCampoCollectionMascaraDeCampo);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipoDeOrigem(tipoDeOrigem.getCodTipoDeOrigem()) != null) {
                throw new PreexistingEntityException("TipoDeOrigem " + tipoDeOrigem + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoDeOrigem tipoDeOrigem) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDeOrigem persistentTipoDeOrigem = em.find(TipoDeOrigem.class, tipoDeOrigem.getCodTipoDeOrigem());
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOld = persistentTipoDeOrigem.getMascaraDeCampoCollection();
            Collection<MascaraDeCampo> mascaraDeCampoCollectionNew = tipoDeOrigem.getMascaraDeCampoCollection();
            List<String> illegalOrphanMessages = null;
            for (MascaraDeCampo mascaraDeCampoCollectionOldMascaraDeCampo : mascaraDeCampoCollectionOld) {
                if (!mascaraDeCampoCollectionNew.contains(mascaraDeCampoCollectionOldMascaraDeCampo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MascaraDeCampo " + mascaraDeCampoCollectionOldMascaraDeCampo + " since its tipoDeOrigem field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollectionNew = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampoToAttach : mascaraDeCampoCollectionNew) {
                mascaraDeCampoCollectionNewMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollectionNew.add(mascaraDeCampoCollectionNewMascaraDeCampoToAttach);
            }
            mascaraDeCampoCollectionNew = attachedMascaraDeCampoCollectionNew;
            tipoDeOrigem.setMascaraDeCampoCollection(mascaraDeCampoCollectionNew);
            tipoDeOrigem = em.merge(tipoDeOrigem);
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampo : mascaraDeCampoCollectionNew) {
                if (!mascaraDeCampoCollectionOld.contains(mascaraDeCampoCollectionNewMascaraDeCampo)) {
                    TipoDeOrigem oldTipoDeOrigemOfMascaraDeCampoCollectionNewMascaraDeCampo = mascaraDeCampoCollectionNewMascaraDeCampo.getTipoDeOrigem();
                    mascaraDeCampoCollectionNewMascaraDeCampo.setTipoDeOrigem(tipoDeOrigem);
                    mascaraDeCampoCollectionNewMascaraDeCampo = em.merge(mascaraDeCampoCollectionNewMascaraDeCampo);
                    if (oldTipoDeOrigemOfMascaraDeCampoCollectionNewMascaraDeCampo != null && !oldTipoDeOrigemOfMascaraDeCampoCollectionNewMascaraDeCampo.equals(tipoDeOrigem)) {
                        oldTipoDeOrigemOfMascaraDeCampoCollectionNewMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionNewMascaraDeCampo);
                        oldTipoDeOrigemOfMascaraDeCampoCollectionNewMascaraDeCampo = em.merge(oldTipoDeOrigemOfMascaraDeCampoCollectionNewMascaraDeCampo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = tipoDeOrigem.getCodTipoDeOrigem();
                if (findTipoDeOrigem(id) == null) {
                    throw new NonexistentEntityException("The tipoDeOrigem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDeOrigem tipoDeOrigem;
            try {
                tipoDeOrigem = em.getReference(TipoDeOrigem.class, id);
                tipoDeOrigem.getCodTipoDeOrigem();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoDeOrigem with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOrphanCheck = tipoDeOrigem.getMascaraDeCampoCollection();
            for (MascaraDeCampo mascaraDeCampoCollectionOrphanCheckMascaraDeCampo : mascaraDeCampoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoDeOrigem (" + tipoDeOrigem + ") cannot be destroyed since the MascaraDeCampo " + mascaraDeCampoCollectionOrphanCheckMascaraDeCampo + " in its mascaraDeCampoCollection field has a non-nullable tipoDeOrigem field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoDeOrigem);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoDeOrigem> findTipoDeOrigemEntities() {
        return findTipoDeOrigemEntities(true, -1, -1);
    }

    public List<TipoDeOrigem> findTipoDeOrigemEntities(int maxResults, int firstResult) {
        return findTipoDeOrigemEntities(false, maxResults, firstResult);
    }

    private List<TipoDeOrigem> findTipoDeOrigemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoDeOrigem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoDeOrigem findTipoDeOrigem(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoDeOrigem.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoDeOrigemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoDeOrigem> rt = cq.from(TipoDeOrigem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
