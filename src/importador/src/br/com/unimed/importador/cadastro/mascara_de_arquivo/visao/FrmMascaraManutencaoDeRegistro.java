/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * FrmMascaraManutencaoDeArquivo.java
 *
 * Created on 28/12/2010, 19:35:06
 */
package br.com.unimed.importador.cadastro.mascara_de_arquivo.visao;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.exceptions.IllegalOrphanException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.MascaraDeCampoJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.MascaraDeRegistroJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.PendenciaDeAlteracaoJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.QueryMascaraJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.TipoDeCampoJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.TipoDeOrigemJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.TipoDePendenciaJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.TipoDeUsoJpaController;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampo;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampoPK;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistro;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistroPK;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.PendenciaDeAlteracao;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeCampo;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeOrigem;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDePendencia;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeUso;
import br.com.unimed.importador.visao.ImportacaoHelp;
import java.awt.Dialog.ModalityType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.TableColumn;

/**
 *
 * @author paulo
 */
public class FrmMascaraManutencaoDeRegistro extends javax.swing.JFrame
        implements MyTableListenerEvent {

    private List<MascaraDeRegistro> mascaraManutencaoDeRegistro = null;
    private MyTableModel modeloTabela;
    private QueryMascaraJpaController queryController;
    private Vector<MascaraDeCampoPK> deletar = null;
    private Vector<MascaraDeCampo> incluir = null;
    private Vector<MascaraDeCampo> alterar = null;
    private Vector<Integer> inserirLinhas = null;
    private MyTableModelListener listenerTableModel = null;
    private String codMascaraRegistro;
    private int numSequencia;
    private boolean startPopulated = false;
    private String codMascaraCampo;
    private int posListMascaraDeRegistro = 0;
    private TableColumn tblOrigem = null;
    private TableColumn tblTipo = null;
    private JComboBox comboBoxColumnOrigem = null;
    private JComboBox comboBoxColumnTipoCampo = null;
    private DefaultCellEditor defaultCellEditorOrigem = null;
    private DefaultCellEditor defaultCellEditorTipo = null;
    private TipoDeOrigemJpaController tpOrigemController = null;
    private TipoDeCampoJpaController tpCampoController = null;
    private Vector<String> elementsComboVersao = new Vector<String>();
    private boolean updateComboVersion = false;
    private static final String TYPE_REGISTRO = "MascaraDeRegistro";

    /** Creates new form FrmMascaraManutencaoDeArquivo */
    public FrmMascaraManutencaoDeRegistro() {

        //inicia controlador pesquisa
        queryController = new QueryMascaraJpaController();
        tpOrigemController = new TipoDeOrigemJpaController();
        tpCampoController = new TipoDeCampoJpaController();

        initComponents();
        atualizaComboVersao();

        //start map
        deletar = new Vector<MascaraDeCampoPK>();
        incluir = new Vector<MascaraDeCampo>();
        alterar = new Vector<MascaraDeCampo>();
        inserirLinhas = new Vector<Integer>();

        popularMascaraDeRegistro();

        //configura colunas da JTable
        tblOrigem = tblManutencao.getColumnModel().getColumn(2);
        tblOrigem.setCellEditor(defaultCellEditorOrigem);
        tblTipo = tblManutencao.getColumnModel().getColumn(3);
        tblTipo.setCellEditor(defaultCellEditorTipo);


        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblCodMascara = new javax.swing.JLabel();
        txtMascaraDeRegistro = new javax.swing.JTextField();
        lblDescricao = new javax.swing.JLabel();
        txtDescricaoRegistro = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblManutencao = new javax.swing.JTable();
        btnAdicionarLinha = new javax.swing.JButton();
        btnRemover = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnProximo = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        btnRemoverReg = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jComboBoxNumVersao = new javax.swing.JComboBox();
        jButtonCopiar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Manutenção Mascara de Arquivo");
        setResizable(false);

        lblCodMascara.setText("Código do Registro:");

        txtMascaraDeRegistro.setEditable(false);

        lblDescricao.setText("Descrição:");

        txtDescricaoRegistro.setEditable(false);
        txtDescricaoRegistro.setText("Notas Fiscais");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tblManutencao.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblManutencao.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tblManutencao.setColumnSelectionAllowed(true);
        tblManutencao.setRowHeight(20);
        tblManutencao.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblManutencao.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblManutencao);
        tblManutencao.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        modeloTabela = new MyTableModel(); //inicia modelo da tabela
        listenerTableModel = new MyTableModelListener(tblManutencao);
        listenerTableModel.addUpdateTableListener(this);
        modeloTabela.addTableModelListener(listenerTableModel);

        if (comboBoxColumnOrigem == null) {
            comboBoxColumnOrigem = new JComboBox();
        } else {
            comboBoxColumnOrigem.removeAllItems();
        }

        if (comboBoxColumnTipoCampo == null) {
            comboBoxColumnTipoCampo = new JComboBox();
        } else {
            comboBoxColumnTipoCampo.removeAllItems();
        }

        tpOrigemController = new TipoDeOrigemJpaController();
        List<TipoDeOrigem> findTipoDeOrigemEntities = tpOrigemController.findTipoDeOrigemEntities();
        Iterator<TipoDeOrigem> findTipoDeOrigemEntitesIterator = findTipoDeOrigemEntities.iterator();
        while (findTipoDeOrigemEntitesIterator.hasNext()) {
            comboBoxColumnOrigem.addItem(findTipoDeOrigemEntitesIterator.next().getDscTipoDeOrigem());
        }

        tpCampoController = new TipoDeCampoJpaController();
        List<TipoDeCampo> findTipoDeCampoEntities = tpCampoController.findTipoDeCampoEntities();
        Iterator<TipoDeCampo> findTipoDeCampoEntitesIterator = findTipoDeCampoEntities.iterator();

        while (findTipoDeCampoEntitesIterator.hasNext()) {
            comboBoxColumnTipoCampo.addItem(findTipoDeCampoEntitesIterator.next().getDscTipoDeCampo());
        }

        if (defaultCellEditorOrigem == null) {
            defaultCellEditorOrigem = new DefaultCellEditor(comboBoxColumnOrigem);
        }

        if (defaultCellEditorTipo == null) {
            defaultCellEditorTipo = new DefaultCellEditor(comboBoxColumnTipoCampo);
        }

        btnAdicionarLinha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/add_stat.gif"))); // NOI18N
        btnAdicionarLinha.setText("Adicionar");
        btnAdicionarLinha.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnAdicionarLinha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarLinhaActionPerformed(evt);
            }
        });

        btnRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/delete_obj.gif"))); // NOI18N
        btnRemover.setText("Remover");
        btnRemover.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1029, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAdicionarLinha, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemover))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAdicionarLinha, btnRemover});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(btnAdicionarLinha)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAdicionarLinha, btnRemover});

        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/nav_backward.gif"))); // NOI18N
        btnAnterior.setText("Anterior");
        btnAnterior.setIconTextGap(8);
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnProximo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/nav_forward.gif"))); // NOI18N
        btnProximo.setText("Próximo");
        btnProximo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnProximo.setIconTextGap(10);
        btnProximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProximoActionPerformed(evt);
            }
        });

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/new.gif"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnNovo.setIconTextGap(8);
        btnNovo.setMaximumSize(new java.awt.Dimension(69, 30));
        btnNovo.setMinimumSize(new java.awt.Dimension(69, 30));
        btnNovo.setPreferredSize(new java.awt.Dimension(69, 30));
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnRemoverReg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/delete_obj.gif"))); // NOI18N
        btnRemoverReg.setText("Remover");
        btnRemoverReg.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnRemoverReg.setIconTextGap(8);
        btnRemoverReg.setMaximumSize(new java.awt.Dimension(69, 30));
        btnRemoverReg.setMinimumSize(new java.awt.Dimension(69, 30));
        btnRemoverReg.setPreferredSize(new java.awt.Dimension(69, 30));
        btnRemoverReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverRegActionPerformed(evt);
            }
        });

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/save_edit.gif"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.setEnabled(false);
        btnSalvar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSalvar.setIconTextGap(8);
        btnSalvar.setMaximumSize(new java.awt.Dimension(69, 30));
        btnSalvar.setMinimumSize(new java.awt.Dimension(69, 30));
        btnSalvar.setPreferredSize(new java.awt.Dimension(69, 30));
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/skip_task.gif"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnCancelar.setIconTextGap(8);
        btnCancelar.setMaximumSize(new java.awt.Dimension(69, 30));
        btnCancelar.setMinimumSize(new java.awt.Dimension(69, 30));
        btnCancelar.setPreferredSize(new java.awt.Dimension(69, 30));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/help_contents.gif"))); // NOI18N
        btnAjuda.setText("Ajuda");
        btnAjuda.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnAjuda.setIconTextGap(8);
        btnAjuda.setMaximumSize(new java.awt.Dimension(69, 30));
        btnAjuda.setMinimumSize(new java.awt.Dimension(69, 30));
        btnAjuda.setPreferredSize(new java.awt.Dimension(69, 30));
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });

        jLabel1.setText("Versão Registro:");

        MascaraDeCampoJpaController mascaraController =
        new MascaraDeCampoJpaController();

        List<MascaraDeCampo> list = mascaraController.findMascaraDeCampoEntities();
        Iterator<MascaraDeCampo> iteratorMascara = list.iterator();
        elementsComboVersao.removeAllElements();
        List noDup = new ArrayList();

        /* preenche lista com todas as versões */
        while(iteratorMascara.hasNext()) {
            MascaraDeCampo temp = iteratorMascara.next();
            noDup.add(temp.getMascaraDeCampoPK().getNumVersaoMascaraDeRegistro());
        }

        /* retira as duplicatas */
        HashSet set = new HashSet(noDup);
        Iterator iterator = set.iterator();

        List ordenar = new ArrayList();

        while(iterator.hasNext()) {
            ordenar.add((Short)iterator.next());
        }

        /* ordena as versoes */
        Collections.sort(ordenar);
        iterator = ordenar.iterator();

        while(iterator.hasNext()){
            String t = Short.toString((Short)iterator.next());
            jComboBoxNumVersao.addItem(t);
            elementsComboVersao.addElement(t);
        }
        jComboBoxNumVersao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxNumVersaoActionPerformed(evt);
            }
        });

        jButtonCopiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/unimed/importador/visao/resources/copy_edit.gif"))); // NOI18N
        jButtonCopiar.setText("Copiar");
        jButtonCopiar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButtonCopiar.setIconTextGap(10);
        jButtonCopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCopiarActionPerformed(evt);
            }
        });
        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblCodMascara)
                            .addComponent(lblDescricao))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtMascaraDeRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 623, Short.MAX_VALUE)
                                .addComponent(jLabel1))
                            .addComponent(txtDescricaoRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, 865, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButtonCopiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBoxNumVersao, 0, 151, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnProximo, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 109, Short.MAX_VALUE)
                                .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRemoverReg, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAjuda, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(29, 29, 29))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAjuda, btnAnterior, btnCancelar, btnNovo, btnProximo, btnRemoverReg, btnSalvar});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCodMascara)
                    .addComponent(txtMascaraDeRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxNumVersao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescricaoRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDescricao)
                    .addComponent(jButtonCopiar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                    .addComponent(btnProximo, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnNovo, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                        .addComponent(btnRemoverReg, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                        .addComponent(btnSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                        .addComponent(btnAjuda, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtDescricaoRegistro, txtMascaraDeRegistro});

        btnProximo.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarLinhaActionPerformed(java.awt.event.ActionEvent evt) {
        Vector v = new Vector();

        int sequencia;
        int row = modeloTabela.getRowCount();
        if (row == 0) {
            sequencia = 1;
        } else {
            sequencia = (Integer) modeloTabela.getValueAt(row - 1, 0) + 1;
        }

        v.addElement(sequencia);
        v.addElement("");
        v.addElement("");
        v.addElement("");
        v.addElement(0);
        v.addElement(0);
        v.addElement(0);
        v.addElement("");
        v.addElement(false);
        v.addElement(false);
        v.addElement(false);

        modeloTabela.addRow(v);
        tblManutencao.updateUI();

        btnSalvar.setEnabled(true);
        modeloTabela.setInsert(true);

    }

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        // TODO add your handling code here:

        //verificar pendências não salvas
        if (!verificarPendencias()) {
            return;
        }

        posListMascaraDeRegistro--;

        if (posListMascaraDeRegistro == 0) {
            btnAnterior.setEnabled(false);

        } else if (posListMascaraDeRegistro == -1) {
            posListMascaraDeRegistro = 0;
            btnAnterior.setEnabled(false);
        } else {
            btnAnterior.setEnabled(true);
        }

        if (mascaraManutencaoDeRegistro.isEmpty()) {
            btnAnterior.setEnabled(false);
            btnProximo.setEnabled(false);
            modeloTabela.clear();
            modeloTabela.updateTable();
        }

        if (mascaraManutencaoDeRegistro.get(posListMascaraDeRegistro) != null) {
            MascaraDeRegistro tmp = mascaraManutencaoDeRegistro.get(posListMascaraDeRegistro);

            txtMascaraDeRegistro.setText(tmp.getMascaraDeRegistroPK().getCodMascaraDeRegistro());
            txtDescricaoRegistro.setText(tmp.getDscMascaraDeRegistro());
            btnProximo.setEnabled(true);

        } else {
            btnAnterior.setEnabled(false);

        }

        queryController.preencheTabelaMascaraDeRegistro(tblManutencao, modeloTabela,
                txtMascaraDeRegistro.getText(), (Short.parseShort((String) jComboBoxNumVersao.getSelectedItem())));


    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnProximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProximoActionPerformed
        // TODO add your handling code here:

        //verificar pendências não salvas
        if (!verificarPendencias()) {
            return;
        }

        posListMascaraDeRegistro++;

        if (posListMascaraDeRegistro > mascaraManutencaoDeRegistro.size()) {
            posListMascaraDeRegistro = mascaraManutencaoDeRegistro.size();
            btnProximo.setEnabled(false);

        } else if (posListMascaraDeRegistro == mascaraManutencaoDeRegistro.size() - 1) {
            btnProximo.setEnabled(false);
        } else {
            btnProximo.setEnabled(true);
        }


        if (mascaraManutencaoDeRegistro.isEmpty()) {
            btnAnterior.setEnabled(false);
            btnProximo.setEnabled(false);
            modeloTabela.clear();
            modeloTabela.updateTable();
            tblManutencao.updateUI();
        }

        if (mascaraManutencaoDeRegistro.size() > posListMascaraDeRegistro) {
            MascaraDeRegistro tmp = mascaraManutencaoDeRegistro.get(posListMascaraDeRegistro);
            txtMascaraDeRegistro.setText(tmp.getMascaraDeRegistroPK().getCodMascaraDeRegistro());
            txtDescricaoRegistro.setText(tmp.getDscMascaraDeRegistro());
            btnAnterior.setEnabled(true);
        } else {
            btnProximo.setEnabled(false);
        }

        queryController.preencheTabelaMascaraDeRegistro(tblManutencao, modeloTabela,
                txtMascaraDeRegistro.getText(), (Short.parseShort((String) jComboBoxNumVersao.getSelectedItem())));
    }//GEN-LAST:event_btnProximoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        // TODO add your handling code here:
        btnProximo.setEnabled(true);
        btnAnterior.setEnabled(true);
        startPopulated = false;
        modeloTabela.setInsert(false);
        boolean sucess = false;
        boolean ehNovoRegistro = false;

        /* verifica novo registro */

        if (txtMascaraDeRegistro.getText().length() == 0
                || txtDescricaoRegistro.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Informe código"
                    + "mascara e descrição da mascara", "Informe"
                    + "Dados", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            if (txtMascaraDeRegistro.isEditable()) {
                //um novo registro sendo inserido
                ehNovoRegistro = true;
                startPopulated = true;
                MascaraDeRegistroJpaController mascaraRegistroController =
                        new MascaraDeRegistroJpaController();
                try {
                    mascaraRegistroController.create(new MascaraDeRegistro(
                            new MascaraDeRegistroPK(
                            txtMascaraDeRegistro.getText(),
                            Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())),
                            txtDescricaoRegistro.getText(), (short) 0));

                } catch (PreexistingEntityException ex) {
                    ex.printStackTrace();
                    Logger.getLogger(FrmMascaraManutencaoDeArquivo.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Logger.getLogger(FrmMascaraManutencaoDeArquivo.class.getName()).log(Level.SEVERE, null, ex);
                }


                //registro criado - procede para inserção //
                txtDescricaoRegistro.setEditable(false);
                txtMascaraDeRegistro.setEditable(false);

                sucess = true;
            }
        }


        /* criar controladores jpa */
        MascaraDeCampoJpaController mascaraController =
                new MascaraDeCampoJpaController();

        PendenciaDeAlteracaoJpaController pendenciaController =
                new PendenciaDeAlteracaoJpaController();

        TipoDePendenciaJpaController tipoPendenciaController =
                new TipoDePendenciaJpaController();

        if (!inserirLinhas.isEmpty()) {

            Iterator itLinhasIncluir = inserirLinhas.iterator();
            TipoDeOrigemJpaController tipoDeOrigem = new TipoDeOrigemJpaController();
            TipoDeCampoJpaController tipoDeCampo = new TipoDeCampoJpaController();
            TipoDeUsoJpaController tipoDeUso = new TipoDeUsoJpaController();

            List<TipoDeOrigem> listTipoOrigem = tipoDeOrigem.findTipoDeOrigemEntities();
            List<TipoDeCampo> listTipoCampo = tipoDeCampo.findTipoDeCampoEntities();
            List<TipoDeUso> listTipoUso = tipoDeUso.findTipoDeUsoEntities();

            while (itLinhasIncluir.hasNext()) {
                try {
                    int index = (Integer) itLinhasIncluir.next();
                    Iterator<TipoDeOrigem> iteratorTipoOrigem = listTipoOrigem.iterator();
                    Iterator<TipoDeCampo> iteratorTipoCampo = listTipoCampo.iterator();
                    Iterator<TipoDeUso> iteratorTipoUso = listTipoUso.iterator();

                    TipoDeCampo _tipoDeCampo = null;
                    TipoDeOrigem _tipoDeOrigem = null;
                    TipoDeUso _tipoDeUso = null;

                    while (iteratorTipoOrigem.hasNext()) {
                        TipoDeOrigem tpOrigem = iteratorTipoOrigem.next();
                        if (tpOrigem.getDscTipoDeOrigem().equals((String) modeloTabela.getValueAt(index, 2))) {
                            _tipoDeOrigem = tpOrigem;
                            break;
                        }
                    }

                    while (iteratorTipoCampo.hasNext()) {
                        TipoDeCampo tpCampo = iteratorTipoCampo.next();
                        if (tpCampo.getDscTipoDeCampo().equals((String) modeloTabela.getValueAt(index, 3))) {
                            _tipoDeCampo = tpCampo;
                            break;
                        }
                    }

                    while (iteratorTipoUso.hasNext()) {
                        TipoDeUso tpUso = iteratorTipoUso.next();
                        if (tpUso.getCodTipoDeUso() == ((Boolean) modeloTabela.getValueAt(index, 9))) {
                            _tipoDeUso = tpUso;
                            break;
                        }
                    }

                    Integer posInicial = (Integer) modeloTabela.getValueAt(index, 4);
                    Integer posFinal = (Integer) modeloTabela.getValueAt(index, 5);
                    Integer tamanho = (Integer) modeloTabela.getValueAt(index, 6);
                    Integer vlrDecimais = (Integer) modeloTabela.getValueAt(index, 7);
                    Integer sequencia = (Integer) modeloTabela.getValueAt(index, 0);

                    boolean indContemTipoReg;

                    indContemTipoReg = (Boolean) modeloTabela.getValueAt(index, 10);


                    boolean indChavePrimaria = (Boolean) modeloTabela.getValueAt(index, 8);

                    if (((String) modeloTabela.getValueAt(index, 1)).length() < 2) {
                        JOptionPane.showMessageDialog(null, "Informe os dados "
                                + "corretamente !", "Entrada de dados - Erro",
                                JOptionPane.ERROR_MESSAGE);
                        return;
                    }

                    MascaraDeCampo temp = new MascaraDeCampo(new MascaraDeCampoPK(
                            (String) modeloTabela.getValueAt(index, 1),
                            txtMascaraDeRegistro.getText(),
                            Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())));

                    temp.setTipoDeCampo(_tipoDeCampo);
                    temp.setTipoDeOrigem(_tipoDeOrigem);
                    temp.setTipoDeUso(_tipoDeUso);
                    temp.setNumSequencialCampo(sequencia.shortValue());
                    temp.setVlrPosicaoInicial(posInicial.shortValue());
                    temp.setVlrPosicaoFinal(posFinal.shortValue());
                    temp.setIndChavePrimaria(indChavePrimaria);
                    temp.setIndContemTipoDeRegistro(indContemTipoReg);
                    temp.setVlrDecimais(vlrDecimais.shortValue());
                    temp.setVlrTamanho(tamanho.shortValue());

                    temp.setMascaraDeRegistro(new MascaraDeRegistro(
                            txtMascaraDeRegistro.getText(),
                            Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())));

                    incluir.addElement(temp);

                } catch (ClassCastException c) {
                    JOptionPane.showMessageDialog(null, "Preencha Corretamente os dados",
                            "Erro de entrada de dados", JOptionPane.ERROR_MESSAGE);

                    sucess = false;

                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(null, "Preencha Corretamente os dados",
                            "Erro de entrada de dados", JOptionPane.ERROR_MESSAGE);

                    sucess = false;
                }
            }

            if (incluir.isEmpty()) {
                return;
            }

            Iterator itIncluir = incluir.iterator();

            while (itIncluir.hasNext()) {
                MascaraDeCampo e = (MascaraDeCampo) itIncluir.next();

                try {
                    mascaraController.create(e);

                    /* inserir pendência */
                    PendenciaDeAlteracao pendencia = new PendenciaDeAlteracao(null,
                            e.getMascaraDeCampoPK().getCodMascaraDeCampo(),
                            e.getMascaraDeCampoPK().getCodMascaraDeRegistro(),
                            e.getMascaraDeCampoPK().getNumVersaoMascaraDeRegistro(),
                            new Date(), false);

                    TipoDePendencia tipo = tipoPendenciaController.findTipoDePendencia((short) 1);
                    pendencia.setTipoDePendencia(tipo);

                    pendenciaController.create(pendencia);

                    sucess = true;

                } catch (NonexistentEntityException ex) {
                    //ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Problema: "
                            + " Erro ao incluir\nMascara de Registro ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeRegistro() + "]\nMascara de Campo ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeCampo() + "]");
                    sucess = false;
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Problema: "
                            + " Erro ao incluir\nMascara de Registro ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeRegistro() + "]\nMascara de Campo ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeCampo() + "]");
                    sucess = false;
                }
            }
            //pendências processadas
            incluir.clear();
            inserirLinhas.clear();
        }

        if (!alterar.isEmpty()) {
            Iterator itAlterar = alterar.iterator();

            while (itAlterar.hasNext()) {
                MascaraDeCampo e =
                        (MascaraDeCampo) itAlterar.next();
                try {

                    /* inserir pendência */
                    PendenciaDeAlteracao pendencia = new PendenciaDeAlteracao(null,
                            e.getMascaraDeCampoPK().getCodMascaraDeCampo(),
                            e.getMascaraDeCampoPK().getCodMascaraDeRegistro(),
                            e.getMascaraDeCampoPK().getNumVersaoMascaraDeRegistro(),
                            new Date(), false);

                    TipoDePendencia tipo = tipoPendenciaController.findTipoDePendencia((short) 2);
                    pendencia.setTipoDePendencia(tipo);

                    pendenciaController.create(pendencia);
                    mascaraController.edit(e);
                    sucess = true;

                } catch (NonexistentEntityException ex) {
                    //ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Problema: "
                            + " Erro ao alterar\nMascara de campo ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeCampo() + "]\nMascara de Registro ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeRegistro() + "]");
                    sucess = false;
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Problema: "
                            + " Erro ao alterar\nMascara de campo ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeCampo() + "]\nMascara de Registro ["
                            + e.getMascaraDeCampoPK().getCodMascaraDeRegistro() + "]");
                    sucess = false;
                }
            }
            //pendências processadas
            alterar.clear();
        }

        //processando deletar
        if (!deletar.isEmpty()) {
            Iterator itDeletar = deletar.iterator();

            while (itDeletar.hasNext()) {
                MascaraDeCampoPK e =
                        (MascaraDeCampoPK) itDeletar.next();


                try {
                    mascaraController.destroy(e);
                } catch (br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException ex) {
                    Logger.getLogger(FrmMascaraManutencaoDeRegistro.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Problema: "
                            + " Erro ao deletar\n["
                            + e.getCodMascaraDeCampo() + "]\nMascara de Registro ["
                            + e.getCodMascaraDeRegistro() + "]");
                    sucess = false;
                }


                /* incluir pendencia de deleção */
                PendenciaDeAlteracao pendencia = new PendenciaDeAlteracao(null,
                        e.getCodMascaraDeCampo(),
                        e.getCodMascaraDeRegistro(),
                        e.getNumVersaoMascaraDeRegistro(),
                        new Date(), false);

                TipoDePendencia tipo = tipoPendenciaController.findTipoDePendencia((short) 1);
                pendencia.setTipoDePendencia(tipo);

                pendenciaController.create(pendencia);





                /* inserir pendência */
                sucess = true;


            }
            //pendências processadas
            deletar.clear();
        }

        if (sucess) {
            JOptionPane.showMessageDialog(null, "Operação com sucesso !", "Sucesso",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Operação Falhou !", "Falha",
                    JOptionPane.ERROR_MESSAGE);
        }


        if (startPopulated) {
            popularMascaraDeRegistro();
        }

        btnSalvar.setEnabled(false);
        btnNovo.setEnabled(true);
        btnRemoverReg.setEnabled(true);
        btnProximo.setEnabled(true);
        btnAnterior.setEnabled(true);
    }//GEN-LAST:event_btnSalvarActionPerformed

    private boolean verificarPendencias() {
        //verificar pendências não salvas
        if (!deletar.isEmpty() || !inserirLinhas.isEmpty() || !alterar.isEmpty()) {
            int op = JOptionPane.showConfirmDialog(null, "Existem "
                    + "pendências não salvas. \nDeseja continuar sem salvar ?"
                    + "\n\nAs alterações feitas serão perdidas",
                    "Pendências", JOptionPane.YES_NO_OPTION);

            if (op == JOptionPane.NO_OPTION) {
                return false;
            } else {
                deletar.clear();
                inserirLinhas.clear();
                alterar.clear();
                incluir.clear();
            }
        }
        return true;
    }

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        // TODO add your handling code here:
        int row = tblManutencao.getSelectedRow();
        int col = 1;  /* segunda coluna apresenta codMascaraRegistro */

        if (row == -1) {
            JOptionPane.showMessageDialog(null, "Selecione um campo",
                    "Selecione os dados", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        codMascaraRegistro = (String) txtMascaraDeRegistro.getText().trim();
        codMascaraCampo = ((String) modeloTabela.getValueAt(row, col)).trim();
        numSequencia = (Integer) modeloTabela.getValueAt(row, col - 1);

        modeloTabela.deleteRow(row);
        modeloTabela.updateTable();

        if (inserirLinhas.contains(row)) {
            inserirLinhas.removeElement(row);
        }

    }//GEN-LAST:event_btnRemoverActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        // TODO add your handling code here:
        txtDescricaoRegistro.setEditable(true);
        txtMascaraDeRegistro.setEditable(true);
        txtDescricaoRegistro.setText("");
        txtMascaraDeRegistro.setText("");

        modeloTabela.clear();
        modeloTabela.updateTable();

        inserirLinhas.clear();
        deletar.clear();
        alterar.clear();
        incluir.clear();

        btnSalvar.setEnabled(true);
        btnNovo.setEnabled(false);
        btnRemoverReg.setEnabled(false);
        btnProximo.setEnabled(false);
        btnAnterior.setEnabled(false);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnRemoverRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverRegActionPerformed
        // TODO add your handling code here:

        if (txtMascaraDeRegistro.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Informe mascara"
                    + "de arquivo antes de deletar", "Informação",
                    JOptionPane.INFORMATION_MESSAGE);

            return;
        }

        int op = JOptionPane.showConfirmDialog(null, "Confirma exclusão de "
                + txtMascaraDeRegistro.getText() + " ?", "Confirme a operação",
                JOptionPane.YES_NO_OPTION);

        if (op == JOptionPane.NO_OPTION) {
            return;
        }

        MascaraDeCampoJpaController mascaraCampoController =
                new MascaraDeCampoJpaController();

        Iterator<MascaraDeCampo> iteratorMascaraDeCampo =
                mascaraCampoController.findMascaraDeCampoEntities().iterator();

        MascaraDeCampo temporary = new MascaraDeCampo();

        while (iteratorMascaraDeCampo.hasNext()) {
            temporary = iteratorMascaraDeCampo.next();

            if (temporary.getMascaraDeCampoPK().getCodMascaraDeRegistro().equals(txtMascaraDeRegistro.getText())) {

                try {
                    mascaraCampoController.destroy(temporary.getMascaraDeCampoPK());
                } catch (br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException ex) {
                    Logger.getLogger(FrmMascaraManutencaoDeRegistro.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        /* deleta a mascara de registro */
        MascaraDeRegistroJpaController mascaraDeRegistroController =
                new MascaraDeRegistroJpaController();


        try {
            mascaraDeRegistroController.destroy(new MascaraDeRegistroPK(txtMascaraDeRegistro.getText(), Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())));
        } catch (br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.IllegalOrphanException ex) {
            Logger.getLogger(FrmMascaraManutencaoDeRegistro.class.getName()).log(Level.SEVERE, null, ex);
        } catch (br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException ex) {
            Logger.getLogger(FrmMascaraManutencaoDeRegistro.class.getName()).log(Level.SEVERE, null, ex);
        }

        inserirLinhas.clear();
        deletar.clear();
        alterar.clear();
        incluir.clear();

        popularMascaraDeRegistro();
    }//GEN-LAST:event_btnRemoverRegActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        txtDescricaoRegistro.setEditable(false);
        txtMascaraDeRegistro.setEditable(false);
        modeloTabela.setInsert(false);

        modeloTabela.clear();
        modeloTabela.updateTable();

        inserirLinhas.clear();
        deletar.clear();
        alterar.clear();
        incluir.clear();

        popularMascaraDeRegistro();
        btnSalvar.setEnabled(false);
        btnNovo.setEnabled(true);
        btnRemoverReg.setEnabled(true);
        btnProximo.setEnabled(true);
        btnAnterior.setEnabled(true);

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void jComboBoxNumVersaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxNumVersaoActionPerformed
        // TODO add your handling code here:
        if (!updateComboVersion) {
            popularMascaraDeRegistro();
            modeloTabela.updateTable();
        }
    }//GEN-LAST:event_jComboBoxNumVersaoActionPerformed

    private void jButtonCopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCopiarActionPerformed
        // TODO add your handling code here:
        FrmVersao frmVersao = new FrmVersao(
                Short.toString(Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())),
                elementsComboVersao, TYPE_REGISTRO);

        frmVersao.setModalityType(ModalityType.APPLICATION_MODAL);
        frmVersao.setVisible(true);
        atualizaComboVersao();
    }//GEN-LAST:event_jButtonCopiarActionPerformed

    private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
        // TODO add your handling code here:
        ImportacaoHelp.getInstance().openHelp();
    }//GEN-LAST:event_btnAjudaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new FrmMascaraManutencaoDeRegistro().setVisible(true);
            }
        });
    }

    private void popularMascaraDeRegistro() {
        /* cria list com todas as mascaras de arquivo */
        MascaraDeRegistroJpaController mascaraDeRegistroController =
                new MascaraDeRegistroJpaController();

        mascaraManutencaoDeRegistro = mascaraDeRegistroController.findMascaraDeRegistroEntities();
        // listMascaraDeRegistro = mascaraManutencaoDeRegistro;

        Iterator<MascaraDeRegistro> iterator = mascaraManutencaoDeRegistro.listIterator();
        MascaraDeRegistro tmp;

        /* Esse laço deixa apenas as mascaras de registro da
         * versão selecionada
         */
        while (iterator.hasNext()) {
            tmp = iterator.next();
            if (tmp.getMascaraDeRegistroPK().getNumVersaoMascaraDeRegistro()
                    != Short.parseShort((String) jComboBoxNumVersao.getSelectedItem())) {
                iterator.remove();
            }
        }

        posListMascaraDeRegistro = 0;

        if (!mascaraManutencaoDeRegistro.isEmpty()) {
            tmp = mascaraManutencaoDeRegistro.get(posListMascaraDeRegistro);
            txtMascaraDeRegistro.setText(tmp.getMascaraDeRegistroPK().getCodMascaraDeRegistro());
            txtDescricaoRegistro.setText(tmp.getDscMascaraDeRegistro());
            btnAnterior.setEnabled(false);
            btnProximo.setEnabled(true);
        } else {
            btnAnterior.setEnabled(false);
            btnProximo.setEnabled(false);
        }

        queryController.preencheTabelaMascaraDeRegistro(tblManutencao, modeloTabela,
                txtMascaraDeRegistro.getText(), (Short.parseShort((String) jComboBoxNumVersao.getSelectedItem())));

    }

    @Override
    public void notifyUpdateTable(int row, int col) {
        int columnIdRegistro = 10;
        int columnTamanho = 4;
        int i = 0;
        int rowOld = -1;
        if (col == columnIdRegistro) {
            //verifica se existe outro combo selecionado e desmarca
            Vector data = modeloTabela.getData();
            Iterator it = data.iterator();
            Vector tmp = null;
            while (it.hasNext()) {
                tmp = ((Vector) it.next());



                if (row != i) {
                    if (tmp.get(columnIdRegistro) == Boolean.TRUE) {
                        rowOld = i;
                    }
                    tmp.setElementAt(Boolean.FALSE, columnIdRegistro);
                } else {
                    tmp.setElementAt(Boolean.TRUE, columnIdRegistro);
                }
                i++;
            }
            modeloTabela.updateTable();
        }

        if (col == columnTamanho || col == columnTamanho + 1) {
            //verifica se existe outro combo selecionado e desmarca
            Integer tamIni = 0, tamFim = 0, tam = 0;
            Vector data = modeloTabela.getData();
            Iterator it = data.iterator();
            Vector t = null;
            while (it.hasNext()) {
                if (row == i) {
                    try {
                        t = (Vector) it.next();
                        Object t1 = t.get(columnTamanho);
                        Object t2 = t.get(columnTamanho + 1);

                        if (t1 instanceof Integer) {
                            tamIni = Integer.parseInt(String.valueOf(t1));
                        }
                        if (t2 instanceof Integer) {
                            tamFim = Integer.parseInt(String.valueOf(t2));
                        } else if (t2 instanceof BigDecimal) {
                            tamFim = ((BigDecimal) t2).intValue();
                        }

                        tam = tamFim - tamIni + 1;
                        t.setElementAt(tam, columnTamanho + 2);
                    } catch (NoSuchElementException e) {
                        break;
                    } catch (java.lang.ClassCastException e) {
                        break;
                    }
                } else {
                    it.next();
                }
                i++;
            }

            modeloTabela.updateTable();
        }

        /* se estiver inserindo retorna */
        if (inserirLinhas.contains(row)) {
            return;
        }

        //detecta alteração e prepara vector para persistencia
        TipoDeOrigemJpaController tipoDeOrigem = new TipoDeOrigemJpaController();
        TipoDeCampoJpaController tipoDeCampo = new TipoDeCampoJpaController();

        List<TipoDeOrigem> listTipoOrigem = tipoDeOrigem.findTipoDeOrigemEntities();
        List<TipoDeCampo> listTipoCampo = tipoDeCampo.findTipoDeCampoEntities();

        Integer posInicial = null, posFinal = null, tamanho = null,
                vlrDecimais = null, sequencia = null;

        int index = row;
        Iterator<TipoDeOrigem> iteratorTipoOrigem = listTipoOrigem.iterator();
        Iterator<TipoDeCampo> iteratorTipoCampo = listTipoCampo.iterator();

        TipoDeCampo _tipoDeCampo = null;
        TipoDeOrigem _tipoDeOrigem = null;

        while (iteratorTipoOrigem.hasNext()) {
            TipoDeOrigem tpOrigem = iteratorTipoOrigem.next();
            if (tpOrigem.getDscTipoDeOrigem().equals((String) modeloTabela.getValueAt(index, 2))) {
                _tipoDeOrigem = tpOrigem;
                break;
            }
        }

        while (iteratorTipoCampo.hasNext()) {
            TipoDeCampo tpCampo = iteratorTipoCampo.next();
            if (tpCampo.getDscTipoDeCampo().equals((String) modeloTabela.getValueAt(index, 3))) {
                _tipoDeCampo = tpCampo;
                break;
            }
        }

        if (modeloTabela.getValueAt(index, 4) instanceof Integer) {
            posInicial = (Integer) modeloTabela.getValueAt(index, 4);
        } else if (modeloTabela.getValueAt(index, 4) instanceof BigDecimal) {
            posInicial = ((BigDecimal) modeloTabela.getValueAt(index, 4)).intValue();
        }

        if (modeloTabela.getValueAt(index, 5) instanceof Integer) {
            posFinal = (Integer) modeloTabela.getValueAt(index, 5);
        } else if (modeloTabela.getValueAt(index, 5) instanceof BigDecimal) {
            posFinal = ((BigDecimal) modeloTabela.getValueAt(index, 5)).intValue();
        }

        if (modeloTabela.getValueAt(index, 6) instanceof Integer) {
            tamanho = (Integer) modeloTabela.getValueAt(index, 6);
        } else if (modeloTabela.getValueAt(index, 6) instanceof BigDecimal) {
            tamanho = ((BigDecimal) modeloTabela.getValueAt(index, 6)).intValue();
        }

        if (modeloTabela.getValueAt(index, 7) instanceof Integer) {
            vlrDecimais = (Integer) modeloTabela.getValueAt(index, 7);
        } else if (modeloTabela.getValueAt(index, 7) instanceof BigDecimal) {
            vlrDecimais = ((BigDecimal) modeloTabela.getValueAt(index, 7)).intValue();
        }

        if (modeloTabela.getValueAt(index, 0) instanceof Integer) {
            sequencia = (Integer) modeloTabela.getValueAt(index, 0);
        } else if (modeloTabela.getValueAt(index, 0) instanceof BigDecimal) {
            sequencia = ((BigDecimal) modeloTabela.getValueAt(index, 0)).intValue();
        }


        boolean codTipoUso = (Boolean) modeloTabela.getValueAt(index, 9);
        boolean indContemTipoReg = (Boolean) modeloTabela.getValueAt(index, 10);
        boolean indChavePrimaria = (Boolean) modeloTabela.getValueAt(index, 8);

        if (posInicial == null || posFinal == null || vlrDecimais == null
                || sequencia == null) {
            return;
        }

        TipoDeUso tpUso = new TipoDeUso();

        MascaraDeCampo temp = new MascaraDeCampo(new MascaraDeCampoPK(
                (String) modeloTabela.getValueAt(index, 1),
                txtMascaraDeRegistro.getText(),
                Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())), posInicial.shortValue(),
                posFinal.shortValue(), tamanho.shortValue(),
                vlrDecimais.shortValue(), indContemTipoReg, indChavePrimaria, codTipoUso,
                sequencia.shortValue());


        /* cria o objeto tipo de uso */
        tpUso.setCodTipoDeUso(codTipoUso);

        temp.setTipoDeCampo(_tipoDeCampo);
        temp.setTipoDeOrigem(_tipoDeOrigem);
        temp.setTipoDeUso(tpUso);


        MascaraDeRegistroJpaController mascaraRegistroController =
                new MascaraDeRegistroJpaController();

        MascaraDeRegistro mre = mascaraRegistroController.findMascaraDeRegistro(
                new MascaraDeRegistroPK(txtMascaraDeRegistro.getText(),
                Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())));

        temp.setMascaraDeRegistro(mre);

        alterar.addElement(temp);


        /* altera a linha anterior */


        if (rowOld != -1) {
            index = rowOld;
            iteratorTipoOrigem = listTipoOrigem.iterator();
            iteratorTipoCampo = listTipoCampo.iterator();

            _tipoDeCampo = null;
            _tipoDeOrigem = null;

            while (iteratorTipoOrigem.hasNext()) {
                TipoDeOrigem tpOrigem = iteratorTipoOrigem.next();
                if (tpOrigem.getDscTipoDeOrigem().equals((String) modeloTabela.getValueAt(index, 2))) {
                    _tipoDeOrigem = tpOrigem;
                    break;
                }
            }

            while (iteratorTipoCampo.hasNext()) {
                TipoDeCampo tpCampo = iteratorTipoCampo.next();
                if (tpCampo.getDscTipoDeCampo().equals((String) modeloTabela.getValueAt(index, 3))) {
                    _tipoDeCampo = tpCampo;
                    break;
                }
            }

            if (modeloTabela.getValueAt(index, 4) instanceof Integer) {
                posInicial = (Integer) modeloTabela.getValueAt(index, 4);
            } else if (modeloTabela.getValueAt(index, 4) instanceof BigDecimal) {
                posInicial = ((BigDecimal) modeloTabela.getValueAt(index, 4)).intValue();
            }

            if (modeloTabela.getValueAt(index, 5) instanceof Integer) {
                posFinal = (Integer) modeloTabela.getValueAt(index, 5);
            } else if (modeloTabela.getValueAt(index, 5) instanceof BigDecimal) {
                posFinal = ((BigDecimal) modeloTabela.getValueAt(index, 5)).intValue();
            }

            if (modeloTabela.getValueAt(index, 6) instanceof Integer) {
                tamanho = (Integer) modeloTabela.getValueAt(index, 6);
            } else if (modeloTabela.getValueAt(index, 6) instanceof BigDecimal) {
                tamanho = ((BigDecimal) modeloTabela.getValueAt(index, 6)).intValue();
            }

            if (modeloTabela.getValueAt(index, 7) instanceof Integer) {
                vlrDecimais = (Integer) modeloTabela.getValueAt(index, 7);
            } else if (modeloTabela.getValueAt(index, 7) instanceof BigDecimal) {
                vlrDecimais = ((BigDecimal) modeloTabela.getValueAt(index, 7)).intValue();
            }

            if (modeloTabela.getValueAt(index, 0) instanceof Integer) {
                sequencia = (Integer) modeloTabela.getValueAt(index, 0);
            } else if (modeloTabela.getValueAt(index, 0) instanceof BigDecimal) {
                sequencia = ((BigDecimal) modeloTabela.getValueAt(index, 0)).intValue();
            }


            codTipoUso = (Boolean) modeloTabela.getValueAt(index, 9);
            indContemTipoReg = (Boolean) modeloTabela.getValueAt(index, 10);
            indChavePrimaria = (Boolean) modeloTabela.getValueAt(index, 8);

            if (posInicial == null || posFinal == null || vlrDecimais == null
                    || sequencia == null) {
                return;
            }

            tpUso = new TipoDeUso();

            temp = new MascaraDeCampo(new MascaraDeCampoPK(
                    (String) modeloTabela.getValueAt(index, 1),
                    txtMascaraDeRegistro.getText(),
                    Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())), posInicial.shortValue(),
                    posFinal.shortValue(), tamanho.shortValue(),
                    vlrDecimais.shortValue(), indContemTipoReg, indChavePrimaria, codTipoUso,
                    sequencia.shortValue());


            /* cria o objeto tipo de uso */
            tpUso.setCodTipoDeUso(codTipoUso);

            temp.setTipoDeCampo(_tipoDeCampo);
            temp.setTipoDeOrigem(_tipoDeOrigem);
            temp.setTipoDeUso(tpUso);


            mascaraRegistroController =
                    new MascaraDeRegistroJpaController();

            mre = mascaraRegistroController.findMascaraDeRegistro(
                    new MascaraDeRegistroPK(txtMascaraDeRegistro.getText(),
                    Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())));

            temp.setMascaraDeRegistro(mre);

            alterar.addElement(temp);
        }


        modeloTabela.updateTable();
        btnSalvar.setEnabled(true);

    }

    public void notifyInsertTable(int row, int col) {

        if (!inserirLinhas.contains(row)) {
            inserirLinhas.addElement(row);
        }

    }

    public void notifyDeleteTable(int row, int col) {

        if (inserirLinhas.contains(row)) /* linha sendo inserida */ {
            modeloTabela.updateTable();
            inserirLinhas.removeElement(row);
            return;
        }

        if (codMascaraRegistro.isEmpty() || codMascaraCampo.isEmpty()
                || numSequencia < 0) {
            return;
        }

        /* Inseri entrada para deletar na tabela */
        deletar.add(new MascaraDeCampoPK(codMascaraCampo,
                codMascaraRegistro, Short.valueOf((String) jComboBoxNumVersao.getSelectedItem())));
        btnSalvar.setEnabled(true);
    }

    private void atualizaComboVersao() {
        MascaraDeCampoJpaController mascaraController =
                new MascaraDeCampoJpaController();

        updateComboVersion = true;

        List<MascaraDeCampo> list = mascaraController.findMascaraDeCampoEntities();
        Iterator<MascaraDeCampo> iteratorMascara = list.iterator();
        elementsComboVersao.removeAllElements();
        List noDup = new ArrayList();


        /* preenche lista com todas as versões */
        while (iteratorMascara.hasNext()) {
            MascaraDeCampo temp = iteratorMascara.next();
            noDup.add(temp.getMascaraDeCampoPK().getNumVersaoMascaraDeRegistro());
        }


        /* retira as duplicatas */

        HashSet set = new HashSet(noDup);
        Iterator itComboVersao = set.iterator();

        List ordenar = new ArrayList();

        while (itComboVersao.hasNext()) {
            ordenar.add((Short) itComboVersao.next());
        }

        /* ordena as versoes */
        Collections.sort(ordenar);
        itComboVersao = ordenar.iterator();

        jComboBoxNumVersao.removeAllItems();

        while (itComboVersao.hasNext()) {
            String t = Short.toString((Short) itComboVersao.next());
            jComboBoxNumVersao.addItem(t);
            elementsComboVersao.addElement(t);
        }

        updateComboVersion = false;
        jComboBoxNumVersao.setSelectedIndex(jComboBoxNumVersao.getItemCount() - 1);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionarLinha;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnProximo;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnRemoverReg;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton jButtonCopiar;
    private javax.swing.JComboBox jComboBoxNumVersao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCodMascara;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JTable tblManutencao;
    private javax.swing.JTextField txtDescricaoRegistro;
    private javax.swing.JTextField txtMascaraDeRegistro;
    // End of variables declaration//GEN-END:variables
}
