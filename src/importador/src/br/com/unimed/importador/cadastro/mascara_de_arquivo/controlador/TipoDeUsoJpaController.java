/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.IllegalOrphanException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeUso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampo;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author paulo
 */
public class TipoDeUsoJpaController {

    public TipoDeUsoJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoDeUso tipoDeUso) throws PreexistingEntityException, Exception {
        if (tipoDeUso.getMascaraDeCampoCollection() == null) {
            tipoDeUso.setMascaraDeCampoCollection(new ArrayList<MascaraDeCampo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollection = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampoToAttach : tipoDeUso.getMascaraDeCampoCollection()) {
                mascaraDeCampoCollectionMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollection.add(mascaraDeCampoCollectionMascaraDeCampoToAttach);
            }
            tipoDeUso.setMascaraDeCampoCollection(attachedMascaraDeCampoCollection);
            em.persist(tipoDeUso);
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampo : tipoDeUso.getMascaraDeCampoCollection()) {
                TipoDeUso oldTipoDeUsoOfMascaraDeCampoCollectionMascaraDeCampo = mascaraDeCampoCollectionMascaraDeCampo.getTipoDeUso();
                mascaraDeCampoCollectionMascaraDeCampo.setTipoDeUso(tipoDeUso);
                mascaraDeCampoCollectionMascaraDeCampo = em.merge(mascaraDeCampoCollectionMascaraDeCampo);
                if (oldTipoDeUsoOfMascaraDeCampoCollectionMascaraDeCampo != null) {
                    oldTipoDeUsoOfMascaraDeCampoCollectionMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionMascaraDeCampo);
                    oldTipoDeUsoOfMascaraDeCampoCollectionMascaraDeCampo = em.merge(oldTipoDeUsoOfMascaraDeCampoCollectionMascaraDeCampo);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipoDeUso(tipoDeUso.getCodTipoDeUso()) != null) {
                throw new PreexistingEntityException("TipoDeUso " + tipoDeUso + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoDeUso tipoDeUso) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDeUso persistentTipoDeUso = em.find(TipoDeUso.class, tipoDeUso.getCodTipoDeUso());
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOld = persistentTipoDeUso.getMascaraDeCampoCollection();
            Collection<MascaraDeCampo> mascaraDeCampoCollectionNew = tipoDeUso.getMascaraDeCampoCollection();
            List<String> illegalOrphanMessages = null;
            for (MascaraDeCampo mascaraDeCampoCollectionOldMascaraDeCampo : mascaraDeCampoCollectionOld) {
                if (!mascaraDeCampoCollectionNew.contains(mascaraDeCampoCollectionOldMascaraDeCampo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MascaraDeCampo " + mascaraDeCampoCollectionOldMascaraDeCampo + " since its tipoDeUso field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollectionNew = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampoToAttach : mascaraDeCampoCollectionNew) {
                mascaraDeCampoCollectionNewMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollectionNew.add(mascaraDeCampoCollectionNewMascaraDeCampoToAttach);
            }
            mascaraDeCampoCollectionNew = attachedMascaraDeCampoCollectionNew;
            tipoDeUso.setMascaraDeCampoCollection(mascaraDeCampoCollectionNew);
            tipoDeUso = em.merge(tipoDeUso);
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampo : mascaraDeCampoCollectionNew) {
                if (!mascaraDeCampoCollectionOld.contains(mascaraDeCampoCollectionNewMascaraDeCampo)) {
                    TipoDeUso oldTipoDeUsoOfMascaraDeCampoCollectionNewMascaraDeCampo = mascaraDeCampoCollectionNewMascaraDeCampo.getTipoDeUso();
                    mascaraDeCampoCollectionNewMascaraDeCampo.setTipoDeUso(tipoDeUso);
                    mascaraDeCampoCollectionNewMascaraDeCampo = em.merge(mascaraDeCampoCollectionNewMascaraDeCampo);
                    if (oldTipoDeUsoOfMascaraDeCampoCollectionNewMascaraDeCampo != null && !oldTipoDeUsoOfMascaraDeCampoCollectionNewMascaraDeCampo.equals(tipoDeUso)) {
                        oldTipoDeUsoOfMascaraDeCampoCollectionNewMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionNewMascaraDeCampo);
                        oldTipoDeUsoOfMascaraDeCampoCollectionNewMascaraDeCampo = em.merge(oldTipoDeUsoOfMascaraDeCampoCollectionNewMascaraDeCampo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Boolean id = tipoDeUso.getCodTipoDeUso();
                if (findTipoDeUso(id) == null) {
                    throw new NonexistentEntityException("The tipoDeUso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Boolean id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDeUso tipoDeUso;
            try {
                tipoDeUso = em.getReference(TipoDeUso.class, id);
                tipoDeUso.getCodTipoDeUso();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoDeUso with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOrphanCheck = tipoDeUso.getMascaraDeCampoCollection();
            for (MascaraDeCampo mascaraDeCampoCollectionOrphanCheckMascaraDeCampo : mascaraDeCampoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoDeUso (" + tipoDeUso + ") cannot be destroyed since the MascaraDeCampo " + mascaraDeCampoCollectionOrphanCheckMascaraDeCampo + " in its mascaraDeCampoCollection field has a non-nullable tipoDeUso field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoDeUso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoDeUso> findTipoDeUsoEntities() {
        return findTipoDeUsoEntities(true, -1, -1);
    }

    public List<TipoDeUso> findTipoDeUsoEntities(int maxResults, int firstResult) {
        return findTipoDeUsoEntities(false, maxResults, firstResult);
    }

    private List<TipoDeUso> findTipoDeUsoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoDeUso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoDeUso findTipoDeUso(Boolean id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoDeUso.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoDeUsoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoDeUso> rt = cq.from(TipoDeUso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
