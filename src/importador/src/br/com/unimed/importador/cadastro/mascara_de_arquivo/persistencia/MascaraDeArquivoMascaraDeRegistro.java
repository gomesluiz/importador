/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "mascara_de_arquivo_mascara_de_registro")
@NamedQueries({
    @NamedQuery(name = "MascaraDeArquivoMascaraDeRegistro.findAll", query = "SELECT m FROM MascaraDeArquivoMascaraDeRegistro m"),
    @NamedQuery(name = "MascaraDeArquivoMascaraDeRegistro.findByCodMascaraDeArquivo", query = "SELECT m FROM MascaraDeArquivoMascaraDeRegistro m WHERE m.mascaraDeArquivoMascaraDeRegistroPK.codMascaraDeArquivo = :codMascaraDeArquivo"),
    @NamedQuery(name = "MascaraDeArquivoMascaraDeRegistro.findByNumVersaoMascaraDeArquivo", query = "SELECT m FROM MascaraDeArquivoMascaraDeRegistro m WHERE m.mascaraDeArquivoMascaraDeRegistroPK.numVersaoMascaraDeArquivo = :numVersaoMascaraDeArquivo"),
    @NamedQuery(name = "MascaraDeArquivoMascaraDeRegistro.findByCodMascaraDeRegistro", query = "SELECT m FROM MascaraDeArquivoMascaraDeRegistro m WHERE m.mascaraDeArquivoMascaraDeRegistroPK.codMascaraDeRegistro = :codMascaraDeRegistro"),
    @NamedQuery(name = "MascaraDeArquivoMascaraDeRegistro.findByNumVersaoMascaraDeRegistro", query = "SELECT m FROM MascaraDeArquivoMascaraDeRegistro m WHERE m.mascaraDeArquivoMascaraDeRegistroPK.numVersaoMascaraDeRegistro = :numVersaoMascaraDeRegistro"),
    @NamedQuery(name = "MascaraDeArquivoMascaraDeRegistro.findByNumSequencialRegistro", query = "SELECT m FROM MascaraDeArquivoMascaraDeRegistro m WHERE m.numSequencialRegistro = :numSequencialRegistro")})
public class MascaraDeArquivoMascaraDeRegistro implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MascaraDeArquivoMascaraDeRegistroPK mascaraDeArquivoMascaraDeRegistroPK;
    @Basic(optional = false)
    @Column(name = "num_sequencial_registro")
    private short numSequencialRegistro;
    @JoinColumns({
        @JoinColumn(name = "cod_mascara_de_registro", referencedColumnName = "cod_mascara_de_registro", insertable = false, updatable = false),
        @JoinColumn(name = "num_versao_mascara_de_registro", referencedColumnName = "num_versao_mascara_de_registro", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private MascaraDeRegistro mascaraDeRegistro;
    @JoinColumns({
        @JoinColumn(name = "cod_mascara_de_arquivo", referencedColumnName = "cod_mascara_de_arquivo", insertable = false, updatable = false),
        @JoinColumn(name = "num_versao_mascara_de_arquivo", referencedColumnName = "num_versao_mascara_de_arquivo", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private MascaraDeArquivo mascaraDeArquivo;

    public MascaraDeArquivoMascaraDeRegistro() {
    }

    public MascaraDeArquivoMascaraDeRegistro(MascaraDeArquivoMascaraDeRegistroPK mascaraDeArquivoMascaraDeRegistroPK) {
        this.mascaraDeArquivoMascaraDeRegistroPK = mascaraDeArquivoMascaraDeRegistroPK;
    }

    public MascaraDeArquivoMascaraDeRegistro(MascaraDeArquivoMascaraDeRegistroPK mascaraDeArquivoMascaraDeRegistroPK, short numSequencialRegistro) {
        this.mascaraDeArquivoMascaraDeRegistroPK = mascaraDeArquivoMascaraDeRegistroPK;
        this.numSequencialRegistro = numSequencialRegistro;
    }

    public MascaraDeArquivoMascaraDeRegistro(String codMascaraDeArquivo, short numVersaoMascaraDeArquivo, String codMascaraDeRegistro, short numVersaoMascaraDeRegistro) {
        this.mascaraDeArquivoMascaraDeRegistroPK = new MascaraDeArquivoMascaraDeRegistroPK(codMascaraDeArquivo, numVersaoMascaraDeArquivo, codMascaraDeRegistro, numVersaoMascaraDeRegistro);
    }

    public MascaraDeArquivoMascaraDeRegistroPK getMascaraDeArquivoMascaraDeRegistroPK() {
        return mascaraDeArquivoMascaraDeRegistroPK;
    }

    public void setMascaraDeArquivoMascaraDeRegistroPK(MascaraDeArquivoMascaraDeRegistroPK mascaraDeArquivoMascaraDeRegistroPK) {
        this.mascaraDeArquivoMascaraDeRegistroPK = mascaraDeArquivoMascaraDeRegistroPK;
    }

    public short getNumSequencialRegistro() {
        return numSequencialRegistro;
    }

    public void setNumSequencialRegistro(short numSequencialRegistro) {
        this.numSequencialRegistro = numSequencialRegistro;
    }

    public MascaraDeRegistro getMascaraDeRegistro() {
        return mascaraDeRegistro;
    }

    public void setMascaraDeRegistro(MascaraDeRegistro mascaraDeRegistro) {
        this.mascaraDeRegistro = mascaraDeRegistro;
    }

    public MascaraDeArquivo getMascaraDeArquivo() {
        return mascaraDeArquivo;
    }

    public void setMascaraDeArquivo(MascaraDeArquivo mascaraDeArquivo) {
        this.mascaraDeArquivo = mascaraDeArquivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mascaraDeArquivoMascaraDeRegistroPK != null ? mascaraDeArquivoMascaraDeRegistroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MascaraDeArquivoMascaraDeRegistro)) {
            return false;
        }
        MascaraDeArquivoMascaraDeRegistro other = (MascaraDeArquivoMascaraDeRegistro) object;
        if ((this.mascaraDeArquivoMascaraDeRegistroPK == null && other.mascaraDeArquivoMascaraDeRegistroPK != null) || (this.mascaraDeArquivoMascaraDeRegistroPK != null && !this.mascaraDeArquivoMascaraDeRegistroPK.equals(other.mascaraDeArquivoMascaraDeRegistroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivoMascaraDeRegistro[mascaraDeArquivoMascaraDeRegistroPK=" + mascaraDeArquivoMascaraDeRegistroPK + "]";
    }

}
