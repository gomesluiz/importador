/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "mascara_de_campo")
@NamedQueries({
    @NamedQuery(name = "MascaraDeCampo.findAll", query = "SELECT m FROM MascaraDeCampo m"),
    @NamedQuery(name = "MascaraDeCampo.findByCodMascaraDeCampo", query = "SELECT m FROM MascaraDeCampo m WHERE m.mascaraDeCampoPK.codMascaraDeCampo = :codMascaraDeCampo"),
    @NamedQuery(name = "MascaraDeCampo.findByCodMascaraDeRegistro", query = "SELECT m FROM MascaraDeCampo m WHERE m.mascaraDeCampoPK.codMascaraDeRegistro = :codMascaraDeRegistro"),
    @NamedQuery(name = "MascaraDeCampo.findByNumVersaoMascaraDeRegistro", query = "SELECT m FROM MascaraDeCampo m WHERE m.mascaraDeCampoPK.numVersaoMascaraDeRegistro = :numVersaoMascaraDeRegistro"),
    @NamedQuery(name = "MascaraDeCampo.findByVlrPosicaoInicial", query = "SELECT m FROM MascaraDeCampo m WHERE m.vlrPosicaoInicial = :vlrPosicaoInicial"),
    @NamedQuery(name = "MascaraDeCampo.findByVlrPosicaoFinal", query = "SELECT m FROM MascaraDeCampo m WHERE m.vlrPosicaoFinal = :vlrPosicaoFinal"),
    @NamedQuery(name = "MascaraDeCampo.findByVlrTamanho", query = "SELECT m FROM MascaraDeCampo m WHERE m.vlrTamanho = :vlrTamanho"),
    @NamedQuery(name = "MascaraDeCampo.findByVlrDecimais", query = "SELECT m FROM MascaraDeCampo m WHERE m.vlrDecimais = :vlrDecimais"),
    @NamedQuery(name = "MascaraDeCampo.findByIndContemTipoDeRegistro", query = "SELECT m FROM MascaraDeCampo m WHERE m.indContemTipoDeRegistro = :indContemTipoDeRegistro"),
    @NamedQuery(name = "MascaraDeCampo.findByIndChavePrimaria", query = "SELECT m FROM MascaraDeCampo m WHERE m.indChavePrimaria = :indChavePrimaria"),
    @NamedQuery(name = "MascaraDeCampo.findByIndContemNumeroDeVersao", query = "SELECT m FROM MascaraDeCampo m WHERE m.indContemNumeroDeVersao = :indContemNumeroDeVersao"),
    @NamedQuery(name = "MascaraDeCampo.findByNumSequencialCampo", query = "SELECT m FROM MascaraDeCampo m WHERE m.numSequencialCampo = :numSequencialCampo")})
public class MascaraDeCampo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MascaraDeCampoPK mascaraDeCampoPK;
    @Basic(optional = false)
    @Column(name = "vlr_posicao_inicial")
    private short vlrPosicaoInicial;
    @Basic(optional = false)
    @Column(name = "vlr_posicao_final")
    private short vlrPosicaoFinal;
    @Basic(optional = false)
    @Column(name = "vlr_tamanho")
    private short vlrTamanho;
    @Basic(optional = false)
    @Column(name = "vlr_decimais")
    private short vlrDecimais;
    @Basic(optional = false)
    @Column(name = "ind_contem_tipo_de_registro")
    private boolean indContemTipoDeRegistro;
    @Basic(optional = false)
    @Column(name = "ind_chave_primaria")
    private boolean indChavePrimaria;
    @Basic(optional = false)
    @Column(name = "ind_contem_numero_de_versao")
    private boolean indContemNumeroDeVersao;
    @Basic(optional = false)
    @Column(name = "num_sequencial_campo")
    private short numSequencialCampo;
    @JoinColumn(name = "cod_tipo_de_uso", referencedColumnName = "cod_tipo_de_uso")
    @ManyToOne(optional = false)
    private TipoDeUso tipoDeUso;
    @JoinColumn(name = "cod_tipo_de_origem", referencedColumnName = "cod_tipo_de_origem")
    @ManyToOne(optional = false)
    private TipoDeOrigem tipoDeOrigem;
    @JoinColumn(name = "cod_tipo_de_campo", referencedColumnName = "cod_tipo_de_campo")
    @ManyToOne(optional = false)
    private TipoDeCampo tipoDeCampo;
    @JoinColumns({
        @JoinColumn(name = "cod_mascara_de_registro", referencedColumnName = "cod_mascara_de_registro", insertable = false, updatable = false),
        @JoinColumn(name = "num_versao_mascara_de_registro", referencedColumnName = "num_versao_mascara_de_registro", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private MascaraDeRegistro mascaraDeRegistro;

    public MascaraDeCampo() {
    }

    public MascaraDeCampo(MascaraDeCampoPK mascaraDeCampoPK) {
        this.mascaraDeCampoPK = mascaraDeCampoPK;
    }

    public MascaraDeCampo(MascaraDeCampoPK mascaraDeCampoPK, short vlrPosicaoInicial, short vlrPosicaoFinal, short vlrTamanho, short vlrDecimais, boolean indContemTipoDeRegistro, boolean indChavePrimaria, boolean indContemNumeroDeVersao, short numSequencialCampo) {
        this.mascaraDeCampoPK = mascaraDeCampoPK;
        this.vlrPosicaoInicial = vlrPosicaoInicial;
        this.vlrPosicaoFinal = vlrPosicaoFinal;
        this.vlrTamanho = vlrTamanho;
        this.vlrDecimais = vlrDecimais;
        this.indContemTipoDeRegistro = indContemTipoDeRegistro;
        this.indChavePrimaria = indChavePrimaria;
        this.indContemNumeroDeVersao = indContemNumeroDeVersao;
        this.numSequencialCampo = numSequencialCampo;
    }

    public MascaraDeCampo(String codMascaraDeCampo, String codMascaraDeRegistro, short numVersaoMascaraDeRegistro) {
        this.mascaraDeCampoPK = new MascaraDeCampoPK(codMascaraDeCampo, codMascaraDeRegistro, numVersaoMascaraDeRegistro);
    }

    public MascaraDeCampoPK getMascaraDeCampoPK() {
        return mascaraDeCampoPK;
    }

    public void setMascaraDeCampoPK(MascaraDeCampoPK mascaraDeCampoPK) {
        this.mascaraDeCampoPK = mascaraDeCampoPK;
    }

    public short getVlrPosicaoInicial() {
        return vlrPosicaoInicial;
    }

    public void setVlrPosicaoInicial(short vlrPosicaoInicial) {
        this.vlrPosicaoInicial = vlrPosicaoInicial;
    }

    public short getVlrPosicaoFinal() {
        return vlrPosicaoFinal;
    }

    public void setVlrPosicaoFinal(short vlrPosicaoFinal) {
        this.vlrPosicaoFinal = vlrPosicaoFinal;
    }

    public short getVlrTamanho() {
        return vlrTamanho;
    }

    public void setVlrTamanho(short vlrTamanho) {
        this.vlrTamanho = vlrTamanho;
    }

    public short getVlrDecimais() {
        return vlrDecimais;
    }

    public void setVlrDecimais(short vlrDecimais) {
        this.vlrDecimais = vlrDecimais;
    }

    public boolean getIndContemTipoDeRegistro() {
        return indContemTipoDeRegistro;
    }

    public void setIndContemTipoDeRegistro(boolean indContemTipoDeRegistro) {
        this.indContemTipoDeRegistro = indContemTipoDeRegistro;
    }

    public boolean getIndChavePrimaria() {
        return indChavePrimaria;
    }

    public void setIndChavePrimaria(boolean indChavePrimaria) {
        this.indChavePrimaria = indChavePrimaria;
    }

    public boolean getIndContemNumeroDeVersao() {
        return indContemNumeroDeVersao;
    }

    public void setIndContemNumeroDeVersao(boolean indContemNumeroDeVersao) {
        this.indContemNumeroDeVersao = indContemNumeroDeVersao;
    }

    public short getNumSequencialCampo() {
        return numSequencialCampo;
    }

    public void setNumSequencialCampo(short numSequencialCampo) {
        this.numSequencialCampo = numSequencialCampo;
    }

    public TipoDeUso getTipoDeUso() {
        return tipoDeUso;
    }

    public void setTipoDeUso(TipoDeUso tipoDeUso) {
        this.tipoDeUso = tipoDeUso;
    }

    public TipoDeOrigem getTipoDeOrigem() {
        return tipoDeOrigem;
    }

    public void setTipoDeOrigem(TipoDeOrigem tipoDeOrigem) {
        this.tipoDeOrigem = tipoDeOrigem;
    }

    public TipoDeCampo getTipoDeCampo() {
        return tipoDeCampo;
    }

    public void setTipoDeCampo(TipoDeCampo tipoDeCampo) {
        this.tipoDeCampo = tipoDeCampo;
    }

    public MascaraDeRegistro getMascaraDeRegistro() {
        return mascaraDeRegistro;
    }

    public void setMascaraDeRegistro(MascaraDeRegistro mascaraDeRegistro) {
        this.mascaraDeRegistro = mascaraDeRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mascaraDeCampoPK != null ? mascaraDeCampoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MascaraDeCampo)) {
            return false;
        }
        MascaraDeCampo other = (MascaraDeCampo) object;
        if ((this.mascaraDeCampoPK == null && other.mascaraDeCampoPK != null) || (this.mascaraDeCampoPK != null && !this.mascaraDeCampoPK.equals(other.mascaraDeCampoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampo[mascaraDeCampoPK=" + mascaraDeCampoPK + "]";
    }

}
