/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "tipo_de_campo")
@NamedQueries({
    @NamedQuery(name = "TipoDeCampo.findAll", query = "SELECT t FROM TipoDeCampo t"),
    @NamedQuery(name = "TipoDeCampo.findByCodTipoDeCampo", query = "SELECT t FROM TipoDeCampo t WHERE t.codTipoDeCampo = :codTipoDeCampo"),
    @NamedQuery(name = "TipoDeCampo.findByDscTipoDeCampo", query = "SELECT t FROM TipoDeCampo t WHERE t.dscTipoDeCampo = :dscTipoDeCampo"),
    @NamedQuery(name = "TipoDeCampo.findByCodTipoDeCampoNoDestino", query = "SELECT t FROM TipoDeCampo t WHERE t.codTipoDeCampoNoDestino = :codTipoDeCampoNoDestino"),
    @NamedQuery(name = "TipoDeCampo.findByDscFormatoTipoDeCampo", query = "SELECT t FROM TipoDeCampo t WHERE t.dscFormatoTipoDeCampo = :dscFormatoTipoDeCampo")})
public class TipoDeCampo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_tipo_de_campo")
    private Short codTipoDeCampo;
    @Basic(optional = false)
    @Column(name = "dsc_tipo_de_campo")
    private String dscTipoDeCampo;
    @Basic(optional = false)
    @Column(name = "cod_tipo_de_campo_no_destino")
    private String codTipoDeCampoNoDestino;
    @Column(name = "dsc_formato_tipo_de_campo")
    private String dscFormatoTipoDeCampo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDeCampo")
    private Collection<MascaraDeCampo> mascaraDeCampoCollection;

    public TipoDeCampo() {
    }

    public TipoDeCampo(Short codTipoDeCampo) {
        this.codTipoDeCampo = codTipoDeCampo;
    }

    public TipoDeCampo(Short codTipoDeCampo, String dscTipoDeCampo, String codTipoDeCampoNoDestino) {
        this.codTipoDeCampo = codTipoDeCampo;
        this.dscTipoDeCampo = dscTipoDeCampo;
        this.codTipoDeCampoNoDestino = codTipoDeCampoNoDestino;
    }

    public Short getCodTipoDeCampo() {
        return codTipoDeCampo;
    }

    public void setCodTipoDeCampo(Short codTipoDeCampo) {
        this.codTipoDeCampo = codTipoDeCampo;
    }

    public String getDscTipoDeCampo() {
        return dscTipoDeCampo;
    }

    public void setDscTipoDeCampo(String dscTipoDeCampo) {
        this.dscTipoDeCampo = dscTipoDeCampo;
    }

    public String getCodTipoDeCampoNoDestino() {
        return codTipoDeCampoNoDestino;
    }

    public void setCodTipoDeCampoNoDestino(String codTipoDeCampoNoDestino) {
        this.codTipoDeCampoNoDestino = codTipoDeCampoNoDestino;
    }

    public String getDscFormatoTipoDeCampo() {
        return dscFormatoTipoDeCampo;
    }

    public void setDscFormatoTipoDeCampo(String dscFormatoTipoDeCampo) {
        this.dscFormatoTipoDeCampo = dscFormatoTipoDeCampo;
    }

    public Collection<MascaraDeCampo> getMascaraDeCampoCollection() {
        return mascaraDeCampoCollection;
    }

    public void setMascaraDeCampoCollection(Collection<MascaraDeCampo> mascaraDeCampoCollection) {
        this.mascaraDeCampoCollection = mascaraDeCampoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoDeCampo != null ? codTipoDeCampo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDeCampo)) {
            return false;
        }
        TipoDeCampo other = (TipoDeCampo) object;
        if ((this.codTipoDeCampo == null && other.codTipoDeCampo != null) || (this.codTipoDeCampo != null && !this.codTipoDeCampo.equals(other.codTipoDeCampo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeCampo[codTipoDeCampo=" + codTipoDeCampo + "]";
    }

}
