/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "tipo_de_origem")
@NamedQueries({
    @NamedQuery(name = "TipoDeOrigem.findAll", query = "SELECT t FROM TipoDeOrigem t"),
    @NamedQuery(name = "TipoDeOrigem.findByCodTipoDeOrigem", query = "SELECT t FROM TipoDeOrigem t WHERE t.codTipoDeOrigem = :codTipoDeOrigem"),
    @NamedQuery(name = "TipoDeOrigem.findByDscTipoDeOrigem", query = "SELECT t FROM TipoDeOrigem t WHERE t.dscTipoDeOrigem = :dscTipoDeOrigem")})
public class TipoDeOrigem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_tipo_de_origem")
    private Short codTipoDeOrigem;
    @Basic(optional = false)
    @Column(name = "dsc_tipo_de_origem")
    private String dscTipoDeOrigem;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDeOrigem")
    private Collection<MascaraDeCampo> mascaraDeCampoCollection;

    public TipoDeOrigem() {
    }

    public TipoDeOrigem(Short codTipoDeOrigem) {
        this.codTipoDeOrigem = codTipoDeOrigem;
    }

    public TipoDeOrigem(Short codTipoDeOrigem, String dscTipoDeOrigem) {
        this.codTipoDeOrigem = codTipoDeOrigem;
        this.dscTipoDeOrigem = dscTipoDeOrigem;
    }

    public Short getCodTipoDeOrigem() {
        return codTipoDeOrigem;
    }

    public void setCodTipoDeOrigem(Short codTipoDeOrigem) {
        this.codTipoDeOrigem = codTipoDeOrigem;
    }

    public String getDscTipoDeOrigem() {
        return dscTipoDeOrigem;
    }

    public void setDscTipoDeOrigem(String dscTipoDeOrigem) {
        this.dscTipoDeOrigem = dscTipoDeOrigem;
    }

    public Collection<MascaraDeCampo> getMascaraDeCampoCollection() {
        return mascaraDeCampoCollection;
    }

    public void setMascaraDeCampoCollection(Collection<MascaraDeCampo> mascaraDeCampoCollection) {
        this.mascaraDeCampoCollection = mascaraDeCampoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoDeOrigem != null ? codTipoDeOrigem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDeOrigem)) {
            return false;
        }
        TipoDeOrigem other = (TipoDeOrigem) object;
        if ((this.codTipoDeOrigem == null && other.codTipoDeOrigem != null) || (this.codTipoDeOrigem != null && !this.codTipoDeOrigem.equals(other.codTipoDeOrigem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeOrigem[codTipoDeOrigem=" + codTipoDeOrigem + "]";
    }

}
