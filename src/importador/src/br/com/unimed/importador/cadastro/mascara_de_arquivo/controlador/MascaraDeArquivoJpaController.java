/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.IllegalOrphanException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivo;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivoPK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivoMascaraDeRegistro;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author paulo
 */
public class MascaraDeArquivoJpaController {

    public MascaraDeArquivoJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MascaraDeArquivo mascaraDeArquivo) throws PreexistingEntityException, Exception {
        if (mascaraDeArquivo.getMascaraDeArquivoPK() == null) {
            mascaraDeArquivo.setMascaraDeArquivoPK(new MascaraDeArquivoPK());
        }
        if (mascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection() == null) {
            mascaraDeArquivo.setMascaraDeArquivoMascaraDeRegistroCollection(new ArrayList<MascaraDeArquivoMascaraDeRegistro>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<MascaraDeArquivoMascaraDeRegistro> attachedMascaraDeArquivoMascaraDeRegistroCollection = new ArrayList<MascaraDeArquivoMascaraDeRegistro>();
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach : mascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection()) {
                mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach = em.getReference(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach.getClass(), mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach.getMascaraDeArquivoMascaraDeRegistroPK());
                attachedMascaraDeArquivoMascaraDeRegistroCollection.add(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach);
            }
            mascaraDeArquivo.setMascaraDeArquivoMascaraDeRegistroCollection(attachedMascaraDeArquivoMascaraDeRegistroCollection);
            em.persist(mascaraDeArquivo);
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro : mascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection()) {
                MascaraDeArquivo oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro = mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo();
                mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro.setMascaraDeArquivo(mascaraDeArquivo);
                mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro = em.merge(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro);
                if (oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro != null) {
                    oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro);
                    oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro = em.merge(oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMascaraDeArquivo(mascaraDeArquivo.getMascaraDeArquivoPK()) != null) {
                throw new PreexistingEntityException("MascaraDeArquivo " + mascaraDeArquivo + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MascaraDeArquivo mascaraDeArquivo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeArquivo persistentMascaraDeArquivo = em.find(MascaraDeArquivo.class, mascaraDeArquivo.getMascaraDeArquivoPK());
            Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollectionOld = persistentMascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection();
            Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollectionNew = mascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection();
            List<String> illegalOrphanMessages = null;
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionOldMascaraDeArquivoMascaraDeRegistro : mascaraDeArquivoMascaraDeRegistroCollectionOld) {
                if (!mascaraDeArquivoMascaraDeRegistroCollectionNew.contains(mascaraDeArquivoMascaraDeRegistroCollectionOldMascaraDeArquivoMascaraDeRegistro)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MascaraDeArquivoMascaraDeRegistro " + mascaraDeArquivoMascaraDeRegistroCollectionOldMascaraDeArquivoMascaraDeRegistro + " since its mascaraDeArquivo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<MascaraDeArquivoMascaraDeRegistro> attachedMascaraDeArquivoMascaraDeRegistroCollectionNew = new ArrayList<MascaraDeArquivoMascaraDeRegistro>();
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach : mascaraDeArquivoMascaraDeRegistroCollectionNew) {
                mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach = em.getReference(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach.getClass(), mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach.getMascaraDeArquivoMascaraDeRegistroPK());
                attachedMascaraDeArquivoMascaraDeRegistroCollectionNew.add(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach);
            }
            mascaraDeArquivoMascaraDeRegistroCollectionNew = attachedMascaraDeArquivoMascaraDeRegistroCollectionNew;
            mascaraDeArquivo.setMascaraDeArquivoMascaraDeRegistroCollection(mascaraDeArquivoMascaraDeRegistroCollectionNew);
            mascaraDeArquivo = em.merge(mascaraDeArquivo);
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro : mascaraDeArquivoMascaraDeRegistroCollectionNew) {
                if (!mascaraDeArquivoMascaraDeRegistroCollectionOld.contains(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro)) {
                    MascaraDeArquivo oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro = mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo();
                    mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.setMascaraDeArquivo(mascaraDeArquivo);
                    mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro = em.merge(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro);
                    if (oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro != null && !oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.equals(mascaraDeArquivo)) {
                        oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro);
                        oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro = em.merge(oldMascaraDeArquivoOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                MascaraDeArquivoPK id = mascaraDeArquivo.getMascaraDeArquivoPK();
                if (findMascaraDeArquivo(id) == null) {
                    throw new NonexistentEntityException("The mascaraDeArquivo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(MascaraDeArquivoPK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeArquivo mascaraDeArquivo;
            try {
                mascaraDeArquivo = em.getReference(MascaraDeArquivo.class, id);
                mascaraDeArquivo.getMascaraDeArquivoPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mascaraDeArquivo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheck = mascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection();
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheckMascaraDeArquivoMascaraDeRegistro : mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This MascaraDeArquivo (" + mascaraDeArquivo + ") cannot be destroyed since the MascaraDeArquivoMascaraDeRegistro " + mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheckMascaraDeArquivoMascaraDeRegistro + " in its mascaraDeArquivoMascaraDeRegistroCollection field has a non-nullable mascaraDeArquivo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(mascaraDeArquivo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MascaraDeArquivo> findMascaraDeArquivoEntities() {
        return findMascaraDeArquivoEntities(true, -1, -1);
    }

    public List<MascaraDeArquivo> findMascaraDeArquivoEntities(int maxResults, int firstResult) {
        return findMascaraDeArquivoEntities(false, maxResults, firstResult);
    }

    private List<MascaraDeArquivo> findMascaraDeArquivoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MascaraDeArquivo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MascaraDeArquivo findMascaraDeArquivo(MascaraDeArquivoPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MascaraDeArquivo.class, id);
        } finally {
            em.close();
        }
    }

    public int getMascaraDeArquivoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MascaraDeArquivo> rt = cq.from(MascaraDeArquivo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
