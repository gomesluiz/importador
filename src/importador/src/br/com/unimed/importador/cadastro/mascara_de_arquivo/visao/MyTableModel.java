/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.cadastro.mascara_de_arquivo.visao;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 * Classe responsável em criar o modelo de apresentação da tabela
 * @author Equipe Affirmare
 */
public class MyTableModel extends AbstractTableModel {

    private Object rowData[][] = {{}};
    private Object columnNames[] = {};
    private Vector vecRowData = null;
    private Vector vecColumnNames = null;
    private boolean inserir = false;
    private static final String TIPO_ORIGEM = "PARAMETRO";

    /**
     * Construtor padrão
     */
    public MyTableModel() {
        vecRowData = convertToVector(rowData);
        vecColumnNames = convertToVector(columnNames);
    }

    /**
     * Envia mensagem para atualizar os dados da tabela
     */
    public void updateTable() {
        this.fireTableDataChanged();

    }

    /**
     * Construtor
     * @param rowData os dados a serem apresentados na tabela
     * @param columnNames cabeçalho da tabela
     */
    public MyTableModel(Object[][] rowData, Object[] columnNames) {
        this.rowData = rowData;
        this.columnNames = columnNames;

        vecRowData = convertToVector(rowData);
        vecColumnNames = convertToVector(columnNames);
    }

    /**
     * Construtor
     * @param vecRowData vector com os dados a serem apresentados na tabela
     * @param vecColumnNames vector com o nome das colunas
     */
    public MyTableModel(Vector vecRowData, Vector vecColumnNames) {
        this.vecColumnNames = vecColumnNames;
        this.vecRowData = vecRowData;
    }

    /**
     * Informa novos dados para serem apresentados na tabela
     * @param rowData Object[][] que representa linha e colunas com os dados
     */
    public void setRowData(Object[][] rowData) {
        this.rowData = rowData;
        vecRowData = convertToVector(rowData);
    }

    /**
     * Informa novo nome para as colunas da tabela
     * @param columnNames Object[] com os dados
     */
    public void setColumnName(Object[] columnNames) {
        this.columnNames = columnNames;
        vecColumnNames = convertToVector(columnNames);
    }

    /**
     * Informa novos dados a serem apresentados na tabela
     * @param data vector com os dados
     */
    public void setRowData(Vector data) {
        this.vecRowData = data;
    }

    /**
     * Informa nome para as colunas na tabela
     * @param vecColumnNames vector com os novos nomes
     */
    public void setColumnName(Vector vecColumnNames) {
        this.vecColumnNames = vecColumnNames;
    }

    /**
     * Apaga todo o conteúdo da tabela
     */
    public void clear() {
        this.vecRowData.clear();

    }

    /**
     * Recuperara o nome da coluna
     * @param column <b> int </b> que especifica a posição da coluna
     * @return <b> String </b> que representa o nome da coluna na tabela.
     */
    @Override
    public String getColumnName(int column) {
        return (String) vecColumnNames.get(column);
    }

    /**
     * Retorna o número de linhas na tabela
     * @return <b> int </b> representando o número de linhas na tabela
     */
    public int getRowCount() {
        return vecRowData.size();
    }

    /**
     * Retorna o número de colunas da tabela
     * @return <b> int <b> que representa o número de colunas.
     */
    public int getColumnCount() {
        return vecColumnNames.size();
    }

    /**
     * Retorna o conteúdo de uma célula da tabela
     * @param row linha para obter o valor
     * @param col coluna para obter o valor
     * @return Object existente no cruzamento de row x col
     */
    public Object getValueAt(int row, int col) {
        return ((Vector) vecRowData.get(row)).get(col);


    }

    /**
     * Permite inserir/alterar o valor de uma célula da tabela
     * @param aValue Novo valor a ser inserido/alterado na tabela
     * @param rowIndex Linha onde se deve inserir/alterar o dado
     * @param columnIndex Coluna onde se deve inserir/alterar o dado
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        try {
            boolean inserindo;

            if (aValue == null) {
                return;
            }

            if (aValue.equals(((Vector) vecRowData.get(rowIndex)).get(columnIndex))) {
                return;
            }

            if (((Vector) vecRowData.get(rowIndex)).get(columnIndex).equals("")) {
                inserindo = true;
            } else {
                inserindo = false;
            }


            //update aValue
            ((Vector) vecRowData.get(rowIndex)).set(columnIndex, aValue);

            if (!inserindo) {
                fireTableCellUpdated(rowIndex, columnIndex);
            } else {
                fireTableRowsInserted(rowIndex, rowIndex);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            return;
        }

    }

    /**
     * Recupera todos os dados da tabela.
     * Para acessar os dados utilize a seguinte sintaxe:
     * <code>
     * Object obj = ((Vector) vecRowData.get(rowIndex)).get(columnIndex));
     * </code>
     * @return <b> Vector <b> com os dados.
     */
    public Vector getData() {
        return vecRowData;
    }

    /**
     * Recupera o tipo de dado presente em cada coluna da tabela.
     * Tipos Integer permite a inserção apenas de valores (Integer).
     * Tipos Boolean permite manusear a tabela através de JCheckBox.
     * @param columnIndex <b> int </b> com a coluna
     * @return <b> Class </b> que representa o tipo de dado
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {

        if (getColumnCount() == 3) {

            switch (columnIndex) {
                case 0:
                    return Integer.class;
                case 1:
                    return String.class;
                case 2:
                    return String.class;

            }

        } else if (getColumnCount() == 11) {
            switch (columnIndex) {
                case 0:
                    return Integer.class;
                case 1:
                    return String.class;
                case 2:
                    return String.class;
                case 3:
                    return String.class;
                case 4:
                    return Integer.class;
                case 5:
                    return Integer.class;
                case 6:
                    return Integer.class;
                case 7:
                    return Integer.class;
                case 8:
                    return Boolean.class;
                case 9:
                    return Boolean.class;
                case 10:
                    return Boolean.class;
            }

        }

        return Object.class;
    }

    /**
     * Define se a célula pode ou não ser editada
     * @param rowIndex linha
     * @param columnIndex coluna
     * @return <b> true </b> se a célula é editavel, <b> false </b> caso contrário
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (getColumnCount() == 3) {


            switch (columnIndex) {
                case 0:
                    return false;
                case 1:
                    return true;
                case 2:
                    return false;
            }


        } else if (getColumnCount() == 11) {
            boolean b = false;

            if (((String) getValueAt(rowIndex, 2)).equals(TIPO_ORIGEM)) {
                b = true;
            }

            if (!inserir) {

                switch (columnIndex) {
                    case 0:
                        return false;
                    case 1:
                        return false;
                    case 4:
                        return !b;
                    case 5:
                        return !b;
                    case 6:
                        return b;
                    default:
                        return true;
                }
            } else {
                switch (columnIndex) {
                    case 0:
                        return false;
                    case 1:
                        return true;
                    case 4:
                        return !b;
                    case 5:
                        return !b;
                    case 6:
                        return b;

                    case 10:
                        return b;
                    default:
                        return true;
                }
            }

        }

        return false;
    }

    /**
     * Inserir nova linha na tabela
     * @param row Vector com os dados a serem inseridos
     */
    public void addRow(Vector row) {
        vecRowData.addElement(row);
        /* evento fireTableRowsInsert(int,int) é gerado no setValueat() */

    }

    /**
     * Deletar uma linha da tabela
     * @param index linha a ser deletada
     */
    public void deleteRow(int index) {
        if (index < 0 || index > vecRowData.size()) {
            return;
        }
        vecRowData.remove(index);
        fireTableRowsDeleted(index, index);
    }

    /**
     * Inserir dados em uma determinada posição da tabela
     * @param row em qual linha se deseja inserir os dados
     * @param index Vector com os dados
     */
    public void addRowIndex(Vector row, int index) {
        vecRowData.add(index, rowData);
        fireTableRowsInserted(index, index);
    }

    /**
     * Move uma linha acima
     * @param index linha a ser movida
     */
    public void moveRowUp(int index) {
        if (index == 0 || index == vecRowData.size()) {
            return;
        }

        Object atual = vecRowData.remove(index);
        vecRowData.insertElementAt(atual, index - 1);
    }

    /**
     * Move uma linha abaixo
     * @param index linha a ser movida
     */
    public void moveRowDown(int index) {
        if (index == vecRowData.size() - 1) {
            return;
        }

        Object atual = vecRowData.remove(index);
        vecRowData.insertElementAt(atual, index + 1);

    }

    /**
     * Setar a tabela em modo de inserção de dados
     * @param inserir <b> true </b> se estiver em inserção, <b> false </b> caso
     * contrário
     */
    public void setInsert(boolean inserir) {
        this.inserir = inserir;
    }

    ////protected methods
    /**
     * Retorna um vector que contém o mesmo conteúdo do array.
     * @param anArray  Um array a ser convertido
     * @return  Um novo Vector; se <code>anArray</code> é <code>null</code>,
     *				returna <code>null</code>
     */
    protected static Vector convertToVector(Object[] anArray) {
        if (anArray == null) {
            return null;
        }
        Vector v = new Vector(anArray.length);
        for (int i = 0; i < anArray.length; i++) {
            v.addElement(anArray[i]);
        }
        return v;
    }

    /**
     * Retorna Um vector de vector com o mesmo conteúdo do array.
     * @param anArray  Um array duplo a ser convertido
     * @return Um novo Vector de Vector; se <code>anArray</code> for
     *				<code>null</code>, returna <code>null</code>
     */
    protected static Vector convertToVector(Object[][] anArray) {
        if (anArray == null) {
            return null;
        }
        Vector v = new Vector(anArray.length);
        for (int i = 0; i < anArray.length; i++) {
            v.addElement(convertToVector(anArray[i]));
        }
        return v;
    }

    /**
     * Converte um Vector de Vector em um Array duplo com o mesmo conteúdo
     * @param vector Vector de Vector a ser convertido
     * @return Object[][] Array duplo convertido ou null
     */
    protected Object[][] convertToArray(Vector vector) {
        if (vector.isEmpty()) {
            return new Object[0][0];
        }

        Object[][] ob = new Object[vector.size()][getColumnCount()];

        for (int i = 0; i < vector.size(); i++) {
            String tmp = String.valueOf(vector.get(i));
            tmp = tmp.substring(1, tmp.length() - 1);
            String[] vc = tmp.split(",");

            System.arraycopy(vc, 0, ob[i], 0, getColumnCount());
        }

        return ob;
    }

    /**
     * Converte um Vector em um Array
     * @param vector Vector a ser convertido
     * @return Object[] com o mesmo conteúdo do Vector ou null
     */
    protected Object[] convertToArrayUni(Vector vector) {
        if (vector.isEmpty()) {
            return new Object[0];
        }

        Object[] ob = new Object[vector.size()];

        for (int i = 0; i < vector.size(); i++) {
            String tmp = String.valueOf(vector.get(i));
            tmp = tmp.substring(1, tmp.length() - 1);
            String[] vc = tmp.split(",");
            System.arraycopy(vc, 0, ob, 0, getColumnCount());
        }
        return ob;
    }
}
