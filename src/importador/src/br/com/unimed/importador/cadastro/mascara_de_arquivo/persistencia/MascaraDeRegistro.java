/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "mascara_de_registro")
@NamedQueries({
    @NamedQuery(name = "MascaraDeRegistro.findAll", query = "SELECT m FROM MascaraDeRegistro m"),
    @NamedQuery(name = "MascaraDeRegistro.findByCodMascaraDeRegistro", query = "SELECT m FROM MascaraDeRegistro m WHERE m.mascaraDeRegistroPK.codMascaraDeRegistro = :codMascaraDeRegistro"),
    @NamedQuery(name = "MascaraDeRegistro.findByNumVersaoMascaraDeRegistro", query = "SELECT m FROM MascaraDeRegistro m WHERE m.mascaraDeRegistroPK.numVersaoMascaraDeRegistro = :numVersaoMascaraDeRegistro"),
    @NamedQuery(name = "MascaraDeRegistro.findByDscMascaraDeRegistro", query = "SELECT m FROM MascaraDeRegistro m WHERE m.dscMascaraDeRegistro = :dscMascaraDeRegistro"),
    @NamedQuery(name = "MascaraDeRegistro.findByVlrTamanho", query = "SELECT m FROM MascaraDeRegistro m WHERE m.vlrTamanho = :vlrTamanho")})
public class MascaraDeRegistro implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MascaraDeRegistroPK mascaraDeRegistroPK;
    @Basic(optional = false)
    @Column(name = "dsc_mascara_de_registro")
    private String dscMascaraDeRegistro;
    @Basic(optional = false)
    @Column(name = "vlr_tamanho")
    private short vlrTamanho;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mascaraDeRegistro")
    private Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mascaraDeRegistro")
    private Collection<MascaraDeCampo> mascaraDeCampoCollection;

    public MascaraDeRegistro() {
    }

    public MascaraDeRegistro(MascaraDeRegistroPK mascaraDeRegistroPK) {
        this.mascaraDeRegistroPK = mascaraDeRegistroPK;
    }

    public MascaraDeRegistro(MascaraDeRegistroPK mascaraDeRegistroPK, String dscMascaraDeRegistro, short vlrTamanho) {
        this.mascaraDeRegistroPK = mascaraDeRegistroPK;
        this.dscMascaraDeRegistro = dscMascaraDeRegistro;
        this.vlrTamanho = vlrTamanho;
    }

    public MascaraDeRegistro(String codMascaraDeRegistro, short numVersaoMascaraDeRegistro) {
        this.mascaraDeRegistroPK = new MascaraDeRegistroPK(codMascaraDeRegistro, numVersaoMascaraDeRegistro);
    }

    public MascaraDeRegistroPK getMascaraDeRegistroPK() {
        return mascaraDeRegistroPK;
    }

    public void setMascaraDeRegistroPK(MascaraDeRegistroPK mascaraDeRegistroPK) {
        this.mascaraDeRegistroPK = mascaraDeRegistroPK;
    }

    public String getDscMascaraDeRegistro() {
        return dscMascaraDeRegistro;
    }

    public void setDscMascaraDeRegistro(String dscMascaraDeRegistro) {
        this.dscMascaraDeRegistro = dscMascaraDeRegistro;
    }

    public short getVlrTamanho() {
        return vlrTamanho;
    }

    public void setVlrTamanho(short vlrTamanho) {
        this.vlrTamanho = vlrTamanho;
    }

    public Collection<MascaraDeArquivoMascaraDeRegistro> getMascaraDeArquivoMascaraDeRegistroCollection() {
        return mascaraDeArquivoMascaraDeRegistroCollection;
    }

    public void setMascaraDeArquivoMascaraDeRegistroCollection(Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollection) {
        this.mascaraDeArquivoMascaraDeRegistroCollection = mascaraDeArquivoMascaraDeRegistroCollection;
    }

    public Collection<MascaraDeCampo> getMascaraDeCampoCollection() {
        return mascaraDeCampoCollection;
    }

    public void setMascaraDeCampoCollection(Collection<MascaraDeCampo> mascaraDeCampoCollection) {
        this.mascaraDeCampoCollection = mascaraDeCampoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mascaraDeRegistroPK != null ? mascaraDeRegistroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MascaraDeRegistro)) {
            return false;
        }
        MascaraDeRegistro other = (MascaraDeRegistro) object;
        if ((this.mascaraDeRegistroPK == null && other.mascaraDeRegistroPK != null) || (this.mascaraDeRegistroPK != null && !this.mascaraDeRegistroPK.equals(other.mascaraDeRegistroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistro[mascaraDeRegistroPK=" + mascaraDeRegistroPK + "]";
    }

}
