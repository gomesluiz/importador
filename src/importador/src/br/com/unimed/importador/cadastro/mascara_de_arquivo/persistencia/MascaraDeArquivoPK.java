/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author paulo
 */
@Embeddable
public class MascaraDeArquivoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "cod_mascara_de_arquivo")
    private String codMascaraDeArquivo;
    @Basic(optional = false)
    @Column(name = "num_versao_mascara_de_arquivo")
    private short numVersaoMascaraDeArquivo;

    public MascaraDeArquivoPK() {
    }

    public MascaraDeArquivoPK(String codMascaraDeArquivo, short numVersaoMascaraDeArquivo) {
        this.codMascaraDeArquivo = codMascaraDeArquivo;
        this.numVersaoMascaraDeArquivo = numVersaoMascaraDeArquivo;
    }

    public String getCodMascaraDeArquivo() {
        return codMascaraDeArquivo;
    }

    public void setCodMascaraDeArquivo(String codMascaraDeArquivo) {
        this.codMascaraDeArquivo = codMascaraDeArquivo;
    }

    public short getNumVersaoMascaraDeArquivo() {
        return numVersaoMascaraDeArquivo;
    }

    public void setNumVersaoMascaraDeArquivo(short numVersaoMascaraDeArquivo) {
        this.numVersaoMascaraDeArquivo = numVersaoMascaraDeArquivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codMascaraDeArquivo != null ? codMascaraDeArquivo.hashCode() : 0);
        hash += (int) numVersaoMascaraDeArquivo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MascaraDeArquivoPK)) {
            return false;
        }
        MascaraDeArquivoPK other = (MascaraDeArquivoPK) object;
        if ((this.codMascaraDeArquivo == null && other.codMascaraDeArquivo != null) || (this.codMascaraDeArquivo != null && !this.codMascaraDeArquivo.equals(other.codMascaraDeArquivo))) {
            return false;
        }
        if (this.numVersaoMascaraDeArquivo != other.numVersaoMascaraDeArquivo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivoPK[codMascaraDeArquivo=" + codMascaraDeArquivo + ", numVersaoMascaraDeArquivo=" + numVersaoMascaraDeArquivo + "]";
    }

}
