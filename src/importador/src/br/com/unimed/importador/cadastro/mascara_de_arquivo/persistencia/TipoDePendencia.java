/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "tipo_de_pendencia")
@NamedQueries({
    @NamedQuery(name = "TipoDePendencia.findAll", query = "SELECT t FROM TipoDePendencia t"),
    @NamedQuery(name = "TipoDePendencia.findByCodTipoDePendencia", query = "SELECT t FROM TipoDePendencia t WHERE t.codTipoDePendencia = :codTipoDePendencia"),
    @NamedQuery(name = "TipoDePendencia.findByDscTipoDePendencia", query = "SELECT t FROM TipoDePendencia t WHERE t.dscTipoDePendencia = :dscTipoDePendencia")})
public class TipoDePendencia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_tipo_de_pendencia")
    private Short codTipoDePendencia;
    @Basic(optional = false)
    @Column(name = "dsc_tipo_de_pendencia")
    private String dscTipoDePendencia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDePendencia")
    private Collection<PendenciaDeAlteracao> pendenciaDeAlteracaoCollection;

    public TipoDePendencia() {
    }

    public TipoDePendencia(Short codTipoDePendencia) {
        this.codTipoDePendencia = codTipoDePendencia;
    }

    public TipoDePendencia(Short codTipoDePendencia, String dscTipoDePendencia) {
        this.codTipoDePendencia = codTipoDePendencia;
        this.dscTipoDePendencia = dscTipoDePendencia;
    }

    public Short getCodTipoDePendencia() {
        return codTipoDePendencia;
    }

    public void setCodTipoDePendencia(Short codTipoDePendencia) {
        this.codTipoDePendencia = codTipoDePendencia;
    }

    public String getDscTipoDePendencia() {
        return dscTipoDePendencia;
    }

    public void setDscTipoDePendencia(String dscTipoDePendencia) {
        this.dscTipoDePendencia = dscTipoDePendencia;
    }

    public Collection<PendenciaDeAlteracao> getPendenciaDeAlteracaoCollection() {
        return pendenciaDeAlteracaoCollection;
    }

    public void setPendenciaDeAlteracaoCollection(Collection<PendenciaDeAlteracao> pendenciaDeAlteracaoCollection) {
        this.pendenciaDeAlteracaoCollection = pendenciaDeAlteracaoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoDePendencia != null ? codTipoDePendencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDePendencia)) {
            return false;
        }
        TipoDePendencia other = (TipoDePendencia) object;
        if ((this.codTipoDePendencia == null && other.codTipoDePendencia != null) || (this.codTipoDePendencia != null && !this.codTipoDePendencia.equals(other.codTipoDePendencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDePendencia[codTipoDePendencia=" + codTipoDePendencia + "]";
    }

}
