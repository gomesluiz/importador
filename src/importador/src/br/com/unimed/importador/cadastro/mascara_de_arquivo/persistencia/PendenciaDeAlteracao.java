/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "pendencia_de_alteracao")
@NamedQueries({
    @NamedQuery(name = "PendenciaDeAlteracao.findAll", query = "SELECT p FROM PendenciaDeAlteracao p"),
    @NamedQuery(name = "PendenciaDeAlteracao.findByNumSequencialPendencia", query = "SELECT p FROM PendenciaDeAlteracao p WHERE p.numSequencialPendencia = :numSequencialPendencia"),
    @NamedQuery(name = "PendenciaDeAlteracao.findByCodMascaraDeCampo", query = "SELECT p FROM PendenciaDeAlteracao p WHERE p.codMascaraDeCampo = :codMascaraDeCampo"),
    @NamedQuery(name = "PendenciaDeAlteracao.findByCodMascaraDeRegistro", query = "SELECT p FROM PendenciaDeAlteracao p WHERE p.codMascaraDeRegistro = :codMascaraDeRegistro"),
    @NamedQuery(name = "PendenciaDeAlteracao.findByNumVersaoMascaraDeRegistro", query = "SELECT p FROM PendenciaDeAlteracao p WHERE p.numVersaoMascaraDeRegistro = :numVersaoMascaraDeRegistro"),
    @NamedQuery(name = "PendenciaDeAlteracao.findByDatPendenciaAlteracao", query = "SELECT p FROM PendenciaDeAlteracao p WHERE p.datPendenciaAlteracao = :datPendenciaAlteracao"),
    @NamedQuery(name = "PendenciaDeAlteracao.findByIndPendenciaResolvida", query = "SELECT p FROM PendenciaDeAlteracao p WHERE p.indPendenciaResolvida = :indPendenciaResolvida")})
public class PendenciaDeAlteracao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "num_sequencial_pendencia")
    private Integer numSequencialPendencia;
    @Basic(optional = false)
    @Column(name = "cod_mascara_de_campo")
    private String codMascaraDeCampo;
    @Basic(optional = false)
    @Column(name = "cod_mascara_de_registro")
    private String codMascaraDeRegistro;
    @Basic(optional = false)
    @Column(name = "num_versao_mascara_de_registro")
    private short numVersaoMascaraDeRegistro;
    @Basic(optional = false)
    @Column(name = "dat_pendencia_alteracao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datPendenciaAlteracao;
    @Basic(optional = false)
    @Column(name = "ind_pendencia_resolvida")
    private boolean indPendenciaResolvida;
    @JoinColumn(name = "cod_tipo_de_pendencia", referencedColumnName = "cod_tipo_de_pendencia")
    @ManyToOne(optional = false)
    private TipoDePendencia tipoDePendencia;

    public PendenciaDeAlteracao() {
    }

    public PendenciaDeAlteracao(Integer numSequencialPendencia) {
        this.numSequencialPendencia = numSequencialPendencia;
    }

    public PendenciaDeAlteracao(Integer numSequencialPendencia, String codMascaraDeCampo, String codMascaraDeRegistro, short numVersaoMascaraDeRegistro, Date datPendenciaAlteracao, boolean indPendenciaResolvida) {
        this.numSequencialPendencia = numSequencialPendencia;
        this.codMascaraDeCampo = codMascaraDeCampo;
        this.codMascaraDeRegistro = codMascaraDeRegistro;
        this.numVersaoMascaraDeRegistro = numVersaoMascaraDeRegistro;
        this.datPendenciaAlteracao = datPendenciaAlteracao;
        this.indPendenciaResolvida = indPendenciaResolvida;
    }

    public Integer getNumSequencialPendencia() {
        return numSequencialPendencia;
    }

    public void setNumSequencialPendencia(Integer numSequencialPendencia) {
        this.numSequencialPendencia = numSequencialPendencia;
    }

    public String getCodMascaraDeCampo() {
        return codMascaraDeCampo;
    }

    public void setCodMascaraDeCampo(String codMascaraDeCampo) {
        this.codMascaraDeCampo = codMascaraDeCampo;
    }

    public String getCodMascaraDeRegistro() {
        return codMascaraDeRegistro;
    }

    public void setCodMascaraDeRegistro(String codMascaraDeRegistro) {
        this.codMascaraDeRegistro = codMascaraDeRegistro;
    }

    public short getNumVersaoMascaraDeRegistro() {
        return numVersaoMascaraDeRegistro;
    }

    public void setNumVersaoMascaraDeRegistro(short numVersaoMascaraDeRegistro) {
        this.numVersaoMascaraDeRegistro = numVersaoMascaraDeRegistro;
    }

    public Date getDatPendenciaAlteracao() {
        return datPendenciaAlteracao;
    }

    public void setDatPendenciaAlteracao(Date datPendenciaAlteracao) {
        this.datPendenciaAlteracao = datPendenciaAlteracao;
    }

    public boolean getIndPendenciaResolvida() {
        return indPendenciaResolvida;
    }

    public void setIndPendenciaResolvida(boolean indPendenciaResolvida) {
        this.indPendenciaResolvida = indPendenciaResolvida;
    }

    public TipoDePendencia getTipoDePendencia() {
        return tipoDePendencia;
    }

    public void setTipoDePendencia(TipoDePendencia tipoDePendencia) {
        this.tipoDePendencia = tipoDePendencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numSequencialPendencia != null ? numSequencialPendencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PendenciaDeAlteracao)) {
            return false;
        }
        PendenciaDeAlteracao other = (PendenciaDeAlteracao) object;
        if ((this.numSequencialPendencia == null && other.numSequencialPendencia != null) || (this.numSequencialPendencia != null && !this.numSequencialPendencia.equals(other.numSequencialPendencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.PendenciaDeAlteracao[numSequencialPendencia=" + numSequencialPendencia + "]";
    }

}
