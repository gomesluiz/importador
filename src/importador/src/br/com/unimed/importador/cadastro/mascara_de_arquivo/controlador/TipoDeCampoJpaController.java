/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.IllegalOrphanException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeCampo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampo;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author paulo
 */
public class TipoDeCampoJpaController {

    public TipoDeCampoJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoDeCampo tipoDeCampo) throws PreexistingEntityException, Exception {
        if (tipoDeCampo.getMascaraDeCampoCollection() == null) {
            tipoDeCampo.setMascaraDeCampoCollection(new ArrayList<MascaraDeCampo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollection = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampoToAttach : tipoDeCampo.getMascaraDeCampoCollection()) {
                mascaraDeCampoCollectionMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollection.add(mascaraDeCampoCollectionMascaraDeCampoToAttach);
            }
            tipoDeCampo.setMascaraDeCampoCollection(attachedMascaraDeCampoCollection);
            em.persist(tipoDeCampo);
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampo : tipoDeCampo.getMascaraDeCampoCollection()) {
                TipoDeCampo oldTipoDeCampoOfMascaraDeCampoCollectionMascaraDeCampo = mascaraDeCampoCollectionMascaraDeCampo.getTipoDeCampo();
                mascaraDeCampoCollectionMascaraDeCampo.setTipoDeCampo(tipoDeCampo);
                mascaraDeCampoCollectionMascaraDeCampo = em.merge(mascaraDeCampoCollectionMascaraDeCampo);
                if (oldTipoDeCampoOfMascaraDeCampoCollectionMascaraDeCampo != null) {
                    oldTipoDeCampoOfMascaraDeCampoCollectionMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionMascaraDeCampo);
                    oldTipoDeCampoOfMascaraDeCampoCollectionMascaraDeCampo = em.merge(oldTipoDeCampoOfMascaraDeCampoCollectionMascaraDeCampo);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipoDeCampo(tipoDeCampo.getCodTipoDeCampo()) != null) {
                throw new PreexistingEntityException("TipoDeCampo " + tipoDeCampo + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoDeCampo tipoDeCampo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDeCampo persistentTipoDeCampo = em.find(TipoDeCampo.class, tipoDeCampo.getCodTipoDeCampo());
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOld = persistentTipoDeCampo.getMascaraDeCampoCollection();
            Collection<MascaraDeCampo> mascaraDeCampoCollectionNew = tipoDeCampo.getMascaraDeCampoCollection();
            List<String> illegalOrphanMessages = null;
            for (MascaraDeCampo mascaraDeCampoCollectionOldMascaraDeCampo : mascaraDeCampoCollectionOld) {
                if (!mascaraDeCampoCollectionNew.contains(mascaraDeCampoCollectionOldMascaraDeCampo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MascaraDeCampo " + mascaraDeCampoCollectionOldMascaraDeCampo + " since its tipoDeCampo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollectionNew = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampoToAttach : mascaraDeCampoCollectionNew) {
                mascaraDeCampoCollectionNewMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollectionNew.add(mascaraDeCampoCollectionNewMascaraDeCampoToAttach);
            }
            mascaraDeCampoCollectionNew = attachedMascaraDeCampoCollectionNew;
            tipoDeCampo.setMascaraDeCampoCollection(mascaraDeCampoCollectionNew);
            tipoDeCampo = em.merge(tipoDeCampo);
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampo : mascaraDeCampoCollectionNew) {
                if (!mascaraDeCampoCollectionOld.contains(mascaraDeCampoCollectionNewMascaraDeCampo)) {
                    TipoDeCampo oldTipoDeCampoOfMascaraDeCampoCollectionNewMascaraDeCampo = mascaraDeCampoCollectionNewMascaraDeCampo.getTipoDeCampo();
                    mascaraDeCampoCollectionNewMascaraDeCampo.setTipoDeCampo(tipoDeCampo);
                    mascaraDeCampoCollectionNewMascaraDeCampo = em.merge(mascaraDeCampoCollectionNewMascaraDeCampo);
                    if (oldTipoDeCampoOfMascaraDeCampoCollectionNewMascaraDeCampo != null && !oldTipoDeCampoOfMascaraDeCampoCollectionNewMascaraDeCampo.equals(tipoDeCampo)) {
                        oldTipoDeCampoOfMascaraDeCampoCollectionNewMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionNewMascaraDeCampo);
                        oldTipoDeCampoOfMascaraDeCampoCollectionNewMascaraDeCampo = em.merge(oldTipoDeCampoOfMascaraDeCampoCollectionNewMascaraDeCampo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = tipoDeCampo.getCodTipoDeCampo();
                if (findTipoDeCampo(id) == null) {
                    throw new NonexistentEntityException("The tipoDeCampo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDeCampo tipoDeCampo;
            try {
                tipoDeCampo = em.getReference(TipoDeCampo.class, id);
                tipoDeCampo.getCodTipoDeCampo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoDeCampo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOrphanCheck = tipoDeCampo.getMascaraDeCampoCollection();
            for (MascaraDeCampo mascaraDeCampoCollectionOrphanCheckMascaraDeCampo : mascaraDeCampoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoDeCampo (" + tipoDeCampo + ") cannot be destroyed since the MascaraDeCampo " + mascaraDeCampoCollectionOrphanCheckMascaraDeCampo + " in its mascaraDeCampoCollection field has a non-nullable tipoDeCampo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoDeCampo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoDeCampo> findTipoDeCampoEntities() {
        return findTipoDeCampoEntities(true, -1, -1);
    }

    public List<TipoDeCampo> findTipoDeCampoEntities(int maxResults, int firstResult) {
        return findTipoDeCampoEntities(false, maxResults, firstResult);
    }

    private List<TipoDeCampo> findTipoDeCampoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoDeCampo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoDeCampo findTipoDeCampo(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoDeCampo.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoDeCampoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoDeCampo> rt = cq.from(TipoDeCampo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
