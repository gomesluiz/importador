/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.visao;

/**
 * Interface para notificar alterações na tabela
 * @author Equipe Affirmare
 */
public interface MyTableListenerEvent {
    public void notifyUpdateTable(int row,int col);
    public void notifyInsertTable(int row,int col);
    public void notifyDeleteTable(int row,int col);
}
