/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.PendenciaDeAlteracao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDePendencia;

/**
 *
 * @author paulo
 */
public class PendenciaDeAlteracaoJpaController {

    public PendenciaDeAlteracaoJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PendenciaDeAlteracao pendenciaDeAlteracao) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDePendencia tipoDePendencia = pendenciaDeAlteracao.getTipoDePendencia();
            if (tipoDePendencia != null) {
                tipoDePendencia = em.getReference(tipoDePendencia.getClass(), tipoDePendencia.getCodTipoDePendencia());
                pendenciaDeAlteracao.setTipoDePendencia(tipoDePendencia);
            }
            em.persist(pendenciaDeAlteracao);
            if (tipoDePendencia != null) {
                tipoDePendencia.getPendenciaDeAlteracaoCollection().add(pendenciaDeAlteracao);
                tipoDePendencia = em.merge(tipoDePendencia);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PendenciaDeAlteracao pendenciaDeAlteracao) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PendenciaDeAlteracao persistentPendenciaDeAlteracao = em.find(PendenciaDeAlteracao.class, pendenciaDeAlteracao.getNumSequencialPendencia());
            TipoDePendencia tipoDePendenciaOld = persistentPendenciaDeAlteracao.getTipoDePendencia();
            TipoDePendencia tipoDePendenciaNew = pendenciaDeAlteracao.getTipoDePendencia();
            if (tipoDePendenciaNew != null) {
                tipoDePendenciaNew = em.getReference(tipoDePendenciaNew.getClass(), tipoDePendenciaNew.getCodTipoDePendencia());
                pendenciaDeAlteracao.setTipoDePendencia(tipoDePendenciaNew);
            }
            pendenciaDeAlteracao = em.merge(pendenciaDeAlteracao);
            if (tipoDePendenciaOld != null && !tipoDePendenciaOld.equals(tipoDePendenciaNew)) {
                tipoDePendenciaOld.getPendenciaDeAlteracaoCollection().remove(pendenciaDeAlteracao);
                tipoDePendenciaOld = em.merge(tipoDePendenciaOld);
            }
            if (tipoDePendenciaNew != null && !tipoDePendenciaNew.equals(tipoDePendenciaOld)) {
                tipoDePendenciaNew.getPendenciaDeAlteracaoCollection().add(pendenciaDeAlteracao);
                tipoDePendenciaNew = em.merge(tipoDePendenciaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pendenciaDeAlteracao.getNumSequencialPendencia();
                if (findPendenciaDeAlteracao(id) == null) {
                    throw new NonexistentEntityException("The pendenciaDeAlteracao with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PendenciaDeAlteracao pendenciaDeAlteracao;
            try {
                pendenciaDeAlteracao = em.getReference(PendenciaDeAlteracao.class, id);
                pendenciaDeAlteracao.getNumSequencialPendencia();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pendenciaDeAlteracao with id " + id + " no longer exists.", enfe);
            }
            TipoDePendencia tipoDePendencia = pendenciaDeAlteracao.getTipoDePendencia();
            if (tipoDePendencia != null) {
                tipoDePendencia.getPendenciaDeAlteracaoCollection().remove(pendenciaDeAlteracao);
                tipoDePendencia = em.merge(tipoDePendencia);
            }
            em.remove(pendenciaDeAlteracao);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PendenciaDeAlteracao> findPendenciaDeAlteracaoEntities() {
        return findPendenciaDeAlteracaoEntities(true, -1, -1);
    }

    public List<PendenciaDeAlteracao> findPendenciaDeAlteracaoEntities(int maxResults, int firstResult) {
        return findPendenciaDeAlteracaoEntities(false, maxResults, firstResult);
    }

    private List<PendenciaDeAlteracao> findPendenciaDeAlteracaoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PendenciaDeAlteracao.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PendenciaDeAlteracao findPendenciaDeAlteracao(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PendenciaDeAlteracao.class, id);
        } finally {
            em.close();
        }
    }

    public int getPendenciaDeAlteracaoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PendenciaDeAlteracao> rt = cq.from(PendenciaDeAlteracao.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
