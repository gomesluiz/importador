/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.utils.Proper;
import br.com.unimed.importador.utils.LoggerFile;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Equipe Affirmare
 */
public class GenericDAO implements InterfaceJdbcDataAccess {

    private static Connection conn_rdatabase = null;
    private static ResultSet rs_rdatabase;
    private static Statement stmt;
    private static int iCount = 0;
    private static int batchValue = 1;
    private static int errorCount = 0;
    private static InterfaceJdbcDataAccess _instance;

    /**
     * Singleton para obter uma instância da classe
     * @return instância da classe GenericDAO
     */
    public static synchronized InterfaceJdbcDataAccess getInstance() {
        if (_instance == null) {
            _instance = new GenericDAO();
            return _instance;
        }

        return _instance;
    }

    public boolean  abrirConexao() {
           try {
            try {
                Proper.loadFile("base.ini");
            } catch (IOException ex) {
                Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            conn_rdatabase = MySqlDataAccessFactory.createConnection(Proper.getValue("usuario"), Proper.getValue("senha"), Proper.getValue("servidor"), Proper.getValue("banco"));
            stmt = conn_rdatabase.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private GenericDAO() {

    }

    /**
     * Responsável por gerenciar a inclusão de registros nas tabelas
     * @param sql consulta sql com os comandas para incluir registros
     * @return <b> 1 </b> se tudo ocorreu com sucesso.
     */
    public int inserir(String sql) {
       iCount++;

       try {
          stmt.addBatch(sql);

          if (iCount % batchValue == 0) {
             stmt.executeBatch();
          }

       } catch (SQLException ex) {
          LoggerFile.getLogger().log(Level.SEVERE, String.format("%s: %s",ex.getMessage(), sql));
          errorCount++;
          return 0;
       }
       return 1;
    }

    /**
     * Responsável por gerenciar a criação das tabelas
     * @param sql consulta sql que cria a tabela no banco de dados
     * @return <b> true </b> se tudo ocorreu com sucesso.
     */
    public boolean execute(String sql) {
        try {
               stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            LoggerFile.getLogger().log(Level.SEVERE, String.format("%s: %s",ex.getMessage(), sql));
            errorCount++;
            return false;
        }
        return true;
    }



    public boolean remover(String sql) {
        try {
               stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            LoggerFile.getLogger().log(Level.SEVERE, String.format("%s: %s",ex.getMessage(), sql));
            errorCount++;
            return false;
        }
        return true;
    }

   public boolean existe(String sql) {
      Statement comando;
      boolean resultado = false;
      if (sql == null) {
         throw new IllegalArgumentException("a consulta nao pode ser nula.");
      }
      try {
         
         rs_rdatabase = stmt.executeQuery(sql);
         while(rs_rdatabase.next()){ resultado = true; break; }
      } catch (SQLException sqlException) {
         throw new QueryException(String.format("erro ao recuperar executar a consula %s.", sql), sqlException);
      }
      return resultado;
   }

    public InterfaceJdbcDataAccess listarPorChave(String id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Map<String, InterfaceJdbcDataAccess> listarTodos() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean atualizar(String sql) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Verifica se existe a tabela na base de dados
     * @param tabela nome da tabela
     * @return <b> true </b> se existe a tabela, <b> false </b> caso contrário.
     */
    public boolean isTableExist(String tabela) {
      try {
         DatabaseMetaData dbm = conn_rdatabase.getMetaData();
         rs_rdatabase = dbm.getTables(null, null, tabela, null);
         if (rs_rdatabase.next()) {
            return true;
         } else {
            return false;
         }
      } catch (SQLException ex) {
         LoggerFile.getLogger().log(Level.SEVERE, String.format("Error na criação da tabela: %s", tabela));
         return false;
      }

   }

    /**
     * Fecha a conexão e libera os recursos
     * @return <b> true </b> se ocorreu com sucesso
     */
    public boolean fecharConexao() {
        try {
            if (rs_rdatabase != null) {
                rs_rdatabase.close();
                rs_rdatabase = null;
            }
            if (conn_rdatabase != null) {
                conn_rdatabase.close();
                conn_rdatabase = null;
            }
            if (stmt != null) {
               stmt.executeBatch();
               stmt = null;
           }

        } catch (SQLException ex) {
            ex.getSQLState();
            ex.getErrorCode();
        }
        return true;
    }

    /**
     * Conta o número de erros nas inserções e creates sql
     * @return <b> int </b> com o número de erros ocorridos no processamento
     */
    public int getErrorCount() {
        return errorCount;
    }

    public void incErrorCount() {
        errorCount++;
    }

    public void setErrorCount(int errorcount) {
        errorCount = errorcount;
    }

    @Override
    public String toString() {
        return "GenericDAO";
    }

   public List<InterfaceJdbcDataAccess> listarPor(String condicao) {
      throw new UnsupportedOperationException("Not supported yet.");
   }




}
