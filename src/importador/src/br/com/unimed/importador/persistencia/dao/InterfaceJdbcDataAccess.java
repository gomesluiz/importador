package br.com.unimed.importador.persistencia.dao;

import java.util.Map;

/**
 * Interface que provê métodos padrões nas operações em base de dados
 * @author Equipe Affirmare
 */
public interface InterfaceJdbcDataAccess {

    public int inserir(String sql);

    public boolean remover(String sql);

    public InterfaceJdbcDataAccess listarPorChave(String chavePrimaria);

    public Map<String, InterfaceJdbcDataAccess> listarTodos();

    public boolean atualizar(String sql);

    public boolean fecharConexao();

    public boolean abrirConexao();

    public boolean execute(String sql);

    public boolean existe(String sql);
}
