/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.dominio.MascaraDeRegistro;
import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.utils.Proper;
import java.io.IOException;
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MascaraDeRegistroDAO {

   private static Connection conexao = null;
   private static Statement comando;
   private static ResultSet tuplas;
   private static MascaraDeRegistroDAO instancia;
   static final String CONSULTA_PADRAO =
           "SELECT m.cod_mascara_de_registro"
           + " ,m.num_versao_mascara_de_registro"
           + " ,m.dsc_mascara_de_registro"
           + " ,m.vlr_tamanho"
           + " FROM mascara_de_registro m";

   public static synchronized MascaraDeRegistroDAO getInstance() {
      if (instancia == null) {
         instancia = new MascaraDeRegistroDAO();
      }
      return instancia;
   }

   public MascaraDeRegistro listaPorChavePrimaria(final String codigoDaMascaraDeRegistro,
            final int numeroDeVersaoDaMascaraDeRegistro){

      StringBuilder consulta = new StringBuilder(CONSULTA_PADRAO)
              .append(' ')
              .append("WHERE")
              .append(' ')
              .append(" cod_mascara_de_registro = ")
              .append("'")
              .append(codigoDaMascaraDeRegistro)
              .append("'")
              .append(" AND")
              .append(" num_versao_mascara_de_registro =")
              .append(numeroDeVersaoDaMascaraDeRegistro);

      List<MascaraDeRegistro> resultado = lista(consulta.toString());
      if (resultado.isEmpty()) {
         throw new QueryException("mascara de registro " + codigoDaMascaraDeRegistro + " versão "
                 + numeroDeVersaoDaMascaraDeRegistro + " nao encontrada.");
      }
      return resultado.get(0);
   }

   private List<MascaraDeRegistro> lista(String consulta) {

      if (consulta == null) {
         throw new IllegalArgumentException("a consulta nao pode ser nula.");
      }
      try {
         tuplas = comando.executeQuery(consulta);
         if (!tuplas.isAfterLast()) {
            List<MascaraDeRegistro> resultado = new ArrayList<MascaraDeRegistro>();
            while (tuplas.next()) {
               resultado.add(new MascaraDeRegistro(tuplas.getString(1), tuplas.getInt(2), tuplas.getString(3)));
            }
            return resultado;
         }
      } catch (SQLException sqlException) {
         throw new QueryException("erro ao recuperar as máscaras de registros.", sqlException);
      }
      return Collections.emptyList();
   }

   public boolean fecharConexao() {
      try {
         if (tuplas != null) {
            tuplas.close();
         }
         if (conexao != null) {
            conexao.close();
         }
         comando = null;
      } catch (SQLException ex) {
         ex.getSQLState();
         ex.getErrorCode();
      }
      return true;
   }

   public boolean abrirConexao() {
      try {
         Proper.loadFile("base.ini");
      } catch (IOException ex) {
         Logger.getLogger(MascaraDeRegistroDAO.class.getName()).log(Level.SEVERE, null, ex);
      }

      conexao = MySqlDataAccessFactory.createConnection(
              Proper.getValue("usuario"), Proper.getValue("senha"),
              Proper.getValue("servidor"), Proper.getValue("banco"));
      try {
         comando = conexao.createStatement();
      } catch (SQLException ex) {
         Logger.getLogger(MascaraDeRegistroDAO.class.getName()).log(Level.SEVERE, null, ex);
         return false;
      }

      return true;
   }  
}
