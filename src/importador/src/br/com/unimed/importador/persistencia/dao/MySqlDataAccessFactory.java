/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe responsável em estabelecer a conexão na base de dados
 * mysql
 * @author Equipe Affirmare
 */
public class MySqlDataAccessFactory extends DataAccessFactory {

    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static String BDURL = "jdbc:mysql://"; //localhost/SEUBANCO";
    private static String url = null;
    private static int count=0;
    private static Connection connection = null;

    /* DAO */
    public static final int MascaraArquivo = 01;
    public static final int MascaraCampo = 02;
    public static final int MascaraRegistro = 03;
    public static final int MascaraArquivoMascaraRegistro = 04;
    public static final int Generic = 05;
    public static final int TipoCampo = 06;
    public static final int PendenciaDeAlteracao = 07;

     /**
     * Retorna uma conexão válida, se for possível estabelcer a mesma
     * @param user - Usuário do banco
     * @param password - Password do banco
     * @param ip - ip do servidor ou localhost
     * @param banco - Nome do banco de dados para conexão
     * @return null se ocorrer erro ou uma conexão válida.
     */
    public static Connection createConnection(String user, String password,
            String ip, String banco) {

       
       count++;

        if (ip == null) {
            ip = "localhost";
        }

        BDURL = "jdbc:mysql://";

        BDURL += ip + "/" + banco;

        try {
            Class.forName(DRIVER);
        } catch (Exception ex) {
            System.out.println("ERRO AO CARREGAR DRIVER!!!" + ex.getStackTrace());
        }
        //Obter conexão do DriverManager.
        try {
          
            connection = DriverManager.getConnection(BDURL, user, password);
 
        } catch (Exception ex) {
            System.out.println("SQL Exception " + ex.getMessage());
            System.out.println("SQL State " + ((SQLException) ex).getSQLState());
            System.out.println("SQL Error " + ((SQLException) ex).getErrorCode());
 
        }

        return connection;
    }

    public MySqlDataAccessFactory() {
    }

    /**
     * Método fábrica responsável em instanciar classe para manipular e gerenciar
     * acesso as tabelas no banco de dados
     * @param witch parametro que informa qual classe instanciar
     * @return referência a classe instância informada no parametro witch
     */
    @Override
    public InterfaceJdbcDataAccess getObjectDAO(int witch) {
        switch (witch) {
            case Generic:
                return  GenericDAO.getInstance();
 
         }

        return null;
    }

   /**
    * Recupera um contador com o número de conexões abertas simultaneamente
    * @return <b> int </b> com o número de conexões abertas
    */
    public static int getContadorConexao() {
        return count;
    }

    
}
