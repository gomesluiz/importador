/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.persistencia.io;

/**
 * Classe responsável em prover métodos de escrita em disco
 * @author Equipe Affirmare
 */
public class WriteIOFactory extends DataIOFactory{

    /**
     * Recupera uma classe concreta para escrita em arquivos
     * @return InterfaceIODataAccess
     */
    @Override
    public InterfaceIODataAccess getIO() {
        return new OperationIO();
    }

}
