package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.dominio.MascaraDeArquivoMascaraDeRegistro;
import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.utils.Proper;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MascaraDeArquivoMascaraDeRegistroDAO {

   private static Connection conexao = null;
   private static Statement comando;
   private static ResultSet tuplas;
   private static MascaraDeArquivoMascaraDeRegistroDAO instancia;

   static final String CONSULTA_PADRAO =
           "SELECT m.cod_mascara_de_arquivo"
           + " , m.num_versao_mascara_de_arquivo"
           + " , m.cod_mascara_de_registro"
           + " , m.num_versao_mascara_de_registro"
           + " , m.num_sequencial_registro"
           + " FROM mascara_de_arquivo_mascara_de_registro m";

   public static synchronized MascaraDeArquivoMascaraDeRegistroDAO getInstance() {
      if (instancia == null) {
         instancia = new MascaraDeArquivoMascaraDeRegistroDAO();
      }
      return instancia;
   }

   public List<MascaraDeArquivoMascaraDeRegistro> listaPorArquivoEVersao(final String codigoDaMascaraDeArquivo,
            final int numeroDeVersaoDaMascaraDeArquivo){

      StringBuilder consulta = new StringBuilder(CONSULTA_PADRAO)
              .append(' ')
              .append("WHERE")
              .append(' ')
              .append(" cod_mascara_de_arquivo = ")
              .append("'")
              .append(codigoDaMascaraDeArquivo)
              .append("'")
              .append(" AND")
              .append(" num_versao_mascara_de_arquivo =")
              .append(numeroDeVersaoDaMascaraDeArquivo)
              .append(' ')
              .append("ORDER BY num_sequencial_registro");

      return lista(consulta.toString());
   }

   private List<MascaraDeArquivoMascaraDeRegistro> lista(String consulta) {

      if (consulta == null) {
         throw new IllegalArgumentException("a consulta nao pode ser nula.");
      }
      try {

         tuplas = comando.executeQuery(consulta);

         if (!tuplas.isAfterLast()) {
            List<MascaraDeArquivoMascaraDeRegistro> resultado = new ArrayList<MascaraDeArquivoMascaraDeRegistro>();
            while (tuplas.next()) {
               resultado.add(new MascaraDeArquivoMascaraDeRegistro(tuplas.getString(1), tuplas.getInt(2)
                       , tuplas.getString(3), tuplas.getInt(4), tuplas.getInt(5)));
            }
            return resultado;
         }
      } catch (SQLException sqlException) {
         throw new QueryException("erro ao recuperar as máscaras de arquivos e registros.", sqlException);
      }
      return Collections.emptyList();
   }

   public boolean fecharConexao() {
      try {
         if (tuplas != null) {
            tuplas.close();
         }
         if (conexao != null) {
            conexao.close();
         }
         comando = null;
      } catch (SQLException ex) {
         ex.getSQLState();
         ex.getErrorCode();
      }
      return true;
   }

   public boolean abrirConexao() {
      try {
         Proper.loadFile("base.ini");
      } catch (IOException ex) {
         Logger.getLogger(MascaraDeRegistroDAO.class.getName()).log(Level.SEVERE, null, ex);
      }

      conexao = MySqlDataAccessFactory.createConnection(
              Proper.getValue("usuario"), Proper.getValue("senha"),
              Proper.getValue("servidor"), Proper.getValue("banco"));
      try {
         comando = conexao.createStatement();
      } catch (SQLException ex) {
         Logger.getLogger(MascaraDeRegistroDAO.class.getName()).log(Level.SEVERE, null, ex);
         return false;
      }

      return true;
   }
}
