/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.io;

/**
 * Classe responsável em prover métodos de leitura
 * mysql
 * @author Equipe Affirmare
 */
public class ReadIOFactory extends DataIOFactory {
    

    /**
     * Construtor padrão
     */
    public ReadIOFactory() {
    }

    
    /**
     * Recupera uma classe concreta para leitura em arquivos
     * @return classe concreta para leitura ou escrita
     */
    @Override
    public InterfaceIODataAccess getIO() {
        return new OperationIO();
    }
}
