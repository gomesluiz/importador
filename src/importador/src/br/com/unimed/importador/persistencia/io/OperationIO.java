/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ProgressMonitorInputStream;

/**
 * Classe responsável pelas operações de read e write
 * da base de dados construida pelo método factory.
 * @author Equipe Affirmare
 */
public class OperationIO implements InterfaceIODataAccess {

    //List of variables
    private File _ioFile = null;
    private BufferedInputStream _is = null;
    private BufferedOutputStream _os = null;
    private BufferedReader _in = null;
    private int _statusOperation = 0;
    private int _posFile = 0;
    private int _numRead = 0;
    private boolean _end = false;

    /**
     * Cria os handles para manipulação dos arquivos
     * @param type READ_FILE para leitura e WRITE_FILE para escrita
     * @return true se obteve um Handle válido para manipular arquivos
     */
    public boolean connect(int type) {

        switch (type) {
            case READ_FILE:
                try {
                    _is = new BufferedInputStream(new ProgressMonitorInputStream(
                            null,
                            "Importando dados...",
                            new FileInputStream(getFile())));

                    _statusOperation = READ_FILE;
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }

                break;

            case WRITE_FILE:
                try {
                    _os = new BufferedOutputStream(new FileOutputStream(getFile()));
                    _statusOperation = WRITE_FILE;
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
                break;

            case READ_ASC:
                try {
                    _in = new BufferedReader(new FileReader(getFile()));
                    _statusOperation = READ_ASC;

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
                break;

        }

        // Get the size of the file
        long length = getFile().length();
        if (length > Integer.MAX_VALUE) {
            // File _is too large
            throw new IndexOutOfBoundsException("File is too large");
        }

        return true;

    }

     /**
     * Cria os handles para manipulação dos arquivos
     * @param type READ_FILE para leitura e WRITE_FILE para escrita
     * @param append <b> true </b> se deseja anexar dados mantendo os dados antigos
     * @return true se obteve um Handle válido para manipular arquivos
     */
    public boolean connect(int type, boolean append) {

        switch (type) {
            case READ_FILE:
                try {
                    _is = new BufferedInputStream(new FileInputStream(getFile()));
                    _statusOperation = READ_FILE;
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }

                break;

            case WRITE_FILE:
                try {
                    _os = new BufferedOutputStream(new FileOutputStream(getFile(), append));
                    _statusOperation = WRITE_FILE;
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
                break;

            case READ_ASC:
                try {
                    _in = new BufferedReader(new FileReader(getFile()));
                    _statusOperation = READ_ASC;

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
                break;
        }

        // Get the size of the file
        long length = getFile().length();
        if (length > Integer.MAX_VALUE) {
            // File _is too large
            throw new IndexOutOfBoundsException("File is too large");
        }

        return true;

    }

    /**
     * Fechar uma conexão física com o arquivo
     * @param type tipo de conexão aberta. WRITE_FILE, READ_FILE, READ_ASC.
     * @return <b> true </b> se o fechamento ocorrer com sucesso
     */
    public boolean closeConnection(int type) {
        if (_is != null || _os != null || _in != null) {
            try {
                if (type == WRITE_FILE) {
                    _os.close();
                } else if (type == READ_FILE) {
                    _is.close();
                } else if (type == READ_ASC) {
                    _in.close();
                } else {
                    return false;
                }

            } catch (IOException ex) {
                Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        }

        return false;
    }

    /**
     * Fecha todas as conexões abertas
     * @return <b> true </b> se todas as conexões forem encerradas com sucesso.
     */
    public boolean closeConnection() {

        if (_statusOperation == READ_FILE) {
            closeConnection(READ_FILE);
        } else if (_statusOperation == WRITE_FILE) {
            closeConnection(WRITE_FILE);
        } else if (_statusOperation == READ_ASC) {
            closeConnection(READ_ASC);
        }


        return true;
    }

    /**
     * Realiza a leitura para um buffer de bytes do tamanho especificado por len
     * @param len número de bytes a ser lido
     * @return array de bytes lidos do arquivo
     * @throws UnsupportedOperationException
     */
    public byte[] readBuffer(int len) throws
            UnsupportedOperationException, IndexOutOfBoundsException {

        try {

            // Read in the bytes
            int offset = 0;
            long fileTam = getFile().length();
            _end = false;

            //check end of file
            if ((_posFile + len) > fileTam) {
                if (fileTam - _posFile < 0) {
                    throw new IndexOutOfBoundsException();
                }
                len = (int) (fileTam - _posFile);
                _end = true;
            }

            byte[] bytes = new byte[len];


            if (_statusOperation == READ_FILE) {
                _is.mark(len);
                _is.reset();

                _numRead = _is.read(bytes, offset, len);

                if (_numRead > -1) {
                    _posFile = _posFile + _numRead;
                } else {
                    _end = true;
                }
                _is.mark(len);
                return bytes;
            } else {
                throw new UnsupportedOperationException("Tentativa de escrever.Definido Leitura");
            }
        } catch (IOException ex) {
            Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * Realiza a leitura para um buffer de bytes do tamanho especificado por len
     * a partir da posição especificada em start
     * @param start inicio para leitura
     * @param len tamanho do buffer
     * @return array de bytes lidos do arquivo
     */
    public byte[] readBuffer(int start, int len) throws
            UnsupportedOperationException, IndexOutOfBoundsException {
        int iconta = 1;
        long fileTam = getFile().length();
        _end = false;

        //check end of file
        if ((start + len) > fileTam) {
            if (fileTam - start < 0) {
                throw new IndexOutOfBoundsException();
            }
            len = (int) (fileTam - start) + 1;
            _end = true;
        }

        byte[] bytes = new byte[len];

        //close file if opened and opened again
        closeConnection(READ_FILE);
        connect(READ_FILE);

        if (_statusOperation != READ_FILE) {
            throw new UnsupportedOperationException("Tentativa de escrever.Definido Leitura");
        }

        while (iconta < start) {
            try {
                _is.read();
                iconta++;
            } catch (IOException ex) {
                Logger.getLogger(OperationIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        bytes = readBuffer(len);

        return bytes;

    }

    /**
     * Responsável pela operação de escrita em disco
     * @param strContent conteúdo que será gravado
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeBuffer(byte[] strContent) throws FileNotFoundException, IOException {
        _os.write(strContent);
        _os.flush();

    }

    /**
     * Especifica o arquivo fisico para operação de leitura/escrita
     * @param _ioFile diretório e nome do arquivo
     * @throws FileNotFoundException
     */
    public void setFile(File ioFile) throws FileNotFoundException {
        this._ioFile = ioFile;
    }

    /**
     * Recupera o objeto que representa o arquivo físico
     * @return File ou null se nenhum arquivo foi definido
     */
    public File getFile() {
        return _ioFile;
    }

    /**
     * Detecta o fim do arquivo
     * @return <b> true </b> indicando fim do arquivo
     */
    public boolean EOF() {
        return _end;
    }

    /**
     * Realiza a leitura de uma linha do arquivo
     * @return <b> String </b> que representa a linha lida do arquivo fisico.
     * @throws IOException
     */
    public String readLine() throws IOException {
        String tmp = _in.readLine();
        return tmp;
    }
}
