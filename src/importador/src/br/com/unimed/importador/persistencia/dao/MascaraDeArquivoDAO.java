package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.dominio.MascaraDeArquivo;
import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.utils.Proper;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MascaraDeArquivoDAO {

   private static Connection conexao = null;
   private static Statement comando;
   private static ResultSet tuplas;
   private static MascaraDeArquivoDAO instancia;
   static final String CONSULTA_PADRAO =
           "SELECT m.cod_mascara_de_arquivo"
           + " , m.num_versao_mascara_de_arquivo"
           + " , m.dsc_mascara_de_arquivo"
           + " FROM mascara_de_arquivo m";

   public static synchronized MascaraDeArquivoDAO getInstance() {
      if (instancia == null) {
         instancia = new MascaraDeArquivoDAO();
      }
      return instancia;
   }

   public boolean fecharConexao() {
      try {
         if (tuplas != null) {
            tuplas.close();
         }
         if (conexao != null) {
            conexao.close();
         }
      } catch (SQLException sqlException) {
         sqlException.getSQLState();
         sqlException.getErrorCode();
         return false;
      }
      return true;
   }

   public boolean abrirConexao() {
      try {
         Proper.loadFile("base.ini");
      } catch (IOException ex) {
         Logger.getLogger(MascaraDeArquivoDAO.class.getName()).log(Level.SEVERE, null, ex);
      }

      conexao = MySqlDataAccessFactory.createConnection(
              Proper.getValue("usuario"), Proper.getValue("senha"),
              Proper.getValue("servidor"), Proper.getValue("banco"));
      try {
         comando = conexao.createStatement();
      } catch (SQLException ex) {
         Logger.getLogger(MascaraDeArquivoDAO.class.getName()).log(Level.SEVERE, null, ex);
         return false;
      }

      return true;
   }

   public MascaraDeArquivo listaPorChavePrimaria(final String codigoDaMascaraDeArquivo,
            final int numeroDeVersaoDaMascaraDeArquivo){

      StringBuilder consulta = new StringBuilder(CONSULTA_PADRAO)
              .append(' ')
              .append("WHERE")
              .append(' ')
              .append(" cod_mascara_de_arquivo = ")
              .append("'")
              .append(codigoDaMascaraDeArquivo)
              .append("'")
              .append(" AND")
              .append(" num_versao_mascara_de_arquivo =")
              .append(numeroDeVersaoDaMascaraDeArquivo);

      List<MascaraDeArquivo> resultado = lista(consulta.toString());
      if (resultado.isEmpty()) {
         throw new QueryException("mascara de arquivo " + codigoDaMascaraDeArquivo + " versão "
                 + numeroDeVersaoDaMascaraDeArquivo + " nao encontrada.");
      }
      return resultado.get(0);
   }
   private List<MascaraDeArquivo> lista(String consulta) {

      if (consulta == null) {
         throw new IllegalArgumentException("a condicao nao pode ser nula.");
      }

      try {
         tuplas = comando.executeQuery(consulta);

         if (!tuplas.isAfterLast()) {
            List<MascaraDeArquivo> resultado = new ArrayList<MascaraDeArquivo>();
            while (tuplas.next()) {
               resultado.add(new MascaraDeArquivo(tuplas.getString(1), tuplas.getInt(2)
                       , tuplas.getString(3)));
            }
            return resultado;
         }
      } catch (SQLException sqlException) {
         throw new QueryException("erro ao recuperar as máscaras de arquivos.", sqlException);
      }
      return Collections.emptyList();
   }
}
