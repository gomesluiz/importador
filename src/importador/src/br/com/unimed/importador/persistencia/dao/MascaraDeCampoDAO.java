package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.dominio.MascaraDeCampo;
import br.com.unimed.importador.dominio.TipoDeCampo;
import br.com.unimed.importador.dominio.TipoDeOrigem;
import br.com.unimed.importador.dominio.TipoDeUso;
import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.utils.Proper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MascaraDeCampoDAO {

   private static Connection conexao = null;
   private static Statement comando;
   private static ResultSet tuplas;
   private static MascaraDeCampoDAO instancia;
   static final String CONSULTA_PADRAO =
           "SELECT m.cod_mascara_de_campo"
           + ", m.cod_mascara_de_registro"
           + ", m.num_versao_mascara_de_registro"
           + ", m.vlr_posicao_inicial"
           + ", m.vlr_posicao_final"
           + ", m.vlr_tamanho"
           + ", m.vlr_decimais"
           + ", m.cod_tipo_de_origem"
           + ", m.cod_tipo_de_campo"
           + ", m.cod_tipo_de_uso"
           + ", m.ind_contem_tipo_de_registro"
           + ", m.ind_chave_primaria"
           + ", m.ind_contem_numero_de_versao"
           + ", m.num_sequencial_campo"
           + " FROM mascara_de_campo m";

   public static synchronized MascaraDeCampoDAO getInstance() {
      if (instancia == null) {
         instancia = new MascaraDeCampoDAO();
      }
      return instancia;
   }

   public List<MascaraDeCampo> listaPorRegistroEVersao(final String codigoDaMascaraDeRegistro,
            final int numeroDeVersaoDaMascaraDeRegistro){

      StringBuilder consulta = new StringBuilder(CONSULTA_PADRAO)
              .append(' ')
              .append("WHERE")
              .append(' ')
              .append(" cod_mascara_de_registro = ")
              .append("'")
              .append(codigoDaMascaraDeRegistro)
              .append("'")
              .append(" AND")
              .append(" num_versao_mascara_de_registro =")
              .append(numeroDeVersaoDaMascaraDeRegistro)
              .append(' ')
              .append("ORDER BY num_sequencial_campo");
                
      return lista(consulta.toString());
   }


   private List<MascaraDeCampo> lista(String consulta) {
      if (consulta == null) {
         throw new IllegalArgumentException("a consulta nao pode ser nula.");
      }
      
      try {
         tuplas = comando.executeQuery(consulta);
         if (!tuplas.isAfterLast()){
            List<MascaraDeCampo> resultado = new ArrayList<MascaraDeCampo>();
            while (tuplas.next()) {
               resultado.add(new MascaraDeCampo(tuplas.getString(1), tuplas.getInt(4)
                       , tuplas.getInt(5), tuplas.getInt(6), tuplas.getInt(7)
                       , TipoDeOrigem.getTipoDeOrigem(tuplas.getInt(8))
                       , TipoDeCampo.getTipoDeCampo(tuplas.getInt(9))
                       , TipoDeUso.getTipoDeUso(tuplas.getInt(10))
                       , tuplas.getBoolean(11), tuplas.getBoolean(12), tuplas.getBoolean(13)));
            }
            return resultado;
         }
      } catch (SQLException sqlException) {
         throw new QueryException("erro ao recuperar as máscaras de campos.", sqlException);
      }
      return Collections.emptyList();
   }

   public boolean fecharConexao() {
      try {
         if (tuplas != null) {
            tuplas.close();
         }
         if (conexao != null) {
            conexao.close();
         }
         comando = null;
      } catch (SQLException ex) {
         ex.getSQLState();
         ex.getErrorCode();
      }
      return true;
   }

   public boolean abrirConexao() {
      try {
         Proper.loadFile("base.ini");
      } catch (IOException ex) {
         Logger.getLogger(MascaraDeCampoDAO.class.getName()).log(Level.SEVERE, null, ex);
      }

      conexao = MySqlDataAccessFactory.createConnection(
              Proper.getValue("usuario"), Proper.getValue("senha"),
              Proper.getValue("servidor"), Proper.getValue("banco"));
      try {
         comando = conexao.createStatement();
      } catch (SQLException ex) {
         Logger.getLogger(MascaraDeCampoDAO.class.getName()).log(Level.SEVERE, null, ex);
         return false;
      }

      return true;
   }

}
