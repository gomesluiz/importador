/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.io;

/**
 * Classe Abstrata para módulo de persistência.
 * Fornece as opções de bancos para conexão via jdbc
 * @author Equpe Affirmare
 */
public abstract class DataIOFactory {

    // List of types supported by the factory
    public static final int READBYTE    = 1;
    public static final int WRITEBYTE   = 2;
  
    public abstract InterfaceIODataAccess getIO();

    /**
     * Retorna a conexão com o banco de acordo com o tipo especificado
     * @param whichFactory tipo de banco
     * @return DataAccessFactory ou Null
     */
    public static DataIOFactory getIOFactory(int whichFactory) {

        switch (whichFactory) {
            case READBYTE:
                return new ReadIOFactory();
            
            case WRITEBYTE:
                return new WriteIOFactory();
            
                
            default:
                return null;
        }
    }
}
