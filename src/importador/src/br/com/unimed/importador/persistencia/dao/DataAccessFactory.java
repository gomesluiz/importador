/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.dao;

/**
 * Classe Abstrata para módulo de persistência.
 * Fornece as opções de bancos para conexão via jdbc
 * @author affirmare
 */
public abstract class DataAccessFactory {

    // List of DAO types supported by the factory
    public static final int MYSQL = 1;


    public abstract InterfaceJdbcDataAccess getObjectDAO(int which);

    /**
     * Retorna a conexão com o banco de acordo com o tipo especificado
     * @param whichFactory tipo de banco
     * @return DataAccessFactory ou Null
     */
    public static DataAccessFactory getDAOFactory(int whichFactory) {

        switch (whichFactory) {
            case MYSQL:
                return new MySqlDataAccessFactory();
                
            default:
                return null;
        }
    }
}
