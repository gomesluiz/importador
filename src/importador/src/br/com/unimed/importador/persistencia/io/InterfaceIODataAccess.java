/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.persistencia.io;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Interface Para gerenciar operações e I/O
 * @author Equipe Affirmare
 */
public interface InterfaceIODataAccess extends InterfaceConfigureFile {
  public byte[] readBuffer(int  len) throws UnsupportedOperationException, IndexOutOfBoundsException;
  public byte[] readBuffer(int start, int len) throws UnsupportedOperationException, IndexOutOfBoundsException;
  public String readLine() throws IOException;
  public void writeBuffer(byte[] i) throws FileNotFoundException, IOException;
  public boolean EOF();
}
