package br.com.unimed.importador.exceptions;

public class RegistroDesconhecidoException extends RuntimeException {

    public RegistroDesconhecidoException() {
        super("Registro desconhecido");
    }

    public RegistroDesconhecidoException(final String msg) {
        super(msg); 
    }

    public RegistroDesconhecidoException(final String msg, final Throwable causa) {
        super(msg, causa);
    }
}
