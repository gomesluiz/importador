package br.com.unimed.importador.exceptions;
public class QueryException extends RuntimeException{
    public QueryException(final String msg) {
        super(msg);
    }

    public QueryException(final String msg, final Throwable causa) {
        super(msg, causa);
    }
}
