package br.com.unimed.importador.exceptions;

public class RegistroComTamanhoInvalidoException extends RuntimeException {

    public RegistroComTamanhoInvalidoException() {
       super("Registro com o tamanho inválido");
    }
    public RegistroComTamanhoInvalidoException(String msg) {
        super(msg);
    }
    public RegistroComTamanhoInvalidoException(final String msg, final Throwable causa) {
        super(msg, causa);
    }
    public RegistroComTamanhoInvalidoException(final Throwable causa) {
        super(causa);
    }
}
