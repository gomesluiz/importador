package br.com.unimed.importador.dominio;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MascaraDeCampoTest {

    MascaraDeCampo campo;
    PendenciaDeAlteracao pendencia1, pendencia2, pendencia3;

    public MascaraDeCampoTest() {
    }

    @Before
    public void configurarUmTeste() throws Exception {
        pendencia1 = new PendenciaDeAlteracao(1, 1, false);
        pendencia2 = new PendenciaDeAlteracao(2, 1, false);
        pendencia3 = new PendenciaDeAlteracao(3, 1, false);

        campo = new MascaraDeCampo ("Fonte",1,2, 1, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
        campo.adicionarPendencia(pendencia1);
        campo.adicionarPendencia(pendencia2);
        campo.adicionarPendencia(pendencia3);

    }

    @After
    public void finalizarUmTeste() throws Exception {
        campo = null;
        pendencia1 = pendencia2 = pendencia3;
    }

    @Test
    public void testGetDecimais() {
        System.out.println("getDecimais");
        MascaraDeCampo instance = campo;
        int expResult = 0;
        int result = instance.getDecimais();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetNome() {
        System.out.println("getNome");
        MascaraDeCampo instance = campo;
        String expResult = "FONTE";
        String result = instance.getCodigo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPosicaoFinal() {
        System.out.println("getPosicaoFinal");
        MascaraDeCampo instance = campo;
        int expResult = 2;
        int result = instance.getPosicaoFinal();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPosicaoInicial() {
        System.out.println("getPosicaoInicial");
        MascaraDeCampo instance = campo;
        int expResult = 1;
        int result = instance.getPosicaoInicial();
        assertEquals(expResult, result);
        
    }


    @Test
    public void testGetTamanho() {
        System.out.println("getTamanho");
        MascaraDeCampo instance = campo;
        int expResult = 1;
        int result = instance.getTamanho();
        assertEquals(expResult, result);
    }


    @Test
    public void testGetTipoDeCampo() {
        System.out.println("getTipoDeCampo");
        MascaraDeCampo instance = campo;
        TipoDeCampo expResult = TipoDeCampo.A;
        TipoDeCampo result = instance.getTipoDeCampo();
        assertEquals(expResult, result);
    }


    @Test
    public void testGetTipoDeOrigem() {
        System.out.println("getTipoDeOrigem");
        MascaraDeCampo instance = campo;
        TipoDeOrigem expResult = TipoDeOrigem.PARAMETRO;
        TipoDeOrigem result = instance.getTipoDeOrigem();
        assertEquals(expResult, result);
    }


    @Test
    public void testGetTipoDeUso() {
        System.out.println("getTipoDeUso");
        MascaraDeCampo instance = campo;
        TipoDeUso expResult = TipoDeUso.MANDATORIO;
        TipoDeUso result = instance.getTipoDeUso();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsContemTipoRegistro() {
        System.out.println("isContemTipoRegistro");
        MascaraDeCampo instance = campo;
        boolean expResult = false;
        boolean result = instance.contemTipoRegistro();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetSetValor() {
        System.out.println("setValor e getValor");
        MascaraDeCampo instance = campo;
        String expResult = "20100101";
        instance.setValor(expResult);
        String result = instance.getValor();
        assertEquals(expResult, result);  
    }

    @Test
    public void testMudarEstadoDasPendenciasPara() {
        System.out.println("testMudarEstadoDasPendenciasPara");
        boolean resultadoEsperado;

        campo.mudarEstadoDasPendenciasPara(true);
        resultadoEsperado   = pendencia1.isIndicadorDePendenciaResolvida();
        resultadoEsperado   = resultadoEsperado && pendencia2.isIndicadorDePendenciaResolvida();
        resultadoEsperado   = resultadoEsperado && pendencia3.isIndicadorDePendenciaResolvida();

        assertTrue("testMudarEstadoDasPendenciasPara : ", resultadoEsperado);
    }
    
}