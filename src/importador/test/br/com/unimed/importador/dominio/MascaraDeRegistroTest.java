/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.dominio;

import java.util.List;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

public class MascaraDeRegistroTest {

    MascaraDeCampo campo1;
    MascaraDeCampo campo2;
    MascaraDeCampo campo3;
    MascaraDeCampo campo4;

    List<MascaraDeCampo> parametros = new ArrayList<MascaraDeCampo>();
    MascaraDeRegistro mascaraDeRegistro;

    PendenciaDeAlteracao pendencia1, pendencia2, pendencia3;

    @Before
    public void configurarUmTeste() throws Exception {
        campo1 = new MascaraDeCampo("nr_seq", 1, 8, 8, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
       campo2 = new MascaraDeCampo("tp_reg", 9, 11, 3, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, true, false, false);
       campo3 = new MascaraDeCampo("cd_uni_ori", 12, 15, 3, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.AN, TipoDeUso.MANDATORIO, false, false, false);
       campo4 = new MascaraDeCampo("nr_comp_inicial", 16, 21, 4, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);

       pendencia1 = new PendenciaDeAlteracao(1, 1, false);
       pendencia2 = new PendenciaDeAlteracao(2, 2, false);
       pendencia3 = new PendenciaDeAlteracao(3, 3, false);

       campo1.adicionarPendencia(pendencia1);
       campo1.adicionarPendencia(pendencia2);
       campo1.adicionarPendencia(pendencia3);
       
       mascaraDeRegistro = new MascaraDeRegistro("501", 0, "501");
       mascaraDeRegistro.adicionaCampo(campo1);
       mascaraDeRegistro.adicionaCampo(campo2);
       mascaraDeRegistro.adicionaCampo(campo3);
       mascaraDeRegistro.adicionaCampo(campo4);
    }

    @After
    public void limparUmTeste() throws Exception {
        campo1 = null;
       campo2 = null;
       campo3 = null;
       campo4 = null;
       mascaraDeRegistro = null;
       pendencia1 = null;
       pendencia2 = null;
       pendencia3 = null;
    }


    @Test
    public void testAdicionarCampo() {
        System.out.println("adicionarCampo");
        List<MascaraDeCampo> mascaras = new ArrayList<MascaraDeCampo>();
      
        mascaras.add(campo1);
        mascaras.add(campo2);
        mascaras.add(campo3);
        mascaras.add(campo4);

        int i = 0;
        for (MascaraDeCampo c: mascaraDeRegistro.getCampos()){
            assertEquals(c.getCodigo(), mascaras.get(i).getCodigo().toUpperCase());
            assertEquals(c.getPosicaoInicial(), mascaras.get(i).getPosicaoInicial());
            assertEquals(c.getPosicaoFinal(), mascaras.get(i).getPosicaoFinal());
            assertEquals(c.getTipoDeCampo(), mascaras.get(i).getTipoDeCampo());
            assertEquals(c.getTamanho(), mascaras.get(i).getTamanho());
            assertEquals(c.getDecimais(), mascaras.get(i).getDecimais());
            assertEquals(c.getTipoDeOrigem(), mascaras.get(i).getTipoDeOrigem());
            assertEquals(c.getTipoDeUso(), mascaras.get(i).getTipoDeUso());
            assertEquals(c.getValor(), mascaras.get(i).getValor());
            assertEquals(c.contemTipoRegistro(), mascaras.get(i).contemTipoRegistro());
            assertEquals(c.contemChavePrimaria(), mascaras.get(i).contemChavePrimaria());
            i += 1;
        }
    }

    @Test
    public void testObterRegistroComValoresAtribuitosAosCampos() {
        System.out.println("obterRegistroComValoresAtribuitosAosCampos");
        String linhaDoArquivo = "0030035250939564005432915901535303740303182477000000000247837643";
        MascaraDeRegistro mr = new MascaraDeRegistro("509", 0, "Registro De Teste");

        MascaraDeCampo c01 = new MascaraDeCampo("nr_seq",1, 8, 8, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c02 = new MascaraDeCampo("tp_reg",9, 11, 3, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, true, true, false);
        MascaraDeCampo c03 = new MascaraDeCampo("qt_tot_r502",12, 16, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c04 = new MascaraDeCampo("qt_tot_r503",17, 21, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c05 = new MascaraDeCampo("qt_tot_r504",22, 26, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c06 = new MascaraDeCampo("qt_tot_r505",27, 31, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c07 = new MascaraDeCampo("qt_not_exc",32, 36, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c08 = new MascaraDeCampo("qt_tot_ser",42, 52, 11, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c09 = new MascaraDeCampo("vl_tot_serv",53, 64, 12, 2, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);

        mr.adicionaCampo(c01);
        mr.adicionaCampo(c02);
        mr.adicionaCampo(c03);
        mr.adicionaCampo(c04);
        mr.adicionaCampo(c05);
        mr.adicionaCampo(c06);
        mr.adicionaCampo(c07);
        mr.adicionaCampo(c08);
        mr.adicionaCampo(c09);

        MascaraDeRegistro expResult = new MascaraDeRegistro("509", 0, "Registro De Teste");
        MascaraDeCampo c1 = new MascaraDeCampo("nr_seq",1, 8, 8, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c2 = new MascaraDeCampo("tp_reg",9, 11, 3, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, true, true, false);
        MascaraDeCampo c3 = new MascaraDeCampo("qt_tot_r502",12, 16, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c4 = new MascaraDeCampo("qt_tot_r503",17, 21, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c5 = new MascaraDeCampo("qt_tot_r504",22, 26, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c6 = new MascaraDeCampo("qt_tot_r505",27, 31, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c7 = new MascaraDeCampo("qt_not_exc",32, 36, 5, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c8 = new MascaraDeCampo("qt_tot_ser",42, 52, 11, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
        MascaraDeCampo c9 = new MascaraDeCampo("vl_tot_serv",53, 64, 12, 2, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);

        expResult.adicionaCampo(c1);
        expResult.adicionaCampo(c2);
        expResult.adicionaCampo(c3);
        expResult.adicionaCampo(c4);
        expResult.adicionaCampo(c5);
        expResult.adicionaCampo(c6);
        expResult.adicionaCampo(c7);
        expResult.adicionaCampo(c8);
        expResult.adicionaCampo(c9);

        c1.setValor("00300352");
        c2.setValor("509");
        c3.setValor("39564");
        c4.setValor("00543");
        c5.setValor("29159");
        c6.setValor("01535");
        c7.setValor("30374");
        c8.setValor("82477000000");
        c9.setValor("000247837643");

        MascaraDeRegistro result = mr.obtemRegistroComValores(linhaDoArquivo);
        int i = 0;
        for (MascaraDeCampo mc : expResult.getCampos()){            
            assertEquals(mc.getValor(), result.getCampos().get(i).getValor());
            i += 1;
        }

        String esperado = "INSERT INTO R509 (NR_SEQ,TP_REG,QT_TOT_R502,QT_TOT_R503,QT_TOT_R504,QT_TOT_R505,QT_NOT_EXC,QT_TOT_SER,VL_TOT_SERV)VALUES(00300352,509,39564,00543,29159,01535,30374,82477000000,2478376.43)";
        assertEquals("testaFormatoDeInsercaoBuilder", esperado, result.formataParaInsercao());
    }

    @Test
    public void testGetDescricao() {
        System.out.println("getDescricao");
        
        String expResult = "501";
        String result = mascaraDeRegistro.getDescricao();

        assertEquals(expResult, result);
    }
  
    @Test
    public void testGetTipoDoRegistro() {
        System.out.println("getTipoDoRegistro");
        
        String expResult = "501";
        String result = mascaraDeRegistro.getCodigo();

        assertEquals(expResult, result);
        
    }

    @Test
    public void testGetPosicaoInicialDoTipoDoRegistro() {
        System.out.println("getPosicaoInicialDoTipoDoRegistro");
        int expResult = 9;
        int result = mascaraDeRegistro.getPosicaoInicialDoTipoDoRegistro();
        assertEquals("getPosicaoInicialDoTipoDoRegistro : ", expResult, result);

    }

    @Test
    public void testGetPosicaoFinalDoTipoDoRegistro() {
        System.out.println("getPosicaoFinalDoTipoDoRegistro");
        int expResult = 11;
        int result = mascaraDeRegistro.getPosicaoFinalDoTipoDoRegistro();
        assertEquals("getPosicaoFinalDoTipoDoRegistro :", expResult, result);
        
    }

    @Test
    public void testGetTamanho() {
        System.out.println("getTamanho");

        int expResult = 21;
        int result = mascaraDeRegistro.getTamanho();

        assertEquals("getTamanho :", expResult, result);
    }

    @Test
    public void testaFormatoDeTabelaBuilder() {
      String esperado = "CREATE TABLE R501 (NR_SEQ DECIMAL(8),TP_REG DECIMAL(3),CD_UNI_ORI VARCHAR(3),NR_COMP_INICIAL DECIMAL(4))ENGINE='MYISAM';";
      assertEquals("testaFormatoDeTabelaBuilder", esperado, mascaraDeRegistro.formataParaTabela());
   }

    @Test
   public void testaFormatoDeAlteracaoBuilderSemPendencias() {
      MascaraDeCampo campoS1 = new MascaraDeCampo("nr_seq", 1, 8, 8, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);
      MascaraDeCampo campoS2 = new MascaraDeCampo("tp_reg", 9, 11, 3, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, true, false, false);
      MascaraDeCampo campoS3 = new MascaraDeCampo("cd_uni_ori", 12, 15, 3, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.AN, TipoDeUso.MANDATORIO, false, false, false);
      MascaraDeCampo campoS4 = new MascaraDeCampo("nr_comp_inicial", 16, 21, 4, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.N, TipoDeUso.MANDATORIO, false, false, false);

      MascaraDeRegistro mascaraDeRegistroS = new MascaraDeRegistro("501", 0, "501");
      mascaraDeRegistroS.adicionaCampo(campoS1);
      mascaraDeRegistroS.adicionaCampo(campoS2);
      mascaraDeRegistroS.adicionaCampo(campoS3);
      mascaraDeRegistroS.adicionaCampo(campoS4);

      assertTrue("testaFormatoDeAlteracaoBuilderSemPendencias", mascaraDeRegistroS.formataParaAlteracao().isEmpty());
   }

    @Test
    public void testaFormatoDeAlteracaoBuilderComPendencias() {
       String esperado = "ALTER TABLE R501 ADD COLUMN NR_SEQ DECIMAL(8),MODIFY NR_SEQ DECIMAL(8);";

       System.out.println(mascaraDeRegistro.formataParaAlteracao());
       assertEquals("testaFormatoDeAlteracaoBuilder", esperado, mascaraDeRegistro.formataParaAlteracao());
   }
}