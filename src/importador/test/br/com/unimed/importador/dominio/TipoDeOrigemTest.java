/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.dominio;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luiz
 */
public class TipoDeOrigemTest {

     /**
     * Test of values method, of class TipoDeOrigem.
     */
    @Test
    public void testValues() {
        System.out.println("values");
        TipoDeOrigem[] expResult = {TipoDeOrigem.PARAMETRO, TipoDeOrigem.REGISTRO};
        TipoDeOrigem[] result = TipoDeOrigem.values();
        assertEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class TipoDeOrigem.
     */
    @Test
    public void testValueOf() {
        System.out.println("valueOf");
        String name = "PARAMETRO";
        TipoDeOrigem expResult = TipoDeOrigem.PARAMETRO;
        TipoDeOrigem result = TipoDeOrigem.valueOf(name);
        assertEquals(expResult, result);

        name = "REGISTRO";
        expResult = TipoDeOrigem.REGISTRO;
        result = TipoDeOrigem.valueOf(name);
        assertEquals(expResult, result);
        
    }

}