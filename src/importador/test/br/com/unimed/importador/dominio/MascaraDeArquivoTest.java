package br.com.unimed.importador.dominio;

import java.util.List;
import java.util.ArrayList;
import br.com.unimed.importador.exceptions.RegistroComTamanhoMaiorException;
import br.com.unimed.importador.exceptions.RegistroComTamanhoMenorException;
import br.com.unimed.importador.exceptions.RegistroDesconhecidoException;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;

public class MascaraDeArquivoTest {

   MascaraDeArquivo mascaraDeArquivo500;
   MascaraDeRegistro mascaraDeRegistro501, mascaraDeRegistro502;
   MascaraDeCampo campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8,
           campo9, campo10, campo11, campo12, campo13, campo14, campo15, campo16,
           campo17, campo18, campo19, campo20;

   @Before
   public void configurarUmTeste() throws Exception {
      mascaraDeArquivo500 = new MascaraDeArquivo("A500", 15, "A500");
      mascaraDeRegistro501 = new MascaraDeRegistro("501", 15, "501");
      mascaraDeRegistro502 = new MascaraDeRegistro("502", 15, "502");
      campo1 = new MascaraDeCampo("Fonte", 0, 0, 1, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo2 = new MascaraDeCampo("Referencia", 0, 0, 6, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);

      campo3 = new MascaraDeCampo("NR_SEQ", 1, 8, 8, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo4 = new MascaraDeCampo("TP_REG", 9, 11, 3, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, true, false, false);

      campo5 = new MascaraDeCampo("CD_UNI_DES", 9, 12, 4, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo6 = new MascaraDeCampo("CD_UNI_ORI", 13, 14, 2, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo7 = new MascaraDeCampo("DT_GERACAO", 15, 16, 1, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);

      campo8 = new MascaraDeCampo("NR_LOTE", 12, 19, 8, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo9 = new MascaraDeCampo("NR_NOTA", 20, 30, 11, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo10 = new MascaraDeCampo("CD_UNI", 32, 35, 4, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);

      campo11 = new MascaraDeCampo("ID_BENEF", 39, 51, 13, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo12 = new MascaraDeCampo("NM_BENEF", 52, 76, 25, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo13 = new MascaraDeCampo("DT_ATEND", 77, 97, 21, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo14 = new MascaraDeCampo("CD_EXCECAO", 98, 98, 1, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo15 = new MascaraDeCampo("TP_CARACTER_ATEND", 99, 99, 1, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo16 = new MascaraDeCampo("CD_CID", 102, 107, 6, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo17 = new MascaraDeCampo("TP_PACIENTE", 122, 122, 1, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo18 = new MascaraDeCampo("TP_SAIDA_CONS_SADT", 123, 123, 1, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo19 = new MascaraDeCampo("TP_ATENDIMENTO", 124, 125, 2, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      campo20 = new MascaraDeCampo("NR_GUIA_PRINCIPAL", 126, 136, 11, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);


      mascaraDeRegistro501.adicionaCampo(campo3);
      mascaraDeRegistro501.adicionaCampo(campo4);
      mascaraDeRegistro501.adicionaCampo(campo5);
      mascaraDeRegistro501.adicionaCampo(campo6);
      mascaraDeRegistro501.adicionaCampo(campo7);

      mascaraDeRegistro502.adicionaCampo(campo1);
      mascaraDeRegistro502.adicionaCampo(campo2);
      mascaraDeRegistro502.adicionaCampo(campo3);
      mascaraDeRegistro502.adicionaCampo(campo4);
      mascaraDeRegistro502.adicionaCampo(campo8);
      mascaraDeRegistro502.adicionaCampo(campo9);
      mascaraDeRegistro502.adicionaCampo(campo10);

      mascaraDeRegistro502.adicionaCampo(campo11);
      mascaraDeRegistro502.adicionaCampo(campo12);
      mascaraDeRegistro502.adicionaCampo(campo13);
      mascaraDeRegistro502.adicionaCampo(campo14);
      mascaraDeRegistro502.adicionaCampo(campo15);
      mascaraDeRegistro502.adicionaCampo(campo16);
      mascaraDeRegistro502.adicionaCampo(campo17);
      mascaraDeRegistro502.adicionaCampo(campo18);
      mascaraDeRegistro502.adicionaCampo(campo19);
      mascaraDeRegistro502.adicionaCampo(campo20);

      mascaraDeArquivo500.adicionaMascaraDeRegistro(mascaraDeRegistro501);
      mascaraDeArquivo500.adicionaMascaraDeRegistro(mascaraDeRegistro502);
   }

   @After
   public void finalizarUmTeste() throws Exception {
      mascaraDeArquivo500 = null;
      mascaraDeRegistro501 = mascaraDeRegistro502 = null;
      campo1 = campo2 = campo3 = campo4 = campo5 = campo6 = campo7 = campo8 = campo9 = null;
   }

   @Test
   public void testParametrosDoUsuarioValidos() {
      System.out.println("testParametrosDoUsuarioValidos()");

      List<MascaraDeCampo> parametrosEsperados = new ArrayList<MascaraDeCampo>();

      MascaraDeCampo p1 = new MascaraDeCampo("FONTE", 0, 0, 1, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);
      MascaraDeCampo p2 = new MascaraDeCampo("REFERENCIA", 0, 0, 6, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, false);

      parametrosEsperados.add(p1);
      parametrosEsperados.add(p2);
      int i = 0;
      for (MascaraDeCampo parametrosResultante : mascaraDeArquivo500.getParametrosDoUsuario()) {
         assertEquals("obterParametrosDoUsuarioValidos :", parametrosEsperados.get(i).getCodigo(),
                 parametrosResultante.getCodigo());
         i += 1;
      }

   }

   @Test
   public void testAdicionarMascaraDeRegistro() {
      System.out.println("adicionarMascaraDeRegistro");

      Map<String, MascaraDeCampo> campos = new HashMap<String, MascaraDeCampo>();
      Map<String, MascaraDeRegistro> mascaras = new HashMap<String, MascaraDeRegistro>();
      mascaras.put("50115", mascaraDeRegistro501);
      mascaras.put("50215", mascaraDeRegistro502);


      Map<String, MascaraDeRegistro> m;
      m = mascaraDeArquivo500.getMascarasDeRegistros();
      Set<String> chaves = m.keySet();

      for (String i : chaves) {
         assertEquals(m.get(i).getCodigo(), mascaras.get(i).getCodigo());
         assertEquals(m.get(i).getDescricao(), mascaras.get(i).getDescricao());
         assertEquals(m.get(i).getTamanho(), mascaras.get(i).getTamanho());
      }
   }

   @Test
   public void testRegistroComValoresComRegistroValido() {
      System.out.println("testRegistroComValoresComRegistroValido");
      String s = "00000006502201011020009325586710217   0317000029003ADELIA AP COSTA SILVA    2010/11/0813:52:00-03L02 Z00   00000000000000150400093255867";
      
      verificarRegistroObtido("testRegistroComValoresComRegistroValido :", s, mascaraDeRegistro502);
   }

   @Test
   public void testRegistroComValoresComRegistroMenor() {
      System.out.println("testRegistroComValoresComRegistroMenor");
      String s = "00000006502201011020009325586710217   0317000029003ADELIA AP COSTA SILVA    2010/11/0813:52:00-03L02 Z00   0000000000000015040009325586";
      verificarRegistroObtido("testRegistroComValoresComRegistroMenor", s, mascaraDeRegistro502);
   }

   @Test(expected = RegistroComTamanhoMaiorException.class)
   public void testRegistroComValoresComRegistroMaior() {
      System.out.println("testRegistroComValoresComRegistroMaior");
      String s = "00000006502201011020009325586710217   0317000029003ADELIA AP COSTA SILVA    2010/11/0813:52:00-03L02 Z00   000000000000001504000932558670";
      verificarRegistroObtido("testRegistroComValoresComRegistroMaior", s, mascaraDeRegistro502);
   }

   @Test(expected = RegistroDesconhecidoException.class)
   public void testRegistroComValoresComRegistroDesconhecido() {
      System.out.println("testRegistroComValoresComRegistroInvalido");
      String s = "00000006201201011020009325586710217   0317000029003ADELIA AP COSTA SILVA    2010/11/0813:52:00-03L02 Z00   00000000000000150400093255867";
      verificarRegistroObtido("testRegistroComValoresComRegistroInvalido :", s, null);
   }

   private void verificarRegistroObtido(final String msg, final String s, final MascaraDeRegistro expResult) {
      Map<String, MascaraDeRegistro> mascaras = new HashMap<String, MascaraDeRegistro>();
      mascaras.put("50115", mascaraDeRegistro501);
      mascaras.put("50215", mascaraDeRegistro502);

      MascaraDeRegistro m = mascaraDeArquivo500.obtemRegistroComValores(s);
      assertEquals(msg, expResult, m);
   }

   @Test
   public void testAtualizarParametrosDoUsuario() {
      System.out.println("atualizarParametrosDoUsuario");
      List<MascaraDeCampo> parametros = new ArrayList<MascaraDeCampo>();
      int i = 0;

      campo1.setValor("0217");
      campo2.setValor("20101214");
      parametros.add(campo1);
      parametros.add(campo2);

      mascaraDeArquivo500.atualizaParametrosDoUsuario(parametros);

      for (MascaraDeCampo mc : mascaraDeArquivo500.getParametrosDoUsuario()) {
         assertEquals(mc.getValor(), parametros.get(i).getValor());
         i += 1;
      }

      i = 0;
      for (MascaraDeCampo mc : mascaraDeArquivo500.getMascarasDeRegistros().get("50115").getCampos()) {
         if (mc.getTipoDeOrigem() == TipoDeOrigem.PARAMETRO) {
            assertEquals(mc.getCodigo(), parametros.get(i).getCodigo());
            assertEquals(mc.getValor(), parametros.get(i).getValor());
            i += 1;
         }
      }
      String esperado = "DELETE FROM R501 WHERE FONTE= \"0217\" AND REFERENCIA= \"20101214\";";
      assertEquals("testaFormataParaRemocao : ",esperado, mascaraDeArquivo500.getMascarasDeRegistros().get("50115").formataParaRemocao());
   }
}
