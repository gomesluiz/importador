/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.dominio;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luiz
 */
public class TipoDeCampoTest {

    @Test
    public void testValues() {
        System.out.println("values");
        TipoDeCampo[] expResult =
                {TipoDeCampo.A
                , TipoDeCampo.N
                , TipoDeCampo.NS
                , TipoDeCampo.AN
                , TipoDeCampo.ANS
                , TipoDeCampo.MM
                , TipoDeCampo.DD
                , TipoDeCampo.YYYY
                , TipoDeCampo.HH
                , TipoDeCampo.MI
                , TipoDeCampo.SS
                , TipoDeCampo.DATA1
                , TipoDeCampo.DATA2
                , TipoDeCampo.CID
                , TipoDeCampo.HORA
                , TipoDeCampo.UF
                , TipoDeCampo.ML
                , TipoDeCampo.SL};
        TipoDeCampo[] result = TipoDeCampo.values();
        assertEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class TipoDeCampo.
     */
    @Test
    public void testValueOf() {
        System.out.println("valueOf");
        String name = "A";
        TipoDeCampo expResult = TipoDeCampo.A;
        TipoDeCampo result = TipoDeCampo.valueOf(name);
        assertEquals(expResult, result);

        name = "N";
        expResult = TipoDeCampo.N;
        result = TipoDeCampo.valueOf(name);
        assertEquals(expResult, result);

        name = "DATA1";
        expResult = TipoDeCampo.DATA1;
        result = TipoDeCampo.valueOf(name);
        assertEquals(expResult, result);
    }

}