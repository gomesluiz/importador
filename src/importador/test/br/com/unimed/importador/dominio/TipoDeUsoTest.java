/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.dominio;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luiz
 */
public class TipoDeUsoTest {

     /**
     * Test of values method, of class TipoDeUso.
     */
    @Test
    public void testValues() {
        System.out.println("values");
        TipoDeUso[] expResult = {TipoDeUso.MANDATORIO, TipoDeUso.OPCIONAL};
        TipoDeUso[] result = TipoDeUso.values();
        assertEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class TipoDeUso.
     */
    @Test
    public void testValueOf() {
        System.out.println("valueOf");
        TipoDeUso expResult = TipoDeUso.MANDATORIO;
        TipoDeUso result = TipoDeUso.valueOf("MANDATORIO");
        assertEquals(expResult, result);

        expResult = TipoDeUso.MANDATORIO;
        result = TipoDeUso.valueOf("MANDATORIO");
        assertEquals(expResult, result);
    }

}