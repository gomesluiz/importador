package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.exceptions.QueryException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MascaraDeArquivoDAOTest {

    MascaraDeArquivoDAO dao;
    
    @Before
    public void inicializarUmTeste() throws Exception {
       dao = MascaraDeArquivoDAO.getInstance();
       dao.abrirConexao();
    }

    @After
    public void finalizarUmTeste() throws Exception {
       dao.fecharConexao();
       dao = null;
    }

    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        assertEquals(dao, MascaraDeArquivoDAO.getInstance());
    }

    @Test
    public void testListarPorUmRegistroExistente() {
        System.out.println("ListarPorUmRegistroExistente");
        assertEquals("A500", dao.listaPorChavePrimaria("A500", 15).getCodigo());
    }

    @Test(expected=QueryException.class)
    public void testListarPorUmRegistroInexistente() {
        System.out.println("ListarPorUmRegistroInexistente");
        dao.listaPorChavePrimaria("A500", 99);
    }

}
