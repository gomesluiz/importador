/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.persistencia.dao;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author paulo
 */
public class GenericDAOTest {

    public GenericDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of getInstance method, of class GenericDAO.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        InterfaceJdbcDataAccess expResult = GenericDAO.getInstance();
        InterfaceJdbcDataAccess result = GenericDAO.getInstance();
        assertEquals(expResult.toString(), result.toString());
      
    }

 
   
    /**
     * Test of isTableExist method, of class GenericDAO.
     */
    @Test
    public void testIsTableExist() {
        System.out.println("isTableExist");
        String table = "mascara_de_arquivo";
        InterfaceJdbcDataAccess instance = GenericDAO.getInstance();
        boolean expResult = true;

        ((GenericDAO)instance).abrirConexao();
        boolean result = ((GenericDAO)instance).isTableExist(table);
      
        assertEquals(expResult, result);

        table = "R1000"; //false
        expResult = false;
        result = ((GenericDAO)instance).isTableExist(table);
        assertEquals(expResult, result);

        ((GenericDAO)instance).fecharConexao();

    }

 

    /**
     * Test of getErrorCount method, of class GenericDAO.
     */
    @Test
    public void testGetErrorCount() {
        System.out.println("getErrorCount");
         InterfaceJdbcDataAccess instance = GenericDAO.getInstance();
        int expResult = 0;
        int result = ((GenericDAO) instance).getErrorCount();
        assertEquals(expResult, result);
      
    }

  

}