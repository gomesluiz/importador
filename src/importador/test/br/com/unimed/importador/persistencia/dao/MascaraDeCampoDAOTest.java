package br.com.unimed.importador.persistencia.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MascaraDeCampoDAOTest {

    MascaraDeCampoDAO dao;
    
    @Before
    public void inicializarUmTeste() throws Exception {
       dao = MascaraDeCampoDAO.getInstance();
       dao.abrirConexao();

    }

    @After
    public void finalizarUmTeste() throws Exception {
       dao.fecharConexao();
       dao = null;
    }

    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        assertEquals(dao, MascaraDeCampoDAO.getInstance());
    }

    @Test
    public void testListarPorUmRegistroExistente() {
        System.out.println("ListarPorUmRegistroExistente");
        assertEquals(7, dao.listaPorRegistroEVersao("501", 15).size());
    }

    @Test
    public void testListarPorUmRegistroInexistente() {
        System.out.println("ListarPorUmRegistroInexistente");
        assertEquals(0, dao.listaPorRegistroEVersao("510", 15).size());
    }
}