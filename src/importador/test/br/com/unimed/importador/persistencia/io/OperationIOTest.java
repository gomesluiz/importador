package br.com.unimed.importador.persistencia.io;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.FileNotFoundException;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Teste de Unidade para validação da função Read/Write
 * @author Affirmate
 */
public class OperationIOTest {

    DataIOFactory ioFactory;
    InterfaceIODataAccess ioAccess;

    public OperationIOTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Start Testing");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        System.out.println("End Testing");
    }

    @Before
    public void setUp() {

        // Especifica o tipo de operação (read / write)
        ioFactory =
                DataIOFactory.getIOFactory(DataIOFactory.READBYTE);

        // ioAccess permite setar o arquivo e conectar fisicamente e fechar
        // a conexão
        ioAccess = ioFactory.getIO();

        //definir o arquivo;
        try {
            ioAccess.setFile(new File("/tmp/R500.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OperationIOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Conectar no arquivo
        ioAccess.connect(ioAccess.READ_FILE);

    }

    @After
    public void tearDown() {
        if (ioAccess.closeConnection(ioAccess.READ_FILE)) {
            System.out.println("\n\nArquivo fechado com sucesso");
        } else {
            System.out.println("\n\nProblemas no fechamento do arquivo ");
        }
    }

    @Test
    public void testRead() {

        //Testing Read Files

        byte[] k = (ioAccess.readBuffer(10));
        String test1 = new String(k);

        assertEquals(test1, "0000000150");

        byte[] l = ioAccess.readBuffer(8, 10);
        String test2 = new String(l);

        assertEquals(test2, "1501021700");

        byte[] u = ioAccess.readBuffer(80, 10);
        String test3 = new String(u);
        assertEquals(test3, "2010112315");



        //close file and open again
        ioAccess.closeConnection(ioAccess.READ_FILE);





    }

    @Test(expected = AssertionError.class)
    public void testIndexOutOfBoundsException() {
        boolean thrown = false;

        try {
            byte[] k = (ioAccess.readBuffer(75, 15));
            String test1 = new String(k);
        } catch (AssertionError e) {
            thrown = true;
        } catch (IndexOutOfBoundsException e) {
            thrown = true;
        }

        assertTrue(thrown);

    }

    public void testNewFile() {
        // Especifica o tipo de operação (read / write)
        ioFactory =
                DataIOFactory.getIOFactory(DataIOFactory.READBYTE);

        // ioAccess permite setar o arquivo e conectar fisicamente e fechar
        // a conexão
        ioAccess = ioFactory.getIO();

        //definir o arquivo;
        try {
            ioAccess.setFile(new File("/tmp/r500.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OperationIOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Conectar no arquivo
        ioAccess.connect(ioAccess.READ_FILE);
    }
}
