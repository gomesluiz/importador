package br.com.unimed.importador.persistencia.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PendenciaDeAlteracaoDAOTest {

    PendenciaDeAlteracaoDAO dao;

    @Before
    public void configurarUmTeste() throws Exception {
       dao = PendenciaDeAlteracaoDAO.getInstance();
       dao.abrirConexao();

    }

    @After
    public void finalizarUmTeste() throws Exception {
       dao.fecharConexao();
       dao = null;
    }

    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        assertEquals(dao, PendenciaDeAlteracaoDAO.getInstance());
    }

    @Test
    public void testListarComPendenciasNaoResolvidasExistentes() {
        System.out.println("testListarComPendenciasNaoResolvidasExistentes");
        assertEquals(1, dao.listaNaoResolvidas("502", 15, "NR_TESTE").size());
    }

    @Test
    public void testListarComPendenciasNaoResolvidasNaoExistentes() {
        System.out.println("testListarComPendenciasNaoResolvidasNaoExistentes");
        assertEquals(0, dao.listaNaoResolvidas("999", 15, "NR_TESTE").size());
    }
}