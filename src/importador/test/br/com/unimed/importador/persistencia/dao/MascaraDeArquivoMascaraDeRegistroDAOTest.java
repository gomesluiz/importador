package br.com.unimed.importador.persistencia.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MascaraDeArquivoMascaraDeRegistroDAOTest {

   MascaraDeArquivoMascaraDeRegistroDAO dao;

   @Before
   public void configurarUmTeste() throws Exception {
      dao = MascaraDeArquivoMascaraDeRegistroDAO.getInstance();
      dao.abrirConexao();

   }

   @After
   public void limparUmTeste() throws Exception {
      dao.fecharConexao();
      dao = null;
   }

   @Test
   public void testGetInstance() {
      System.out.println("getInstance");
      assertEquals(dao, MascaraDeArquivoMascaraDeRegistroDAO.getInstance());
   }

   @Test
   public void testListarPorUmArquivoEVersaoExistente() {
      System.out.println("ListarPorUmRegistroExistente");
      assertEquals(7, dao.listaPorArquivoEVersao("A500", 15).size());
   }

   @Test
   public void testListarPorUmArquivoEVersaoInexistente() {
      System.out.println("ListarPorUmRegistroInexistente");
      assertEquals(0, dao.listaPorArquivoEVersao("A999", 15).size());
   }
}
