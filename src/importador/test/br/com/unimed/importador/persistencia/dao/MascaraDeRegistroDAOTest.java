package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.exceptions.QueryException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MascaraDeRegistroDAOTest {

    MascaraDeRegistroDAO dao;
    
    @Before
    public void configurarUmTeste() throws Exception {
       dao = MascaraDeRegistroDAO.getInstance();
       dao.abrirConexao();
    }

    @After
    public void limparUmTeste() throws Exception {
       dao.fecharConexao();
       dao = null;
    }

    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        assertEquals(dao, MascaraDeRegistroDAO.getInstance());
    }

    @Test
    public void testListarPorUmRegistroExistente() {
        System.out.println("ListarPorUmRegistroExistente");       
        assertEquals("501", dao.listaPorChavePrimaria("501", 15).getCodigo());
    }

    @Test(expected=QueryException.class)
    public void testListarPorUmRegistroInexistente() {
        System.out.println("ListarPorUmRegistroInexistente");
        dao.listaPorChavePrimaria("999", 15);
    }
}
